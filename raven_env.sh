export RAVEN_INCLUDE="/app/raven/include /app/raven/include/raven-engine /app/raven/include/include /app/raven/dep/usr/local/include"
export RAVEN_LIBS="/app/raven/lib /app/raven/dep/usr/local/lib /app/raven/dep/usr/local/lib64"
export LD_LIBRARY_PATH=/app/raven/lib:/app/raven/dep/usr/local/lib:/app/raven/dep/usr/local/lib64:\$LD_LIBRARY_PATH
export WAL_DIR=/app/raven/wal
export DB_DIR=/app/raven/logs/db
export CLUSTER_NAME=RAVEN_CLUSTER
export HELIX_REST_URL=http://helix:8080
export ZK_SERVER=192.168.99.100:2181
export CMAKE_BUILD_TYPE=RELEASE
export CMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
if [[ -f raven_env.usr.sh ]]; then
  . raven_env.usr.sh
fi
