




#include "ruleset_auth_regulation_ruleset_V1.h"
#include <glog/logging.h>
#include <bitset>
#include <tuple>
#include <string_view>
#include <cmath>
#include <string>

using namespace raven::types; // bring in the chrono operator

namespace raven::generated {
namespace {
enum class RULE_NAMES {
RULE_reg_rule1,
RULE_MAX};

static raven::base::RuleInfo gRules[] = {
    {"reg_rule1", /*ruleName*/ "auth_regulation_ruleset", /*RuleSetName*/ false, /* Simulation */
     {99999, /*soft_max*/ 999999, /*hard_max*/ 0, /*error count*/ true /*enabled*/  }},
};

static raven::base::RuleStats *gRuleStats[] = {
    nullptr, // reg_rule1
};
static raven::base::RuleSetStats *gRuleSetStats = nullptr;
static const std::pair<std::string, std::string> *gRulesetRuleNamePtr[] = {
    nullptr, // reg_rule1
};

static raven::base::RuleSetInfo *get_ruleset_auth_regulation_ruleset_V1_info() {
  static raven::base::RuleSetInfo ruleSetInfo = {
      "auth_regulation_ruleset", 1, {
          1000000000, //soft_max
          1000000000, //hard_max
          0, //error count
          true// enabled
      }, {
        &gRules[static_cast<int>(RULE_NAMES::RULE_reg_rule1)],
      }
  };
  return &ruleSetInfo;
}

/*
void activateDeactivateTimeBasedRules() {
  auto currentTime [[maybe_unused]] = std::chrono::system_clock::now();

}
*/

} // Close anonymous namespace


raven::base::RuleSetExecutionResult  raven::generated::ruleset_auth_regulation_ruleset_V1::execute_ruleset_auth_regulation_ruleset_V1(/* parameters with the right class names */) {
  using namespace std::literals;
  std::bitset<static_cast<size_t>(RULE_NAMES::RULE_MAX)> rulesFired;
  raven::base::RuleSetExecutionResult ruleSetExecutionResult;

  auto preChecks = [](uint32_t ruleNumber) -> bool {
    return raven::base::RuleSet::isRuleEnabled(&gRules[ruleNumber], *gRuleStats[ruleNumber]);
  };
  auto triggerRule = [&](uint32_t ruleNumber) -> void {
    if (UNLIKELY(gRules[ruleNumber].simulation)) {
      ruleSetExecutionResult.rulesTriggeredInSimulation.push_back(gRulesetRuleNamePtr[ruleNumber]);
    } else {
      raven::base::RuleSet::incrementAndRunChecks(&gRules[ruleNumber], *gRuleStats[ruleNumber]);
      rulesFired.set(ruleNumber);
      ruleSetExecutionResult.rulesTrigger.push_back(gRulesetRuleNamePtr[ruleNumber]);
    }
  };

  try {
    if (!raven::base::RuleSet::isRuleSetEnabled(get_ruleset_auth_regulation_ruleset_V1_info(), *gRuleSetStats)) {
      DLOG(INFO)<<"RuleSet auth_regulation_ruleset is not enabled";
      return ruleSetExecutionResult;
    }
    if(!(true)) {
      DLOG(INFO)<<"RuleSet auth_regulation_ruleset condition not successful";
      return ruleSetExecutionResult;
    }
    if(!raven::base::RuleSet::incrementAndRunChecks(get_ruleset_auth_regulation_ruleset_V1_info(), *gRuleSetStats)) {
        DLOG(INFO)<<"Failed ruleset level checks (soft_max or hard_max etc.) for auth_regulation_ruleset";
        return ruleSetExecutionResult;
    }
    

    /* RULE reg_rule1 0 */
    if (preChecks(static_cast<uint32_t>(RULE_NAMES::RULE_reg_rule1))) {
      try {
        const auto &ravenCurrentRuleName [[maybe_unused]] = gRules[static_cast<uint32_t>(RULE_NAMES::RULE_reg_rule1)].ruleName;
        if (/*rule conditions*/ true) {
          triggerRule(static_cast<uint32_t>(RULE_NAMES::RULE_reg_rule1));
          
        }
      } catch (...) {
        raven::base::RuleSet::triggerError(&gRules[static_cast<uint32_t>(RULE_NAMES::RULE_reg_rule1)],
                                           *gRuleStats[static_cast<uint32_t>(RULE_NAMES::RULE_reg_rule1)]);
      }
    } else {
      LOG_FIRST_N(WARNING,10) << "Rule " << "reg_rule1" << " failed prechecks";
    }

  } catch (std::exception &e) {
    raven::base::RuleSet::triggerError(get_ruleset_auth_regulation_ruleset_V1_info(), *gRuleSetStats);
    LOG(WARNING) <<"RuleSet exception in " << "auth_regulation_ruleset" << e.what();
  }
  return ruleSetExecutionResult;
}

}







#include "raven-engine/base/Artifact.h"





static bool ruleset_auth_regulation_ruleset_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
          const auto rulesetLocalInfo = raven::generated::get_ruleset_auth_regulation_ruleset_V1_info();
          bool error = false;
          try {
            raven::base::RuleSet::AddRuleSet(rulesetLocalInfo);
            raven::generated::gRuleSetStats = raven::base::RuleSet::GetRuleSetStats(rulesetLocalInfo->ruleSetName);
            if (raven::generated::gRuleSetStats == nullptr) error = true;
            std::vector<std::string> local_rules;
            for (const auto &v:rulesetLocalInfo->ruleInfo)
              local_rules.emplace_back(v->ruleName);
            auto ret = raven::base::RuleSet::GetRuleStats(rulesetLocalInfo->ruleSetName, local_rules);
            for (size_t i = 0; i < ret.size(); i++) {
              auto[rName, rulestat, ruleset_rulename_ptr] = ret[i];
              if (rName == rulesetLocalInfo->ruleInfo.at(i)->ruleName) {
                if (rulestat == nullptr || ruleset_rulename_ptr == nullptr) error = true;
                raven::generated::gRuleStats[i] = rulestat;
                raven::generated::gRulesetRuleNamePtr[i] = ruleset_rulename_ptr;
              }
            }
          } catch (...) {
            LOG(ERROR) << "Exception thrown while loading ruleset_auth_regulation_ruleset_V1";
            error = true;
          }
          if (error) {
            rulesetLocalInfo->ruleSetInfo.enabled = false;
            LOG(ERROR) << "Error while loading ruleset_auth_regulation_ruleset_V1";
          }
          return !error;
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      raven::base::RuleSet::RemoveRuleSet(raven::generated::get_ruleset_auth_regulation_ruleset_V1_info());
    }
    return true;
}

extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_ruleset_auth_regulation_ruleset_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_ruleset_auth_regulation_ruleset_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Ruleset",     std::vector({    std::string("auth_regulation_ruleset"),
})
, 1, ruleset_auth_regulation_ruleset_V1_StatusChangeCallBack, "ruleset_auth_regulation_ruleset_V1");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("card_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("auth_ruleset_result"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("raven_gbm_model_output"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("account_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("account_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("txn_auth_input"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("card_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void ruleset_auth_regulation_ruleset_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_ruleset_auth_regulation_ruleset_V1());
     if(!artifact) return;
     ruleset_auth_regulation_ruleset_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     ruleset_auth_regulation_ruleset_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void ruleset_auth_regulation_ruleset_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_ruleset_auth_regulation_ruleset_V1());
    if(!artifact) return;
     ruleset_auth_regulation_ruleset_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     ruleset_auth_regulation_ruleset_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
