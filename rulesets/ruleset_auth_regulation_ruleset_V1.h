#pragma once






#include <memory>
#include "raven-engine/base/RuleSet.h"
#include "raven-engine/base/RavenSwitches.h"
#include "../records/record_txn_auth_input_V1.h"
#include "../records/record_card_data_V1.h"
#include "../records/record_account_data_V1.h"
#include "../records/record_customer_data_V1.h"
#include "../records/record_account_level_rollup_V1.h"
#include "../records/record_card_level_rollup_V1.h"
#include "../records/record_customer_level_rollup_V1.h"
#include "../records/record_raven_gbm_model_output_V1.h"
#include "../records/record_auth_ruleset_result_V1.h"

namespace raven::generated {
  
class ruleset_auth_regulation_ruleset_V1 {
   public:
  const record_txn_auth_input_V1 * txn_auth_input;
  const record_card_data_V1 * card_data;
  const record_account_data_V1 * account_data;
  const record_customer_data_V1 * customer_data;
  const record_account_level_rollup_V1 * account_level_rollup;
  const record_card_level_rollup_V1 * card_level_rollup;
  const record_customer_level_rollup_V1 * customer_level_rollup;
  const record_raven_gbm_model_output_V1 * auth_model_score;
  RG_record_auth_ruleset_result_V1 * auth_ruleset_result;
  template<typename T>
  explicit ruleset_auth_regulation_ruleset_V1(T * module) {
      txn_auth_input = get_ptr(module->txn_auth_input);
      card_data = get_ptr(module->card_data);
      account_data = get_ptr(module->account_data);
      customer_data = get_ptr(module->customer_data);
      account_level_rollup = get_ptr(module->account_level_rollup);
      card_level_rollup = get_ptr(module->card_level_rollup);
      customer_level_rollup = get_ptr(module->customer_level_rollup);
      auth_model_score = get_ptr(module->auth_model_score);
      auth_ruleset_result = get_ptr(module->auth_ruleset_result);
    init();
  }
  void init() {}
  raven::base::RuleSetExecutionResult execute_ruleset() {
    return execute_ruleset_auth_regulation_ruleset_V1();
  }
  raven::base::RuleSetExecutionResult execute_ruleset_auth_regulation_ruleset_V1();
};
}
