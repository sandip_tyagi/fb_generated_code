#pragma once






#include <memory>
#include "raven-engine/base/RuleSet.h"
#include "raven-engine/base/RavenSwitches.h"
#include "../records/record_txn_auth_input_V1.h"
#include "../records/record_card_data_V1.h"
#include "../records/record_account_data_V1.h"
#include "../records/record_customer_data_V1.h"
#include "../records/record_black_list_entry_V1.h"
#include "../records/record_black_list_entry_V1.h"
#include "../records/record_black_list_entry_V1.h"
#include "../records/record_ip_address_record_V1.h"
#include "../records/record_card_level_rollup_V1.h"
#include "../records/record_account_level_rollup_V1.h"
#include "../records/record_customer_level_rollup_V1.h"
#include "../records/record_merchant_level_rollup_V1.h"
#include "../records/record_raven_gbm_model_output_V1.h"
#include "../records/record_fraud_ruleset_result_V1.h"

namespace raven::generated {
  
class ruleset_fraud_ruleset_V1 {
   public:
  const record_txn_auth_input_V1 * txn_auth_input;
  const record_card_data_V1 * card_data;
  const record_account_data_V1 * account_data;
  const record_customer_data_V1 * customer_data;
  const record_black_list_entry_V1 * card_black_list;
  const record_black_list_entry_V1 * merchant_black_list;
  const record_black_list_entry_V1 * device_black_list;
  const record_ip_address_record_V1 * ip_address_record;
  const record_card_level_rollup_V1 * card_level_rollup;
  const record_account_level_rollup_V1 * account_level_rollup;
  const record_customer_level_rollup_V1 * customer_level_rollup;
  const record_merchant_level_rollup_V1 * merchant_level_rollup;
  const record_raven_gbm_model_output_V1 * fraud_score;
  RG_record_fraud_ruleset_result_V1 * fraud_ruleset_result;
  template<typename T>
  explicit ruleset_fraud_ruleset_V1(T * module) {
      txn_auth_input = get_ptr(module->txn_auth_input);
      card_data = get_ptr(module->card_data);
      account_data = get_ptr(module->account_data);
      customer_data = get_ptr(module->customer_data);
      card_black_list = get_ptr(module->card_black_list);
      merchant_black_list = get_ptr(module->merchant_black_list);
      device_black_list = get_ptr(module->device_black_list);
      ip_address_record = get_ptr(module->ip_address_record);
      card_level_rollup = get_ptr(module->card_level_rollup);
      account_level_rollup = get_ptr(module->account_level_rollup);
      customer_level_rollup = get_ptr(module->customer_level_rollup);
      merchant_level_rollup = get_ptr(module->merchant_level_rollup);
      fraud_score = get_ptr(module->fraud_score);
      fraud_ruleset_result = get_ptr(module->fraud_ruleset_result);
    init();
  }
  void init() {}
  raven::base::RuleSetExecutionResult execute_ruleset() {
    return execute_ruleset_fraud_ruleset_V1();
  }
  raven::base::RuleSetExecutionResult execute_ruleset_fraud_ruleset_V1();
};
}
