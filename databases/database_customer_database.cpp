
#include "database_customer_database.h"
namespace raven::generated {
const char *getDatabaseName_customer_database() {
    return "customer_database";
}

static raven::base::Database *InitDB_customer_database() {
static raven::base::Database
    *db = new raven::base::Database(getDatabaseName_customer_database(), std::vector<raven::base::DatabaseRecord *>(), 16, true);
    return db;
}
}







#include "raven-engine/base/Artifact.h"





static bool database_customer_database_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
        raven::base::Database::AddDatabase(raven::generated::getDatabaseName_customer_database(), raven::generated::InitDB_customer_database());
    }
    return true;
}


extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_database_customer_database();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_database_customer_database() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Database",     std::vector({    std::string("customer_database"),
})
, 1, database_customer_database_V1_StatusChangeCallBack, "database_customer_database");
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void database_customer_database_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_database_customer_database());
     if(!artifact) return;
     database_customer_database_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     database_customer_database_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void database_customer_database_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_database_customer_database());
    if(!artifact) return;
     database_customer_database_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     database_customer_database_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
