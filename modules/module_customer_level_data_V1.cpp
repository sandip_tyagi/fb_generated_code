


#include "module_customer_level_data_V1.h"
#include "lib/include/custom_date.h"
#include <folly/GLog.h>

using namespace raven::types; // bring in the chrono operator


static std::unique_ptr<raven::base::Module> createNew_module_customer_level_data_V1(raven::base::Transaction *txn) {
    return std::make_unique<raven::generated::module_customer_level_data_V1>(txn);
}






static raven::base::ModuleInfo *GetModuleInfo_module_customer_level_data_V1() {
static raven::base::ModuleInfo moduleInfo = {
    "customer_level_data", // Module Name
    "DATA", // Module Type
    // INPUT RECORDS
    {
          {"card_data","card_data",1,false},
    },
    // OUTPUT RECORDS
    {
        {"customer_data","customer_data",1,false},
    },
    1, // Module Version
    "customer_database", //resourceName - database name. Empty string if no database
      true, // is colocation relevant?
    true, // readOnly - no updates to database
    true, // isSlave ok?
    std::chrono::microseconds(0), // timeout
    std::chrono::microseconds(0), // roll up timeout
0, // max roll up count
    1, // module_hash
    createNew_module_customer_level_data_V1, // New default module
    std::make_shared<::raven::base::ModuleMetrics>("customer_level_data",1),
    nullptr, // module trace information
    google::WARNING, // module log severity
    nullptr
    };
    return &moduleInfo;
}

const raven::base::ModuleInfo *raven::generated::module_customer_level_data_V1::getModuleInfo() const {
    return GetModuleInfo_module_customer_level_data_V1();
}

const raven::base::ModuleInfo *raven::generated::module_customer_level_data_V1::StaticGetModuleInfo() {
return GetModuleInfo_module_customer_level_data_V1();
}

 bool raven::generated::module_customer_level_data_V1::setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
        std::string_view recordId,
        const flatbuffers::Table *record) {
    /* Will be used for remote executing a module */
using namespace std::string_view_literals;

    if(recordId == "card_data"sv) {
        card_data = Getrecord_card_data_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "customer_data"sv) {
    RG_Ptr_customer_data = std::move(*UnPackrecord_customer_data_V1(Getrecord_customer_data_V1(reinterpret_cast<const void *>(record))));
    flatbufferStorage.push_back(std::move(detachedBuffer));
        return true;
    }
return false;
}

uint64_t raven::generated::module_customer_level_data_V1::getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const {
using namespace std::string_view_literals;
    if(recordId == "customer_data"sv) {
        auto offset = raven::generated::Createrecord_customer_data_V1(fbb, customer_data);
        return fbb.GetSize();
   }
        return 0;
}

std::pair<const uint8_t *, uint64_t> raven::generated::module_customer_level_data_V1::getRecordOutputPtr(std::string_view recordId)  {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "customer_data"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_customer_data_V1(fbb, customer_data);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
    flatbufferStorage.push_back(std::move(detachedBuffer));
    return pair;
    }
return {nullptr,0};
}

std::unique_ptr<flatbuffers::DetachedBuffer> raven::generated::module_customer_level_data_V1::getRecordOutputDetachedPtr(std::string_view recordId) const {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "customer_data"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_customer_data_V1(fbb, customer_data);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    return std::move(detachedBuffer);
    }
return nullptr;
}


std::pair<const uint8_t *, uint64_t> raven::generated::module_customer_level_data_V1::getRecordInputPtr(std::string_view recordId) {
            if(recordId == "card_data"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_card_data_V1(fbb, card_data->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
    return {nullptr,0};
}








folly::Future<folly::Unit> raven::generated::module_customer_level_data_V1::run() {
      if(!(true)) {
        LOG(INFO)<<"Skipping module for transaction " << messageMetaDataStruct.getTransactionId().toHexString();
        return folly::Unit();
    }
    

  /* Update colocation logic */
    std::vector<std::string> keyColocation;
    database_record_dbr_customer_data::Colocation customer_dataColocation = {};
    {
    database_record_dbr_customer_data::Colocation * colocation= &customer_data_Key.colocation;
      fb_write(colocation,colocation_0) = fb_read(card_data,customer_number);

    colocation->normalize();
    keyColocation.emplace_back(reinterpret_cast<const char *>(colocation),sizeof(customer_data_Key.colocation));
    if(keyColocation[keyColocation.size()-1]!=keyColocation[0]) {
       LOG(WARNING) << "Colocation keys are not matching for all database records "<< messageMetaDataStruct.getTransactionId().toHexString();
       throw std::runtime_error("Colocation keys are not matching for all database records in customer_level_data");
    }
    customer_dataColocation = *colocation;
    }
    std::shared_ptr<raven::base::VirtualApplicationDB> db = GetResourceAppDBPointerUseCase("customer_database", keyColocation[0], messageMetaDataStruct.getTransactionId(), admin::DBUseCase::Write, raven::db::DBRole::MASTER);
    if(db) {
    // Local processing

            {
            database_record_dbr_customer_data::Key dbKey = {};
            dbKey.colocation = customer_dataColocation;
            database_record_dbr_customer_data::Prefix * prefix = &dbKey.prefix;
            // Check if sort key needs to be generated

              fb_write(prefix,customer_number) = fb_read(card_data,customer_number);

            dbKey.normalize();
            auto dbCf = db->getColumnFamilyHandle("dbr_customer_data");
            if(!dbCf) {
                throw std::runtime_error("Column family not found dbr_customer_data");
            }
            std::string value;
            auto rDB = db->getDB();
            if(!rDB) {
                throw std::runtime_error("Database not returned");
            }
            auto rStatus = rDB->Get(rocksdb::ReadOptions(), dbCf,rocksdb::Slice(reinterpret_cast<const char *>(&dbKey),sizeof(dbKey)) ,&value);
            if(rStatus.ok()){
                if(value.size()<6) {
                    throw std::runtime_error("Invalid record read from database ");
                }
                VLOG(2)<<"Data found for customer_data";
//                raven::base::Record * dbReadRecord = reinterpret_cast<raven::base::Record *>(value.data());
                  const raven::base::fbs::GenericRecordHeaderOverlay * dbReadRecord = raven::base::fbs::GetGenericRecordHeaderOverlay(value.data());
//                dbReadRecord->size = static_cast<uint32_t>(value.size());
                if(customer_data) {
                    if(dbReadRecord->raven_header()->version() == 1) {
//                      if(sizeof(*(customer_data.get()))!=value.size()) {
//                         throw std::runtime_error("customer_data size does not match the record size=" + std::to_string(sizeof(*(customer_data.get()))) +" db size=" + std::to_string(value.size()));
//                      }
                      memcpy(customer_data,value.data(), value.size());
                    } else if(dbReadRecord->raven_header()->version() < 1) {
                        const raven::base::RecordInfo *recordInfo = raven::base::LookupRecord("customer_data",
                                    1);
                        flatbuffers::FlatBufferBuilder fbb;
                        auto upgradedDBRecord = recordInfo->upgraderFunctions.at(dbReadRecord->raven_header()->version())(fbb, reinterpret_cast<const flatbuffers::Table *>(dbReadRecord));
                        fbb.Finish(flatbuffers::Offset<raven::base::fbs::GenericRecordHeaderOverlay>(upgradedDBRecord));
                        auto Dbuff = fbb.Release();
                        if(upgradedDBRecord) {
//                            customer_data.reset(static_cast<record_customer_data_V1 *>(upgradedDBRecord.release()));
                              customer_data = raven::generated::UnPackrecord_customer_data_V1(raven::generated::Getrecord_customer_data_V1(Dbuff.data())).get();
                        } else {
                            LOG(ERROR)<<"Error in module while trying to upgrade record";
                        }
                    } else {
                        auto recordInfoDowngrade = raven::base::LookupRecord("customer_data",dbReadRecord->raven_header()->version());
                        flatbuffers::FlatBufferBuilder fbb;
                        auto downgradedDBRecord = recordInfoDowngrade->downgraderFunctions.at(1 /* version wanted */)(fbb, reinterpret_cast<const flatbuffers::Table *>(dbReadRecord));
                        fbb.Finish(flatbuffers::Offset<raven::base::fbs::GenericRecordHeaderOverlay>(downgradedDBRecord));
                        auto Dbuff = fbb.Release();
//                        customer_data.reset(static_cast<record_customer_data_V1 *>(downgradedDBRecord.release()));
                        customer_data = raven::generated::UnPackrecord_customer_data_V1(raven::generated::Getrecord_customer_data_V1(Dbuff.data())).get();
                    }
                } else
                  LOG(ERROR)<<"Pointer was empty";
            }
            else {
                VLOG(2) << "No data found while trying to retrieve data from customer_database customer_data " << rStatus.ToString();
            }
        }

    } else {
    // Remote processing
      flatbuffers::FlatBufferBuilder fbb;
      auto smInput = serializeModuleInputFB(fbb);
      fbb.Finish(smInput);
      auto payload = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
      std::weak_ptr<raven::base::Transaction> wpTxn;
      if(transaction) {
          wpTxn = transaction->weak_from_this();
      }
      LOG(INFO) << "Sending module for remote processing " << messageMetaDataStruct.getTransactionId().toHexString() << " transaction shared_ptr = " << !!transaction;
      return SendPayloadToRemoteDBResource("module", "customer_database",keyColocation[0],std::move(payload), messageMetaDataStruct).thenValue([this,transaction = (transaction!=nullptr), wpTxn = std::move(wpTxn)](std::unique_ptr<flatbuffers::DetachedBuffer> response) -> folly::Future<folly::Unit> {
        std::shared_ptr<raven::base::Transaction> sp;
        if(transaction) {
            sp = wpTxn.lock();
            if(!sp) return folly::make_exception_wrapper<std::runtime_error>("Transaction no longer exists");
        }
        LOG(INFO) << "Response received from remote node" << messageMetaDataStruct.getTransactionId().toHexString() << " transaction shared_ptr = " << !!sp;
        std::lock_guard lg(responseLock);
        if(!timedout) {
            this->deserializeModuleOutput(this,std::move(response));
            flatbuffers::FlatBufferBuilder fbb_customer_data;
            auto offset_customer_data = raven::generated::Createrecord_customer_data_V1(fbb_customer_data, customer_data);
                fbb_customer_data.Finish(offset_customer_data);
                auto BUFFER_customer_data = fbb_customer_data.Release();
                OUT_customer_data = raven::generated::Getrecord_customer_data_V1(BUFFER_customer_data.data());
        }
        return folly::Unit();
      });

    }
    return folly::Unit();
}









#include "raven-engine/base/Artifact.h"





static bool module_customer_level_data_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
      raven::base::Module::RegisterModule(
                const_cast<raven::base::ModuleInfo *>(raven::generated::module_customer_level_data_V1::StaticGetModuleInfo()
        ));
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      raven::base::Module::UnRegisterModule(raven::generated::module_customer_level_data_V1::StaticGetModuleInfo());
    }
    return true;
}

extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_customer_level_data_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_customer_level_data_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Module",     std::vector({    std::string("customer_level_data"),
})
, 1, module_customer_level_data_V1_StatusChangeCallBack, "module_customer_level_data_V1");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("card_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_database"),
    std::string("dbr_customer_data"),
})
, "DatabaseRecord", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_database"),
})
, "Database", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void module_customer_level_data_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_customer_level_data_V1());
     if(!artifact) return;
     module_customer_level_data_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     module_customer_level_data_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void module_customer_level_data_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_customer_level_data_V1());
    if(!artifact) return;
     module_customer_level_data_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     module_customer_level_data_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
