


#include "module_auth_rules_V1.h"
#include "lib/include/custom_date.h"
#include <folly/GLog.h>

using namespace raven::types; // bring in the chrono operator




#include "../rulesets/ruleset_auth_ruleset_V1.h"
#include "../rulesets/ruleset_auth_regulation_ruleset_V1.h"

static std::unique_ptr<raven::base::Module> createNew_module_auth_rules_V1(raven::base::Transaction *txn) {
    return std::make_unique<raven::generated::module_auth_rules_V1>(txn);
}






static raven::base::ModuleInfo *GetModuleInfo_module_auth_rules_V1() {
static raven::base::ModuleInfo moduleInfo = {
    "auth_rules", // Module Name
    "RULESET", // Module Type
    // INPUT RECORDS
    {
          {"txn_auth_input","txn_auth_input",1,false},
          {"card_data","card_data",1,false},
          {"account_data","account_data",1,false},
          {"customer_data","customer_data",1,false},
          {"account_level_rollup","account_level_rollup",1,false},
          {"card_level_rollup","card_level_rollup",1,false},
          {"customer_level_rollup","customer_level_rollup",1,false},
          {"auth_model_score","raven_gbm_model_output",1,false},
    },
    // OUTPUT RECORDS
    {
        {"auth_ruleset_result","auth_ruleset_result",1,false},
    },
    1, // Module Version
    "", //resourceName - database name. Empty string if no database
      true, // is colocation relevant?
    true, // readOnly - no updates to database
    true, // isSlave ok?
    std::chrono::microseconds(0), // timeout
    std::chrono::microseconds(0), // roll up timeout
0, // max roll up count
    1, // module_hash
    createNew_module_auth_rules_V1, // New default module
    std::make_shared<::raven::base::ModuleMetrics>("auth_rules",1),
    nullptr, // module trace information
    google::WARNING, // module log severity
    nullptr
    };
    return &moduleInfo;
}

const raven::base::ModuleInfo *raven::generated::module_auth_rules_V1::getModuleInfo() const {
    return GetModuleInfo_module_auth_rules_V1();
}

const raven::base::ModuleInfo *raven::generated::module_auth_rules_V1::StaticGetModuleInfo() {
return GetModuleInfo_module_auth_rules_V1();
}

 bool raven::generated::module_auth_rules_V1::setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
        std::string_view recordId,
        const flatbuffers::Table *record) {
    /* Will be used for remote executing a module */
using namespace std::string_view_literals;

    if(recordId == "txn_auth_input"sv) {
        txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "card_data"sv) {
        card_data = Getrecord_card_data_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "account_data"sv) {
        account_data = Getrecord_account_data_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "customer_data"sv) {
        customer_data = Getrecord_customer_data_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "account_level_rollup"sv) {
        account_level_rollup = Getrecord_account_level_rollup_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "card_level_rollup"sv) {
        card_level_rollup = Getrecord_card_level_rollup_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "customer_level_rollup"sv) {
        customer_level_rollup = Getrecord_customer_level_rollup_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "auth_model_score"sv) {
        auth_model_score = Getrecord_raven_gbm_model_output_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "auth_ruleset_result"sv) {
    RG_Ptr_auth_ruleset_result = std::move(*UnPackrecord_auth_ruleset_result_V1(Getrecord_auth_ruleset_result_V1(reinterpret_cast<const void *>(record))));
    flatbufferStorage.push_back(std::move(detachedBuffer));
        return true;
    }
return false;
}

uint64_t raven::generated::module_auth_rules_V1::getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const {
using namespace std::string_view_literals;
    if(recordId == "auth_ruleset_result"sv) {
        auto offset = raven::generated::Createrecord_auth_ruleset_result_V1(fbb, auth_ruleset_result);
        return fbb.GetSize();
   }
        return 0;
}

std::pair<const uint8_t *, uint64_t> raven::generated::module_auth_rules_V1::getRecordOutputPtr(std::string_view recordId)  {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "auth_ruleset_result"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_auth_ruleset_result_V1(fbb, auth_ruleset_result);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
    flatbufferStorage.push_back(std::move(detachedBuffer));
    return pair;
    }
return {nullptr,0};
}

std::unique_ptr<flatbuffers::DetachedBuffer> raven::generated::module_auth_rules_V1::getRecordOutputDetachedPtr(std::string_view recordId) const {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "auth_ruleset_result"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_auth_ruleset_result_V1(fbb, auth_ruleset_result);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    return std::move(detachedBuffer);
    }
return nullptr;
}


std::pair<const uint8_t *, uint64_t> raven::generated::module_auth_rules_V1::getRecordInputPtr(std::string_view recordId) {
            if(recordId == "txn_auth_input"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_txn_auth_input_V1(fbb, txn_auth_input->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "card_data"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_card_data_V1(fbb, card_data->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "account_data"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_account_data_V1(fbb, account_data->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "customer_data"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_customer_data_V1(fbb, customer_data->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "account_level_rollup"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_account_level_rollup_V1(fbb, account_level_rollup->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "card_level_rollup"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_card_level_rollup_V1(fbb, card_level_rollup->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "customer_level_rollup"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_customer_level_rollup_V1(fbb, customer_level_rollup->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "auth_model_score"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_raven_gbm_model_output_V1(fbb, auth_model_score->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
    return {nullptr,0};
}







folly::Future<folly::Unit> raven::generated::module_auth_rules_V1::run() {
    if(!(true)) {
        LOG(INFO)<<"Skipping module for transaction " << messageMetaDataStruct.getTransactionId().toHexString();
        return folly::Unit();
    }
    
    {
        std::string rulesetTriggeredList;
        ruleset_auth_ruleset_V1 auth_ruleset(this);
        const auto&ruleResult= auth_ruleset.execute_ruleset();
        for(const auto&r:ruleResult.rulesTrigger) {
          rulesetTriggeredList+=r->second+",";
        }
        if(!rulesetTriggeredList.empty()) {
          LOG(INFO)<<"Rules triggered from auth_ruleset - [ " << rulesetTriggeredList << "] - transaction id = " << messageMetaDataStruct.getTransactionId().toHexString();
        } else {
            LOG(INFO)<<"No rules triggered from auth_ruleset transaction id = " << messageMetaDataStruct.getTransactionId().toHexString();
        }
    }
    {
        std::string rulesetTriggeredList;
        ruleset_auth_regulation_ruleset_V1 auth_regulation_ruleset(this);
        const auto&ruleResult= auth_regulation_ruleset.execute_ruleset();
        for(const auto&r:ruleResult.rulesTrigger) {
          rulesetTriggeredList+=r->second+",";
        }
        if(!rulesetTriggeredList.empty()) {
          LOG(INFO)<<"Rules triggered from auth_regulation_ruleset - [ " << rulesetTriggeredList << "] - transaction id = " << messageMetaDataStruct.getTransactionId().toHexString();
        } else {
            LOG(INFO)<<"No rules triggered from auth_regulation_ruleset transaction id = " << messageMetaDataStruct.getTransactionId().toHexString();
        }
    }
    return folly::Unit();
}









#include "raven-engine/base/Artifact.h"





static bool module_auth_rules_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
      raven::base::Module::RegisterModule(
                const_cast<raven::base::ModuleInfo *>(raven::generated::module_auth_rules_V1::StaticGetModuleInfo()
        ));
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      raven::base::Module::UnRegisterModule(raven::generated::module_auth_rules_V1::StaticGetModuleInfo());
    }
    return true;
}

extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_auth_rules_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_auth_rules_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Module",     std::vector({    std::string("auth_rules"),
})
, 1, module_auth_rules_V1_StatusChangeCallBack, "module_auth_rules_V1");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("card_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("auth_ruleset_result"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("raven_gbm_model_output"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("auth_regulation_ruleset"),
})
, "Ruleset", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("account_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("auth_ruleset"),
})
, "Ruleset", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("account_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("txn_auth_input"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("card_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void module_auth_rules_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_auth_rules_V1());
     if(!artifact) return;
     module_auth_rules_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     module_auth_rules_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void module_auth_rules_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_auth_rules_V1());
    if(!artifact) return;
     module_auth_rules_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     module_auth_rules_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
