


#include "module_auth_response_V1.h"
#include "lib/include/custom_date.h"
#include <folly/GLog.h>

using namespace raven::types; // bring in the chrono operator


static std::unique_ptr<raven::base::Module> createNew_module_auth_response_V1(raven::base::Transaction *txn) {
    return std::make_unique<raven::generated::module_auth_response_V1>(txn);
}






static raven::base::ModuleInfo *GetModuleInfo_module_auth_response_V1() {
static raven::base::ModuleInfo moduleInfo = {
    "auth_response", // Module Name
    "VARIABLE", // Module Type
    // INPUT RECORDS
    {
          {"txn_auth_input","txn_auth_input",1,false},
          {"auth_ruleset_result","auth_ruleset_result",1,true},
          {"fraud_ruleset_result","fraud_ruleset_result",1,true},
    },
    // OUTPUT RECORDS
    {
        {"txn_auth_output","txn_auth_output",1,false},
    },
    1, // Module Version
    "", //resourceName - database name. Empty string if no database
      true, // is colocation relevant?
    true, // readOnly - no updates to database
    true, // isSlave ok?
    std::chrono::microseconds(0), // timeout
    std::chrono::microseconds(0), // roll up timeout
0, // max roll up count
    1, // module_hash
    createNew_module_auth_response_V1, // New default module
    std::make_shared<::raven::base::ModuleMetrics>("auth_response",1),
    nullptr, // module trace information
    google::WARNING, // module log severity
    nullptr
    };
    return &moduleInfo;
}

const raven::base::ModuleInfo *raven::generated::module_auth_response_V1::getModuleInfo() const {
    return GetModuleInfo_module_auth_response_V1();
}

const raven::base::ModuleInfo *raven::generated::module_auth_response_V1::StaticGetModuleInfo() {
return GetModuleInfo_module_auth_response_V1();
}

 bool raven::generated::module_auth_response_V1::setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
        std::string_view recordId,
        const flatbuffers::Table *record) {
    /* Will be used for remote executing a module */
using namespace std::string_view_literals;

    if(recordId == "txn_auth_input"sv) {
        txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "auth_ruleset_result"sv) {
        auth_ruleset_result = Getrecord_auth_ruleset_result_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "fraud_ruleset_result"sv) {
        fraud_ruleset_result = Getrecord_fraud_ruleset_result_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "txn_auth_output"sv) {
    RG_Ptr_txn_auth_output = std::move(*UnPackrecord_txn_auth_output_V1(Getrecord_txn_auth_output_V1(reinterpret_cast<const void *>(record))));
    flatbufferStorage.push_back(std::move(detachedBuffer));
        return true;
    }
return false;
}

uint64_t raven::generated::module_auth_response_V1::getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const {
using namespace std::string_view_literals;
    if(recordId == "txn_auth_output"sv) {
        auto offset = raven::generated::Createrecord_txn_auth_output_V1(fbb, txn_auth_output);
        return fbb.GetSize();
   }
        return 0;
}

std::pair<const uint8_t *, uint64_t> raven::generated::module_auth_response_V1::getRecordOutputPtr(std::string_view recordId)  {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "txn_auth_output"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_txn_auth_output_V1(fbb, txn_auth_output);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
    flatbufferStorage.push_back(std::move(detachedBuffer));
    return pair;
    }
return {nullptr,0};
}

std::unique_ptr<flatbuffers::DetachedBuffer> raven::generated::module_auth_response_V1::getRecordOutputDetachedPtr(std::string_view recordId) const {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "txn_auth_output"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_txn_auth_output_V1(fbb, txn_auth_output);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    return std::move(detachedBuffer);
    }
return nullptr;
}


std::pair<const uint8_t *, uint64_t> raven::generated::module_auth_response_V1::getRecordInputPtr(std::string_view recordId) {
            if(recordId == "txn_auth_input"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_txn_auth_input_V1(fbb, txn_auth_input->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "auth_ruleset_result"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_auth_ruleset_result_V1(fbb, auth_ruleset_result->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "fraud_ruleset_result"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_fraud_ruleset_result_V1(fbb, fraud_ruleset_result->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
    return {nullptr,0};
}









folly::Future<folly::Unit> raven::generated::module_auth_response_V1::run() {
      if(!(true)) {
        LOG(INFO)<<"Skipping module for transaction " << messageMetaDataStruct.getTransactionId().toHexString();
        return folly::Unit();
    }
    

  /* Update colocation logic */
  std::vector<std::string> keyColocation;
  /* No database - only local processing */
  rollupAtInit();
      rollupAtEnd();
  return folly::Unit();
}

void raven::generated::module_auth_response_V1::rollupAtInit() {
  /* begin code for txn_auth_output.transaction_date */
  fb_write_date(txn_auth_output,transaction_date) = fb_read_date(txn_auth_input,transaction_date);

/* end code for txn_auth_output.transaction_date*/

/* begin code for txn_auth_output.amount */
  fb_write(txn_auth_output,amount) = fb_read(txn_auth_input,amount);

/* end code for txn_auth_output.amount*/

/* begin code for txn_auth_output.trace_number */
  fb_write(txn_auth_output,trace_number) = fb_read(txn_auth_input,trace_number);

/* end code for txn_auth_output.trace_number*/

/* begin code for txn_auth_output.device_id */
  fb_write(txn_auth_output,device_id) = fb_read(txn_auth_input,device_id);

/* end code for txn_auth_output.device_id*/

/* begin code for txn_auth_output.card_number */
  fb_write(txn_auth_output,card_number) = fb_read(txn_auth_input,card_number);

/* end code for txn_auth_output.card_number*/

/* begin code for txn_auth_output.transaction_response */
  if (fb_read(auth_ruleset_result,auth_approve) == u8"Y"sv && fb_read(fraud_ruleset_result,fraud_approve) == u8"Y"sv) {
    fb_write(txn_auth_output,transaction_response) = u8"00"sv;
  } else {
    fb_write(txn_auth_output,transaction_response) = u8"01"sv;
  }

/* end code for txn_auth_output.transaction_response*/

/* begin code for txn_auth_output.transaction_time */
  fb_write_time(txn_auth_output,transaction_time) = fb_read_time(txn_auth_input,transaction_time);

/* end code for txn_auth_output.transaction_time*/

/* begin code for txn_auth_output.merchant_id */
  fb_write(txn_auth_output,merchant_id) = fb_read(txn_auth_input,merchant_id);

/* end code for txn_auth_output.merchant_id*/

/* begin code for txn_auth_output.ip_address */
  fb_write(txn_auth_output,ip_address) = fb_read(txn_auth_input,ip_address);

/* end code for txn_auth_output.ip_address*/

/* begin code for txn_auth_output.transaction_type */
  fb_write(txn_auth_output,transaction_type) = fb_read(txn_auth_input,transaction_type);

/* end code for txn_auth_output.transaction_type*/

/* begin code for txn_auth_output.currency_code */
  fb_write(txn_auth_output,currency_code) = fb_read(txn_auth_input,currency_code);

/* end code for txn_auth_output.currency_code*/

}

void raven::generated::module_auth_response_V1::rollupAtEnd() {
  
}









#include "raven-engine/base/Artifact.h"





static bool module_auth_response_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
      raven::base::Module::RegisterModule(
                const_cast<raven::base::ModuleInfo *>(raven::generated::module_auth_response_V1::StaticGetModuleInfo()
        ));
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      raven::base::Module::UnRegisterModule(raven::generated::module_auth_response_V1::StaticGetModuleInfo());
    }
    return true;
}

extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_auth_response_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_auth_response_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Module",     std::vector({    std::string("auth_response"),
})
, 1, module_auth_response_V1_StatusChangeCallBack, "module_auth_response_V1");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("auth_ruleset_result"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("fraud_ruleset_result"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("txn_auth_input"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("txn_auth_output"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void module_auth_response_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_auth_response_V1());
     if(!artifact) return;
     module_auth_response_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     module_auth_response_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void module_auth_response_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_auth_response_V1());
    if(!artifact) return;
     module_auth_response_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     module_auth_response_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
