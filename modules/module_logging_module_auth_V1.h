#pragma once





#include "raven-engine/base/Module.h"
#include "raven-engine/base/ModuleMetrics.h"
#include "raven-engine/base/RavenSwitches.h"
#include "folly/Conv.h"
#include "raven-engine/base/VirtualApplicationDB.h"
#include <rocksdb/db.h>
#include "spdlog/async_logger.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "../records/record_txn_auth_input_V1.h"

#include "raven-engine/common/Counter.h"

namespace raven::generated {

class module_logging_module_auth_V1 : public raven::base::Module {
public:
    base::ModuleInfo * moduleInfo = nullptr;
const record_txn_auth_input_V1 *txn_auth_input = nullptr;
int SG_txn_auth_input;

module_logging_module_auth_V1 () : Module(nullptr) {
    moduleInfo = const_cast<base::ModuleInfo *>(getModuleInfo());
}
const base::ModuleInfo *getModuleInfo() const override;
void init() override {
    if(txn_auth_input==nullptr) {
    txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_txn_auth_input_V1().first));
    }
return;
}
    bool setRecord(std::string_view recordId, const flatbuffers::Table *record) override {
using namespace std::string_view_literals;
if(recordId == "txn_auth_input"sv) {
    txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(record));
return true;
}
return false;
}

std::optional<uint64_t> colocationHash() override {
return std::optional<uint64_t>();
}
 bool setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
            std::string_view recordId,
            const flatbuffers::Table *record) override ;
 std::unique_ptr<flatbuffers::DetachedBuffer> getRecordOutputDetachedPtr(std::string_view recordId) const override;
 uint64_t getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const override;
 std::pair<const uint8_t *, uint64_t> getRecordOutputPtr(std::string_view recordId) override;
 std::pair<const uint8_t *, uint64_t> getRecordInputPtr(std::string_view recordId) override;

module_logging_module_auth_V1 (raven::base::Transaction *txn) : Module(txn) {
    moduleInfo = const_cast<base::ModuleInfo *>(getModuleInfo());
}
folly::Future<folly::Unit> run() override;

static const base::ModuleInfo * StaticGetModuleInfo();

};
}



