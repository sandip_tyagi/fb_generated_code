


#include "module_logging_module_auth_V1.h"
#include "lib/include/custom_date.h"
#include <folly/GLog.h>

using namespace raven::types; // bring in the chrono operator




#include <spdlog/async.h>
#include <spdlog/sinks/interval_file_sink.h>
#include <spdlog/logger.h>
#include <boost/filesystem.hpp>


static std::shared_ptr<spdlog::logger> getNewLogger();
static std::unique_ptr<raven::base::Module> createNew_module_logging_module_auth_V1(raven::base::Transaction *txn) {
    return std::make_unique<raven::generated::module_logging_module_auth_V1>(txn);
}






static raven::base::ModuleInfo *GetModuleInfo_module_logging_module_auth_V1() {
static raven::base::ModuleInfo moduleInfo = {
    "logging_module_auth", // Module Name
    "LOGGING", // Module Type
    // INPUT RECORDS
    {
          {"txn_auth_input","txn_auth_input",1,false},
    },
    // OUTPUT RECORDS
    {
    },
    1, // Module Version
    "", //resourceName - database name. Empty string if no database
      true, // is colocation relevant?
    true, // readOnly - no updates to database
    true, // isSlave ok?
    std::chrono::microseconds(0), // timeout
    std::chrono::microseconds(0), // roll up timeout
0, // max roll up count
    1, // module_hash
    createNew_module_logging_module_auth_V1, // New default module
    std::make_shared<::raven::base::ModuleMetrics>("logging_module_auth",1),
    nullptr, // module trace information
    google::WARNING, // module log severity
    getNewLogger() // Logger for logging module
    };
    return &moduleInfo;
}

const raven::base::ModuleInfo *raven::generated::module_logging_module_auth_V1::getModuleInfo() const {
    return GetModuleInfo_module_logging_module_auth_V1();
}

const raven::base::ModuleInfo *raven::generated::module_logging_module_auth_V1::StaticGetModuleInfo() {
return GetModuleInfo_module_logging_module_auth_V1();
}

 bool raven::generated::module_logging_module_auth_V1::setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
        std::string_view recordId,
        const flatbuffers::Table *record) {
    /* Will be used for remote executing a module */
using namespace std::string_view_literals;

    if(recordId == "txn_auth_input"sv) {
        txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
return false;
}

uint64_t raven::generated::module_logging_module_auth_V1::getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const {
using namespace std::string_view_literals;
        return 0;
}

std::pair<const uint8_t *, uint64_t> raven::generated::module_logging_module_auth_V1::getRecordOutputPtr(std::string_view recordId)  {
using namespace std::string_view_literals;
    /* No record as output */
return {nullptr,0};
}

std::unique_ptr<flatbuffers::DetachedBuffer> raven::generated::module_logging_module_auth_V1::getRecordOutputDetachedPtr(std::string_view recordId) const {
using namespace std::string_view_literals;
    /* No record as output */
return nullptr;
}


std::pair<const uint8_t *, uint64_t> raven::generated::module_logging_module_auth_V1::getRecordInputPtr(std::string_view recordId) {
            if(recordId == "txn_auth_input"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_txn_auth_input_V1(fbb, txn_auth_input->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
    return {nullptr,0};
}








static std::shared_ptr<spdlog::logger> getNewLogger() {
        try {
    using namespace std;
    boost::filesystem::path logPath = FLAGS_module_logging_directory;
    logPath /= "logging_module_auth";
    if (!boost::filesystem::exists(logPath)){
      boost::filesystem::create_directory(logPath);
    }
    if (!boost::filesystem::is_directory(logPath)) {
            LOG(ERROR) << "While trying to initialize a logger for logging_module_auth version - 1 - " << logPath
            << " is not a directory.";
            return nullptr;
    }
    logPath = logPath / "logging_module_auth_V1.log";
    auto rotating_logger =
    spdlog::interval_logger_mt<spdlog::async_factory>("MODULE_logging_module_auth_V1"s,
    logPath.string(),
    1,
    0);
    if(rotating_logger) {
        rotating_logger->set_pattern("%v");
        spdlog::flush_every(std::chrono::seconds(5));
    }

        return rotating_logger;
    } catch(std::exception &e) {
            LOG(ERROR)<<"ERROR while initializing logger "<<e.what();
            return nullptr;
    }
}




folly::Future<folly::Unit> raven::generated::module_logging_module_auth_V1::run() {
    if(!(true)) {
        LOG(INFO)<<"Skipping module for transaction " << messageMetaDataStruct.getTransactionId().toHexString();
        return folly::Unit();
    }
    

nlohmann::json log = nlohmann::json::object();
log["RAVEN_LOGGING_TIME"] = std::chrono::system_clock::now();
log["RAVEN_TRANSACTION_ID"] = messageMetaDataStruct.getTransactionId().toHexString();
    {
    const auto pRecordInfo = raven::base::LookupRecord("txn_auth_input", 1);
    if (pRecordInfo && txn_auth_input) {
            if(pRecordInfo->toJSON){
              try{
                log["txn_auth_input"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(txn_auth_input));
                {
                  // Padding specified for txn_auth_input.card_number
                    auto currVar=log["txn_auth_input"].find("card_number");
                    if(currVar!=log["txn_auth_input"].end())
                        raven::types::right_pad(currVar.value(),16,'0' );
                }


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for txn_auth_input - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["txn_auth_input"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of txn_auth_input for version " << txn_auth_input->raven_header()->version()
                    << ". Logging from module logging_module_auth is incomplete.";
          }
    } else {
       log["txn_auth_input"] = nullptr;
    }
    }
const auto logger = getModuleInfo()->logger;
if(!logger) {
    FB_LOG_EVERY_MS(WARNING, 1000) << "No logger found for module " << getModuleInfo()->module_name << " version "
    << getModuleInfo()->version << " Trying again - success = "
    << static_cast<bool>(const_cast<raven::base::ModuleInfo *>(getModuleInfo())->logger =
    getNewLogger());
}
if (logger) {
   logger->info(log.flatten().dump(-1,' ', false, nlohmann::json::error_handler_t::replace));
}
return folly::Unit();
}









#include "raven-engine/base/Artifact.h"





static bool module_logging_module_auth_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
      raven::base::Module::RegisterModule(
                const_cast<raven::base::ModuleInfo *>(raven::generated::module_logging_module_auth_V1::StaticGetModuleInfo()
        ));
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      raven::base::Module::UnRegisterModule(raven::generated::module_logging_module_auth_V1::StaticGetModuleInfo());
    }
    return true;
}

extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_logging_module_auth_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_logging_module_auth_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Module",     std::vector({    std::string("logging_module_auth"),
})
, 1, module_logging_module_auth_V1_StatusChangeCallBack, "module_logging_module_auth_V1");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("txn_auth_input"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void module_logging_module_auth_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_logging_module_auth_V1());
     if(!artifact) return;
     module_logging_module_auth_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     module_logging_module_auth_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void module_logging_module_auth_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_logging_module_auth_V1());
    if(!artifact) return;
     module_logging_module_auth_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     module_logging_module_auth_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
