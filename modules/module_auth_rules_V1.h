#pragma once





#include "raven-engine/base/Module.h"
#include "raven-engine/base/ModuleMetrics.h"
#include "raven-engine/base/RavenSwitches.h"
#include "folly/Conv.h"
#include "raven-engine/base/VirtualApplicationDB.h"
#include <rocksdb/db.h>
#include "../records/record_txn_auth_input_V1.h"
#include "../records/record_card_data_V1.h"
#include "../records/record_account_data_V1.h"
#include "../records/record_customer_data_V1.h"
#include "../records/record_account_level_rollup_V1.h"
#include "../records/record_card_level_rollup_V1.h"
#include "../records/record_customer_level_rollup_V1.h"
#include "../records/record_raven_gbm_model_output_V1.h"
#include "../records/record_auth_ruleset_result_V1.h"

#include "raven-engine/common/Counter.h"

namespace raven::generated {

class module_auth_rules_V1 : public raven::base::Module {
public:
    base::ModuleInfo * moduleInfo = nullptr;
const record_txn_auth_input_V1 *txn_auth_input = nullptr;
int SG_txn_auth_input;
const record_card_data_V1 *card_data = nullptr;
int SG_card_data;
const record_account_data_V1 *account_data = nullptr;
int SG_account_data;
const record_customer_data_V1 *customer_data = nullptr;
int SG_customer_data;
const record_account_level_rollup_V1 *account_level_rollup = nullptr;
int SG_account_level_rollup;
const record_card_level_rollup_V1 *card_level_rollup = nullptr;
int SG_card_level_rollup;
const record_customer_level_rollup_V1 *customer_level_rollup = nullptr;
int SG_customer_level_rollup;
const record_raven_gbm_model_output_V1 *auth_model_score = nullptr;
int SG_auth_model_score;
RG_record_auth_ruleset_result_V1 RG_Ptr_auth_ruleset_result;
RG_record_auth_ruleset_result_V1 *auth_ruleset_result = &RG_Ptr_auth_ruleset_result;
// flatbuffers::DetachedBuffer BUFFER_auth_ruleset_result;
const record_auth_ruleset_result_V1 *OUT_auth_ruleset_result = nullptr;

module_auth_rules_V1 () : Module(nullptr) {
    moduleInfo = const_cast<base::ModuleInfo *>(getModuleInfo());
}
const base::ModuleInfo *getModuleInfo() const override;
void init() override {
    if(txn_auth_input==nullptr) {
    txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_txn_auth_input_V1().first));
    }
    if(card_data==nullptr) {
    card_data = Getrecord_card_data_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_card_data_V1().first));
    }
    if(account_data==nullptr) {
    account_data = Getrecord_account_data_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_account_data_V1().first));
    }
    if(customer_data==nullptr) {
    customer_data = Getrecord_customer_data_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_customer_data_V1().first));
    }
    if(account_level_rollup==nullptr) {
    account_level_rollup = Getrecord_account_level_rollup_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_account_level_rollup_V1().first));
    }
    if(card_level_rollup==nullptr) {
    card_level_rollup = Getrecord_card_level_rollup_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_card_level_rollup_V1().first));
    }
    if(customer_level_rollup==nullptr) {
    customer_level_rollup = Getrecord_customer_level_rollup_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_customer_level_rollup_V1().first));
    }
    if(auth_model_score==nullptr) {
    auth_model_score = Getrecord_raven_gbm_model_output_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_raven_gbm_model_output_V1().first));
    }
return;
}
    bool setRecord(std::string_view recordId, const flatbuffers::Table *record) override {
using namespace std::string_view_literals;
if(recordId == "txn_auth_input"sv) {
    txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(record));
return true;
}
if(recordId == "card_data"sv) {
    card_data = Getrecord_card_data_V1(reinterpret_cast<const void *>(record));
return true;
}
if(recordId == "account_data"sv) {
    account_data = Getrecord_account_data_V1(reinterpret_cast<const void *>(record));
return true;
}
if(recordId == "customer_data"sv) {
    customer_data = Getrecord_customer_data_V1(reinterpret_cast<const void *>(record));
return true;
}
if(recordId == "account_level_rollup"sv) {
    account_level_rollup = Getrecord_account_level_rollup_V1(reinterpret_cast<const void *>(record));
return true;
}
if(recordId == "card_level_rollup"sv) {
    card_level_rollup = Getrecord_card_level_rollup_V1(reinterpret_cast<const void *>(record));
return true;
}
if(recordId == "customer_level_rollup"sv) {
    customer_level_rollup = Getrecord_customer_level_rollup_V1(reinterpret_cast<const void *>(record));
return true;
}
if(recordId == "auth_model_score"sv) {
    auth_model_score = Getrecord_raven_gbm_model_output_V1(reinterpret_cast<const void *>(record));
return true;
}
return false;
}

std::optional<uint64_t> colocationHash() override {
return std::optional<uint64_t>();
}
 bool setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
            std::string_view recordId,
            const flatbuffers::Table *record) override ;
 std::unique_ptr<flatbuffers::DetachedBuffer> getRecordOutputDetachedPtr(std::string_view recordId) const override;
 uint64_t getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const override;
 std::pair<const uint8_t *, uint64_t> getRecordOutputPtr(std::string_view recordId) override;
 std::pair<const uint8_t *, uint64_t> getRecordInputPtr(std::string_view recordId) override;

module_auth_rules_V1 (raven::base::Transaction *txn) : Module(txn) {
    moduleInfo = const_cast<base::ModuleInfo *>(getModuleInfo());
}
folly::Future<folly::Unit> run() override;

static const base::ModuleInfo * StaticGetModuleInfo();

};
}
