#pragma once





#include "raven-engine/base/Module.h"
#include "raven-engine/base/ModuleMetrics.h"
#include "raven-engine/base/RavenSwitches.h"
#include "folly/Conv.h"
#include "raven-engine/base/VirtualApplicationDB.h"
#include <rocksdb/db.h>
#include "../records/record_txn_auth_input_V1.h"
#include "../records/record_auth_ruleset_result_V1.h"
#include "../records/record_fraud_ruleset_result_V1.h"
#include "../records/record_txn_auth_output_V1.h"

#include "raven-engine/common/Counter.h"

namespace raven::generated {

class module_auth_response_V1 : public raven::base::Module {
public:
    base::ModuleInfo * moduleInfo = nullptr;
const record_txn_auth_input_V1 *txn_auth_input = nullptr;
int SG_txn_auth_input;
const record_auth_ruleset_result_V1 *auth_ruleset_result = nullptr;
int SG_auth_ruleset_result;
const record_fraud_ruleset_result_V1 *fraud_ruleset_result = nullptr;
int SG_fraud_ruleset_result;
RG_record_txn_auth_output_V1 RG_Ptr_txn_auth_output;
RG_record_txn_auth_output_V1 *txn_auth_output = &RG_Ptr_txn_auth_output;
// flatbuffers::DetachedBuffer BUFFER_txn_auth_output;
const record_txn_auth_output_V1 *OUT_txn_auth_output = nullptr;

module_auth_response_V1 () : Module(nullptr) {
    moduleInfo = const_cast<base::ModuleInfo *>(getModuleInfo());
}
const base::ModuleInfo *getModuleInfo() const override;
void init() override {
    if(txn_auth_input==nullptr) {
    txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_txn_auth_input_V1().first));
    }
    if(auth_ruleset_result==nullptr) {
    auth_ruleset_result = Getrecord_auth_ruleset_result_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_auth_ruleset_result_V1().first));
    }
    if(fraud_ruleset_result==nullptr) {
    fraud_ruleset_result = Getrecord_fraud_ruleset_result_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_fraud_ruleset_result_V1().first));
    }
return;
}
    bool setRecord(std::string_view recordId, const flatbuffers::Table *record) override {
using namespace std::string_view_literals;
if(recordId == "txn_auth_input"sv) {
    txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(record));
return true;
}
if(recordId == "auth_ruleset_result"sv) {
    auth_ruleset_result = Getrecord_auth_ruleset_result_V1(reinterpret_cast<const void *>(record));
return true;
}
if(recordId == "fraud_ruleset_result"sv) {
    fraud_ruleset_result = Getrecord_fraud_ruleset_result_V1(reinterpret_cast<const void *>(record));
return true;
}
return false;
}

std::optional<uint64_t> colocationHash() override {
return std::optional<uint64_t>();
}
 bool setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
            std::string_view recordId,
            const flatbuffers::Table *record) override ;
 std::unique_ptr<flatbuffers::DetachedBuffer> getRecordOutputDetachedPtr(std::string_view recordId) const override;
 uint64_t getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const override;
 std::pair<const uint8_t *, uint64_t> getRecordOutputPtr(std::string_view recordId) override;
 std::pair<const uint8_t *, uint64_t> getRecordInputPtr(std::string_view recordId) override;

module_auth_response_V1 (raven::base::Transaction *txn) : Module(txn) {
    moduleInfo = const_cast<base::ModuleInfo *>(getModuleInfo());
}
folly::Future<folly::Unit> run() override;

static const base::ModuleInfo * StaticGetModuleInfo();








void rollupAtInit();


void rollupAtEnd();
};
}
