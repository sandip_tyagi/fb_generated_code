


#include "module_merchant_transaction_history_rollup_V1.h"
#include "lib/include/custom_date.h"
#include <folly/GLog.h>

using namespace raven::types; // bring in the chrono operator


static std::unique_ptr<raven::base::Module> createNew_module_merchant_transaction_history_rollup_V1(raven::base::Transaction *txn) {
    return std::make_unique<raven::generated::module_merchant_transaction_history_rollup_V1>(txn);
}






static raven::base::ModuleInfo *GetModuleInfo_module_merchant_transaction_history_rollup_V1() {
static raven::base::ModuleInfo moduleInfo = {
    "merchant_transaction_history_rollup", // Module Name
    "VARIABLE", // Module Type
    // INPUT RECORDS
    {
          {"txn_auth_input","txn_auth_input",1,false},
          {"account_data","account_data",1,false},
    },
    // OUTPUT RECORDS
    {
        {"merchant_level_rollup","merchant_level_rollup",1,false},
    },
    1, // Module Version
    "merchant_database", //resourceName - database name. Empty string if no database
      true, // is colocation relevant?
    true, // readOnly - no updates to database
    true, // isSlave ok?
    std::chrono::microseconds(0), // timeout
    std::chrono::microseconds(0), // roll up timeout
10000, // max roll up count
    1, // module_hash
    createNew_module_merchant_transaction_history_rollup_V1, // New default module
    std::make_shared<::raven::base::ModuleMetrics>("merchant_transaction_history_rollup",1),
    nullptr, // module trace information
    google::WARNING, // module log severity
    nullptr
    };
    return &moduleInfo;
}

const raven::base::ModuleInfo *raven::generated::module_merchant_transaction_history_rollup_V1::getModuleInfo() const {
    return GetModuleInfo_module_merchant_transaction_history_rollup_V1();
}

const raven::base::ModuleInfo *raven::generated::module_merchant_transaction_history_rollup_V1::StaticGetModuleInfo() {
return GetModuleInfo_module_merchant_transaction_history_rollup_V1();
}

 bool raven::generated::module_merchant_transaction_history_rollup_V1::setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
        std::string_view recordId,
        const flatbuffers::Table *record) {
    /* Will be used for remote executing a module */
using namespace std::string_view_literals;

    if(recordId == "txn_auth_input"sv) {
        txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "account_data"sv) {
        account_data = Getrecord_account_data_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "merchant_level_rollup"sv) {
    RG_Ptr_merchant_level_rollup = std::move(*UnPackrecord_merchant_level_rollup_V1(Getrecord_merchant_level_rollup_V1(reinterpret_cast<const void *>(record))));
    flatbufferStorage.push_back(std::move(detachedBuffer));
        return true;
    }
return false;
}

uint64_t raven::generated::module_merchant_transaction_history_rollup_V1::getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const {
using namespace std::string_view_literals;
    if(recordId == "merchant_level_rollup"sv) {
        auto offset = raven::generated::Createrecord_merchant_level_rollup_V1(fbb, merchant_level_rollup);
        return fbb.GetSize();
   }
        return 0;
}

std::pair<const uint8_t *, uint64_t> raven::generated::module_merchant_transaction_history_rollup_V1::getRecordOutputPtr(std::string_view recordId)  {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "merchant_level_rollup"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_merchant_level_rollup_V1(fbb, merchant_level_rollup);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
    flatbufferStorage.push_back(std::move(detachedBuffer));
    return pair;
    }
return {nullptr,0};
}

std::unique_ptr<flatbuffers::DetachedBuffer> raven::generated::module_merchant_transaction_history_rollup_V1::getRecordOutputDetachedPtr(std::string_view recordId) const {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "merchant_level_rollup"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_merchant_level_rollup_V1(fbb, merchant_level_rollup);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    return std::move(detachedBuffer);
    }
return nullptr;
}


std::pair<const uint8_t *, uint64_t> raven::generated::module_merchant_transaction_history_rollup_V1::getRecordInputPtr(std::string_view recordId) {
            if(recordId == "txn_auth_input"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_txn_auth_input_V1(fbb, txn_auth_input->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "account_data"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_account_data_V1(fbb, account_data->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
    return {nullptr,0};
}









folly::Future<folly::Unit> raven::generated::module_merchant_transaction_history_rollup_V1::run() {
      if(!(true)) {
        LOG(INFO)<<"Skipping module for transaction " << messageMetaDataStruct.getTransactionId().toHexString();
        return folly::Unit();
    }
    

  /* Update colocation logic */
  std::vector<std::string> keyColocation;
  database_record_dbr_merchant_txn_history::Colocation merchant_txn_historyColocation = {};
  {
      database_record_dbr_merchant_txn_history::Colocation * colocation= &merchant_txn_history_Key.colocation;
        fb_write(colocation,colocation_0) = fb_read(txn_auth_input,merchant_id);

      colocation->normalize();
      keyColocation.emplace_back(reinterpret_cast<const char *>(colocation),sizeof(merchant_txn_history_Key.colocation));
      if(keyColocation[keyColocation.size()-1]!=keyColocation[0]) {
        LOG(ERROR) << "Colocation keys are not matching for all database records in merchant_transaction_history_rollup transaction id " << messageMetaDataStruct.getTransactionId().toHexString();
        throw std::runtime_error("Colocation keys are not matching for all database records in merchant_transaction_history_rollup transaction id " + messageMetaDataStruct.getTransactionId().toHexString());
      }
      merchant_txn_historyColocation = *colocation;
  }
  std::shared_ptr<raven::base::VirtualApplicationDB> db = GetResourceAppDBPointerUseCase("merchant_database", keyColocation[0], messageMetaDataStruct.getTransactionId(), admin::DBUseCase::Read);
  if(db) {
  // Local processing
  rollupAtInit();

      database_record_dbr_merchant_txn_history::Key dbStartKey = {};
      database_record_dbr_merchant_txn_history::Key dbEndKey = {};
      dbStartKey.colocation = merchant_txn_historyColocation;
      database_record_dbr_merchant_txn_history::Prefix * prefix = &dbStartKey.prefix;
      database_record_dbr_merchant_txn_history::Sort * sort_key_begin = &dbStartKey.sort;
      database_record_dbr_merchant_txn_history::Sort SortKeyEnd = {};
      database_record_dbr_merchant_txn_history::Sort * sort_key_end = &dbEndKey.sort;

      {  // Current transaction logic
          RG_record_merchant_txn_history_V1 rollUpRecord = {};
          RG_record_merchant_txn_history_V1 *merchant_txn_history = &rollUpRecord;
            fb_write(merchant_txn_history,account_number) = fb_read(account_data,account_number);
  fb_write(merchant_txn_history,amount) = fb_read(txn_auth_input,amount);
  fb_write(merchant_txn_history,auth_score) = raven::missing;
  fb_write(merchant_txn_history,card_number) = fb_read(txn_auth_input,card_number);
  fb_write(merchant_txn_history,currency_code) = fb_read(txn_auth_input,currency_code);
  fb_write(merchant_txn_history,customer_number) = fb_read(account_data,customer_number);
  fb_write(merchant_txn_history,device_id) = fb_read(txn_auth_input,device_id);
  fb_write(merchant_txn_history,fraud_score) = raven::missing;
  fb_write(merchant_txn_history,ip_address) = fb_read(txn_auth_input,ip_address);
  fb_write(merchant_txn_history,merchant_id) = fb_read(txn_auth_input,merchant_id);
  fb_write(merchant_txn_history,trace_number) = fb_read(txn_auth_input,trace_number);
  fb_write_date(merchant_txn_history,transaction_date) = fb_read_date(txn_auth_input,transaction_date);
  fb_write(merchant_txn_history,transaction_decision) = raven::missing;
  fb_write_time(merchant_txn_history,transaction_time) = fb_read_time(txn_auth_input,transaction_time);
  fb_write(merchant_txn_history,transaction_type) = fb_read(txn_auth_input,transaction_type);
  fb_write(merchant_txn_history,zip_code) = fb_read(txn_auth_input,zip_code);

          VLOG(3)<<"Current Transaction being processed through roll-up logic " << messageMetaDataStruct.getTransactionId().toHexString();
          flatbuffers::FlatBufferBuilder fbb;
          auto merchant_txn_history_offset = raven::generated::Createrecord_merchant_txn_history_V1(fbb, merchant_txn_history);
          fbb.Finish(merchant_txn_history_offset);
          auto merchant_txn_history_ptr = raven::generated::Getrecord_merchant_txn_history_V1(fbb.GetBufferPointer());
          rollupEachCompute(merchant_txn_history_ptr,true);
      } // End current transaction logic
      // Key setting logic from blockly
        fb_write(prefix,merchant_id) = fb_read(txn_auth_input,merchant_id);
  fb_write_date(sort_key_begin,transaction_date) = raven::missing;
  fb_write_time(sort_key_begin,transaction_time) = raven::missing;
  fb_write_date(sort_key_end,transaction_date) = raven::missing;
  fb_write_time(sort_key_end,transaction_time) = raven::missing;

      // End key setting logic from blockly
      dbStartKey.normalize();

      auto rollUpDB = db->getDB();
      if(!rollUpDB) throw std::runtime_error("No database found for rollup db " + messageMetaDataStruct.getTransactionId().toHexString());
      auto cfHandle = db->getColumnFamilyHandle("dbr_merchant_txn_history");
      if(cfHandle==nullptr) throw std::runtime_error("No column family dbr_merchant_txn_history found " + messageMetaDataStruct.getTransactionId().toHexString());
      const raven::base::RecordInfo *recordInfo = raven::base::LookupRecord("merchant_txn_history",
      1);
      rocksdb::ReadOptions readOptions;
      readOptions.prefix_same_as_start = true;
      rocksdb::Slice upperBound;
      if(!dbEndKey.sort.isEmpty()) {
          dbEndKey.colocation = dbStartKey.colocation;
          dbEndKey.prefix = dbStartKey.prefix;
          dbEndKey.sort.normalize();
          upperBound = rocksdb::Slice(reinterpret_cast<const char *>(&dbEndKey), sizeof(dbEndKey));
/*          if constexpr (DCHECK_IS_ON()){
            auto databaseRecord = raven::base::DatabaseRecord::GetDatabaseRecord("merchant_database", "dbr_merchant_txn_history");
            if(databaseRecord){
                DLOG(INFO)<<"Upper bound set to " <<upperBound.ToString(true) << " " <<
                    databaseRecord->GetJsonFromKey(upperBound).dump();
            }
          }
*/
          readOptions.iterate_upper_bound = &upperBound;
      }

      /* Now let us setup the database iterator */
      std::unique_ptr<rocksdb::Iterator> dbIter(rollUpDB->NewIterator(readOptions, cfHandle));
      if(!dbStartKey.sort.isEmpty()){
        dbIter->Seek(rocksdb::Slice(reinterpret_cast<const char*>(&dbStartKey),sizeof(dbStartKey)));
      } else {
        dbIter->Seek(rocksdb::Slice(reinterpret_cast<const char*>(&dbStartKey),sizeof(dbStartKey.colocation)+sizeof(dbStartKey.prefix)));
      }
      uint32_t rollUpCount = 0;
      bool errorPrinted = false;
      auto rollUpStartTime = std::chrono::system_clock::now();
      while(dbIter->Valid()) {
              // Process the current record
              //
              //
              rollUpCount++;
/*              if constexpr (DCHECK_IS_ON()) {
                  auto databaseRecord = raven::base::DatabaseRecord::GetDatabaseRecord("merchant_database", "dbr_merchant_txn_history");
                  if(databaseRecord) {
                      DLOG(INFO) <<"RollUp record key read " <<dbIter->key().ToString(true) << " " <<
                        databaseRecord->GetJsonFromKey(dbIter->key()).dump();
                  }
              }
*/

              const raven::base::fbs::GenericRecordHeaderOverlay * dbReadRecord = raven::base::fbs::GetGenericRecordHeaderOverlay(dbIter->value().data());
              if(dbReadRecord->raven_header()->version()==1) {
                      VLOG(4)<<"Read roll-up record " << messageMetaDataStruct.getTransactionId().toHexString() << " - " << recordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(dbReadRecord));
                      rollupEachCompute(reinterpret_cast<const raven::generated::record_merchant_txn_history_V1 *>(dbReadRecord),false);
              } else if(dbReadRecord->raven_header()->version() < 1) {
//                      std::string value(dbIter->value().data(),dbIter->value().size());
//                      auto dbRec = reinterpret_cast<raven::base::Record *>(value.data());
//                      dbRec->size = static_cast<uint32_t>(dbIter->value().size());
                      try {
                        flatbuffers::FlatBufferBuilder fbb;
                        flatbuffers::uoffset_t upgradedDBRecord = recordInfo->upgraderFunctions.at(dbReadRecord->raven_header()->version())(fbb,reinterpret_cast<const flatbuffers::Table*>(dbReadRecord));
                        fbb.Finish(flatbuffers::Offset<raven::generated::record_merchant_txn_history_V1>(upgradedDBRecord));
//                        rollupEachCompute(static_cast<const raven::generated::record_merchant_txn_history_V1 *>(upgradedDBRecord.get()),false);
                        rollupEachCompute(raven::generated::Getrecord_merchant_txn_history_V1(fbb.GetBufferPointer()),false);
                      } catch(std::exception &e) {
                        if(!errorPrinted) {
                            LOG(WARNING)<<"Got an error while trying to upgrade " << e.what() << " ignoring and continuing."<<messageMetaDataStruct.getTransactionId().toHexString();
                            errorPrinted = true;
                        }
                      }
              } else {
                      std::string value(dbIter->value().data(),dbIter->value().size());
//                      auto dbRec = reinterpret_cast<raven::base::Record *>(value.data());
//                      dbRec->size = static_cast<uint32_t>(dbIter->value().size());
                      auto recordInfoDowngrade = raven::base::LookupRecord("merchant_txn_history",dbReadRecord->raven_header()->version());
                      try {
                        flatbuffers::FlatBufferBuilder fbb;
                        flatbuffers::uoffset_t downgradedDBRecord = recordInfoDowngrade->downgraderFunctions.at(1 /* version wanted */)(fbb,reinterpret_cast<const flatbuffers::Table*>(dbReadRecord));
//                        auto downgradedDBRecord = recordInfoDowngrade->downgraderFunctions.at(1 /* version wanted */)(fbb,dbRec);
                        fbb.Finish(flatbuffers::Offset<raven::generated::record_merchant_txn_history_V1>(downgradedDBRecord));
                        rollupEachCompute(raven::generated::Getrecord_merchant_txn_history_V1(fbb.GetBufferPointer()),false);
//                        rollupEachCompute(static_cast<const raven::generated::record_merchant_txn_history_V1 *>(downgradedDBRecord.get()),false);
                      } catch(std::exception &e) {
                        if(!errorPrinted) {
                            LOG(WARNING)<<"Got an error while trying to downgrade " << e.what() << " ignoring and continuing. "<<messageMetaDataStruct.getTransactionId().toHexString();
                            errorPrinted = true;
                        }
                      }
              }
              if(rollUpCount == 10000 || timedout) {
                ++moduleInfo->metrics->rollUpMaxCountExceededCount;
                LOG(INFO) << "Max roll up count reached 10000 " << messageMetaDataStruct.getTransactionId().toHexString() << " timedout = " << timedout;
                if(timedout) return folly::Unit();
                break;
              }
              if(exceededMaxTime) break;
              if( (rollUpCount % 0x4000) == 0) {
                  LOG(WARNING) << "RollUp count exceeded " << rollUpCount <<" for " << messageMetaDataStruct.getTransactionId().toHexString();
              }
              dbIter->Next();
      }
      auto rollUpEndTime = std::chrono::system_clock::now();
      LOG(INFO) << "RollUp records processed " << rollUpCount << ' ' << messageMetaDataStruct.getTransactionId().toHexString()
      << " roll up time taken : " << std::chrono::duration_cast<std::chrono::microseconds>(rollUpEndTime-rollUpStartTime).count() << " us exceededMaxTime = " << exceededMaxTime;
      rollupAtEnd();
  } else {
  // Remote processing
      flatbuffers::FlatBufferBuilder fbb;
      auto smInput = serializeModuleInputFB(fbb);
      fbb.Finish(smInput);
      auto payload = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
      std::weak_ptr<raven::base::Transaction> wpTxn;
      if(transaction) {
          wpTxn = transaction->weak_from_this();
      }
      LOG(INFO) << "Sending module for remote processing " << messageMetaDataStruct.getTransactionId().toHexString() << " transaction shared_ptr = " << !!transaction;
      return SendPayloadToRemoteDBResource("module", "merchant_database",keyColocation[0],std::move(payload), messageMetaDataStruct).thenValue([this,transaction = (transaction!=nullptr), wpTxn = std::move(wpTxn)](std::unique_ptr<flatbuffers::DetachedBuffer> response) -> folly::Future<folly::Unit> {
        std::shared_ptr<raven::base::Transaction> sp;
        if(transaction) {
            sp = wpTxn.lock();
            if(!sp) return folly::make_exception_wrapper<std::runtime_error>("Transaction no longer exists");
        }
        LOG(INFO) << "Response received from remote node" << messageMetaDataStruct.getTransactionId().toHexString() << " transaction shared_ptr = " << !!sp;
        std::lock_guard lg(responseLock);
        if(!timedout) {
            this->deserializeModuleOutput(this,std::move(response));
                flatbuffers::FlatBufferBuilder fbb_merchant_level_rollup;
          auto offset_merchant_level_rollup = raven::generated::Createrecord_merchant_level_rollup_V1(fbb_merchant_level_rollup, merchant_level_rollup);
          fbb_merchant_level_rollup.Finish(offset_merchant_level_rollup);
          auto BUFFER_merchant_level_rollup = fbb_merchant_level_rollup.Release();
          OUT_merchant_level_rollup = raven::generated::Getrecord_merchant_level_rollup_V1(BUFFER_merchant_level_rollup.data());

        }
        return folly::Unit();
      });
  }
  return folly::Unit();
}

void raven::generated::module_merchant_transaction_history_rollup_V1::rollupAtInit() {
  /* merchant_level_rollup.unique_cards_10min */ 
/* VARIABLE AGGREGATION INIT */

}
void raven::generated::module_merchant_transaction_history_rollup_V1::rollupEachCompute(
    const raven::generated::record_merchant_txn_history_V1
    *merchant_txn_history, bool currentTxn [[maybe_unused]]) {
    if(merchant_txn_history==nullptr) throw std::runtime_error("Could not execute rollupEach compute " + messageMetaDataStruct.getTransactionId().toHexString());

    struct TD {
        long current_transaction = raven::missing;                  // raven::types::raven_sys_date_time
        long accum_transaction = raven::missing;                    // raven::types::raven_sys_date_time
        long time_difference = raven::missing;                      // std::chrono::duration<double,std::milli>
        float time_difference_calendar_seconds = raven::missing;
        float time_difference_calendar_minutes = raven::missing;
        float time_difference_calendar_hours = raven::missing;
        float time_difference_calendar_days = raven::missing;
        float time_difference_calendar_months = raven::missing;
        float time_difference_calendar_years = raven::missing;
    } TimeDifference;
    TD *time_difference = &TimeDifference;
      fb_write_date_time(time_difference,current_transaction) = (fb_read_date(txn_auth_input,transaction_date) + fb_read_time(txn_auth_input,transaction_time));
  fb_write_date_time(time_difference,accum_transaction) = (fb_read_date(merchant_txn_history,transaction_date) + fb_read_time(merchant_txn_history,transaction_time));


    if(fb_read_date_time(time_difference,current_transaction) != raven::missing && fb_read_date_time(time_difference,accum_transaction) != raven::missing) {
    auto acmDays = std::chrono::floor<date::days>(fb_read_date_time(time_difference,accum_transaction));
    auto acmYmd = date::year_month_day{acmDays};
    auto curDays = std::chrono::floor<date::days>(fb_read_date_time(time_difference,current_transaction));
    auto curYmd = date::year_month_day{curDays};
    time_difference->time_difference_calendar_seconds = (std::chrono::floor<std::chrono::seconds>(fb_read_date_time(time_difference,current_transaction)) - std::chrono::floor<std::chrono::seconds>(fb_read_date_time(time_difference,accum_transaction))).count();
    time_difference->time_difference_calendar_minutes = (std::chrono::floor<std::chrono::minutes>(fb_read_date_time(time_difference,current_transaction)) - std::chrono::floor<std::chrono::minutes>(fb_read_date_time(time_difference,accum_transaction))).count();
    time_difference->time_difference_calendar_hours = (std::chrono::floor<std::chrono::hours>(fb_read_date_time(time_difference,current_transaction)) - std::chrono::floor<std::chrono::hours>(fb_read_date_time(time_difference,accum_transaction))).count();
    time_difference->time_difference_calendar_years = (curYmd.year()-acmYmd.year()).count();
    time_difference->time_difference_calendar_months = static_cast<unsigned int>(curYmd.month())+0L-static_cast<unsigned int>(acmYmd.month()) + time_difference->time_difference_calendar_years*12;
    time_difference->time_difference_calendar_days = (curDays-acmDays).count();
    fb_write_time(time_difference,time_difference) = fb_read_date_time(time_difference,current_transaction)-fb_read_date_time(time_difference,accum_transaction);
    } else {

    fb_write_time(time_difference,time_difference) =  std::chrono::milliseconds(0);
    }


    
exceededMaxTime = true;
if(fb_read_time(time_difference,time_difference) < std::chrono::seconds(600)) exceededMaxTime = false;
if((!currentTxn)&&(fb_read_time(time_difference,time_difference) > std::chrono::seconds(0) && fb_read_time(time_difference,time_difference) < std::chrono::seconds(600))) {
if((true)) {
/* merchant_level_rollup.unique_cards_10min */
  /* VARIABLE AGGREGATION COMPUTE */
 /* COUNT UNIQUE */
temp_variable_100.insert(value_for_insert_to_map(fb_read(merchant_txn_history,card_number)));
}
}
}

void raven::generated::module_merchant_transaction_history_rollup_V1::rollupAtEnd() {
  /* merchant_level_rollup.unique_cards_10min */ 
/* VARIABLE AGGREGATION FINAL */ 
/* merchant_level_rollup.unique_cards_10min */ 
   fb_write(merchant_level_rollup,unique_cards_10min) = (/* COUNT UNIQUE */
  /* TODO  temp_variable_100 count_unique*/ temp_variable_100.size()
  /* TODO  count_unique*/
  );

}









#include "raven-engine/base/Artifact.h"





static bool module_merchant_transaction_history_rollup_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
      raven::base::Module::RegisterModule(
                const_cast<raven::base::ModuleInfo *>(raven::generated::module_merchant_transaction_history_rollup_V1::StaticGetModuleInfo()
        ));
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      raven::base::Module::UnRegisterModule(raven::generated::module_merchant_transaction_history_rollup_V1::StaticGetModuleInfo());
    }
    return true;
}

extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_merchant_transaction_history_rollup_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_merchant_transaction_history_rollup_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Module",     std::vector({    std::string("merchant_transaction_history_rollup"),
})
, 1, module_merchant_transaction_history_rollup_V1_StatusChangeCallBack, "module_merchant_transaction_history_rollup_V1");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("account_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("merchant_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("merchant_database"),
    std::string("dbr_merchant_txn_history"),
})
, "DatabaseRecord", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("txn_auth_input"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("merchant_database"),
})
, "Database", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void module_merchant_transaction_history_rollup_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_merchant_transaction_history_rollup_V1());
     if(!artifact) return;
     module_merchant_transaction_history_rollup_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     module_merchant_transaction_history_rollup_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void module_merchant_transaction_history_rollup_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_merchant_transaction_history_rollup_V1());
    if(!artifact) return;
     module_merchant_transaction_history_rollup_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     module_merchant_transaction_history_rollup_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
