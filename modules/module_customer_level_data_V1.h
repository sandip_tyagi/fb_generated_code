#pragma once





#include "raven-engine/base/Module.h"
#include "raven-engine/base/ModuleMetrics.h"
#include "raven-engine/base/RavenSwitches.h"
#include "folly/Conv.h"
#include "raven-engine/base/VirtualApplicationDB.h"
#include <rocksdb/db.h>
#include "../records/record_card_data_V1.h"
#include "../records/record_customer_data_V1.h"
#include "../database_records/database_record_customer_database_dbr_customer_data.h"
#include "../records/record_customer_data_V1.h"

#include "raven-engine/common/Counter.h"

namespace raven::generated {

class module_customer_level_data_V1 : public raven::base::Module {
public:
    base::ModuleInfo * moduleInfo = nullptr;
const record_card_data_V1 *card_data = nullptr;
int SG_card_data;
RG_record_customer_data_V1 RG_Ptr_customer_data;
RG_record_customer_data_V1 *customer_data = &RG_Ptr_customer_data;
// flatbuffers::DetachedBuffer BUFFER_customer_data;
const record_customer_data_V1 *OUT_customer_data = nullptr;

module_customer_level_data_V1 () : Module(nullptr) {
    moduleInfo = const_cast<base::ModuleInfo *>(getModuleInfo());
}
const base::ModuleInfo *getModuleInfo() const override;
void init() override {
    if(card_data==nullptr) {
    card_data = Getrecord_card_data_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_card_data_V1().first));
    }
return;
}
    bool setRecord(std::string_view recordId, const flatbuffers::Table *record) override {
using namespace std::string_view_literals;
if(recordId == "card_data"sv) {
    card_data = Getrecord_card_data_V1(reinterpret_cast<const void *>(record));
return true;
}
return false;
}

std::optional<uint64_t> colocationHash() override {
return std::optional<uint64_t>();
}
 bool setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
            std::string_view recordId,
            const flatbuffers::Table *record) override ;
 std::unique_ptr<flatbuffers::DetachedBuffer> getRecordOutputDetachedPtr(std::string_view recordId) const override;
 uint64_t getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const override;
 std::pair<const uint8_t *, uint64_t> getRecordOutputPtr(std::string_view recordId) override;
 std::pair<const uint8_t *, uint64_t> getRecordInputPtr(std::string_view recordId) override;

module_customer_level_data_V1 (raven::base::Transaction *txn) : Module(txn) {
    moduleInfo = const_cast<base::ModuleInfo *>(getModuleInfo());
}
folly::Future<folly::Unit> run() override;

static const base::ModuleInfo * StaticGetModuleInfo();

    database_record_dbr_customer_data::Key customer_data_Key;
};
}
