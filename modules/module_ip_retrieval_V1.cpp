


#include "module_ip_retrieval_V1.h"
#include "lib/include/custom_date.h"
#include <folly/GLog.h>

using namespace raven::types; // bring in the chrono operator


static std::unique_ptr<raven::base::Module> createNew_module_ip_retrieval_V1(raven::base::Transaction *txn) {
    return std::make_unique<raven::generated::module_ip_retrieval_V1>(txn);
}






static raven::base::ModuleInfo *GetModuleInfo_module_ip_retrieval_V1() {
static raven::base::ModuleInfo moduleInfo = {
    "ip_retrieval", // Module Name
    "VARIABLE", // Module Type
    // INPUT RECORDS
    {
          {"txn_auth_input","txn_auth_input",1,false},
    },
    // OUTPUT RECORDS
    {
        {"ip_address_record","ip_address_record",1,false},
    },
    1, // Module Version
    "misc_database", //resourceName - database name. Empty string if no database
      true, // is colocation relevant?
    true, // readOnly - no updates to database
    true, // isSlave ok?
    std::chrono::microseconds(0), // timeout
    std::chrono::microseconds(0), // roll up timeout
0, // max roll up count
    1, // module_hash
    createNew_module_ip_retrieval_V1, // New default module
    std::make_shared<::raven::base::ModuleMetrics>("ip_retrieval",1),
    nullptr, // module trace information
    google::WARNING, // module log severity
    nullptr
    };
    return &moduleInfo;
}

const raven::base::ModuleInfo *raven::generated::module_ip_retrieval_V1::getModuleInfo() const {
    return GetModuleInfo_module_ip_retrieval_V1();
}

const raven::base::ModuleInfo *raven::generated::module_ip_retrieval_V1::StaticGetModuleInfo() {
return GetModuleInfo_module_ip_retrieval_V1();
}

 bool raven::generated::module_ip_retrieval_V1::setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
        std::string_view recordId,
        const flatbuffers::Table *record) {
    /* Will be used for remote executing a module */
using namespace std::string_view_literals;

    if(recordId == "txn_auth_input"sv) {
        txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "ip_address_record"sv) {
    RG_Ptr_ip_address_record = std::move(*UnPackrecord_ip_address_record_V1(Getrecord_ip_address_record_V1(reinterpret_cast<const void *>(record))));
    flatbufferStorage.push_back(std::move(detachedBuffer));
        return true;
    }
return false;
}

uint64_t raven::generated::module_ip_retrieval_V1::getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const {
using namespace std::string_view_literals;
    if(recordId == "ip_address_record"sv) {
        auto offset = raven::generated::Createrecord_ip_address_record_V1(fbb, ip_address_record);
        return fbb.GetSize();
   }
        return 0;
}

std::pair<const uint8_t *, uint64_t> raven::generated::module_ip_retrieval_V1::getRecordOutputPtr(std::string_view recordId)  {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "ip_address_record"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_ip_address_record_V1(fbb, ip_address_record);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
    flatbufferStorage.push_back(std::move(detachedBuffer));
    return pair;
    }
return {nullptr,0};
}

std::unique_ptr<flatbuffers::DetachedBuffer> raven::generated::module_ip_retrieval_V1::getRecordOutputDetachedPtr(std::string_view recordId) const {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "ip_address_record"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_ip_address_record_V1(fbb, ip_address_record);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    return std::move(detachedBuffer);
    }
return nullptr;
}


std::pair<const uint8_t *, uint64_t> raven::generated::module_ip_retrieval_V1::getRecordInputPtr(std::string_view recordId) {
            if(recordId == "txn_auth_input"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_txn_auth_input_V1(fbb, txn_auth_input->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
    return {nullptr,0};
}









folly::Future<folly::Unit> raven::generated::module_ip_retrieval_V1::run() {
      if(!(true)) {
        LOG(INFO)<<"Skipping module for transaction " << messageMetaDataStruct.getTransactionId().toHexString();
        return folly::Unit();
    }
    

  /* Update colocation logic */
  std::vector<std::string> keyColocation;
  database_record_dbr_ip_address_record::Colocation ip_address_recordColocation = {};
  {
      database_record_dbr_ip_address_record::Colocation * colocation= &ip_address_record_Key.colocation;
      #include <cmath>


  fb_write(colocation,colocation_0) = std::floor((static_cast<long double>(raven::types::convert_ip_string_to_number(fb_read(txn_auth_input,ip_address))) / static_cast<long double>(((256 * 256) * 256))));

      colocation->normalize();
      keyColocation.emplace_back(reinterpret_cast<const char *>(colocation),sizeof(ip_address_record_Key.colocation));
      if(keyColocation[keyColocation.size()-1]!=keyColocation[0]) {
        LOG(ERROR) << "Colocation keys are not matching for all database records in ip_retrieval transaction id " << messageMetaDataStruct.getTransactionId().toHexString();
        throw std::runtime_error("Colocation keys are not matching for all database records in ip_retrieval transaction id " + messageMetaDataStruct.getTransactionId().toHexString());
      }
      ip_address_recordColocation = *colocation;
  }
  std::shared_ptr<raven::base::VirtualApplicationDB> db = GetResourceAppDBPointerUseCase("misc_database", keyColocation[0], messageMetaDataStruct.getTransactionId(), admin::DBUseCase::Read);
  if(db) {
  // Local processing
          {
          database_record_dbr_ip_address_record::Key dbKey = {};
          dbKey.colocation = ip_address_recordColocation;
          database_record_dbr_ip_address_record::Prefix * prefix = &dbKey.prefix;
              database_record_dbr_ip_address_record::Sort * sort_key = &dbKey.sort;
          // Check if sort key needs to be generated

          #include <cmath>


  fb_write(prefix,ip_address_first_octet) = std::floor((static_cast<long double>(raven::types::convert_ip_string_to_number(fb_read(txn_auth_input,ip_address))) / static_cast<long double>(((256 * 256) * 256))));
  fb_write(sort_key,ip_address_begin) = raven::types::convert_ip_string_to_number(fb_read(txn_auth_input,ip_address));

          dbKey.normalize();
          auto dbCf = db->getColumnFamilyHandle("dbr_ip_address_record");
          if(!dbCf) {
              throw std::runtime_error("Column family not found dbr_ip_address_record " + messageMetaDataStruct.getTransactionId().toHexString());
          }
          std::string value;
          auto rDB = db->getDB();
          if(!rDB) {
              throw std::runtime_error("Database not returned " + messageMetaDataStruct.getTransactionId().toHexString());
          }
          auto seekValidation = [&](const record_ip_address_record_V1 *ip_address_record) -> bool {
              struct {
                bool ok = false;
              } Seek_validator, *raven_seek_validator;
              raven_seek_validator = &Seek_validator;
                fb_write(raven_seek_validator,ok) = raven::types::convert_ip_string_to_number(fb_read(txn_auth_input,ip_address)) >= fb_read(ip_address_record,ip_address_begin) && raven::types::convert_ip_string_to_number(fb_read(txn_auth_input,ip_address)) <= fb_read(ip_address_record,ip_address_end);

              return Seek_validator.ok;
          };
          rocksdb::Status rStatus;
          auto iterator = std::unique_ptr<rocksdb::Iterator>(rDB->NewIterator(rocksdb::ReadOptions(), dbCf));
          iterator->Seek(rocksdb::Slice(reinterpret_cast<const char *>(&dbKey),sizeof(dbKey)));
          if(iterator->Valid()) {
            value = std::string(iterator->value().data(),iterator->value().size());
          } else {
            rStatus = rocksdb::Status::NotFound("Not found");
          }
          if(rStatus.ok()){
              if(value.size()<6) {
                  throw std::runtime_error("Invalid record read from database " + messageMetaDataStruct.getTransactionId().toHexString());
              }
              VLOG(2)<<"Data found for ip_address_record";
//          raven::base::Record * dbReadRecord = reinterpret_cast<raven::base::Record *>(value.data());
          const raven::base::fbs::GenericRecordHeaderOverlay * dbReadRecord = raven::base::fbs::GetGenericRecordHeaderOverlay(value.data());
//          dbReadRecord->size = static_cast<uint32_t>(value.size());
          if(ip_address_record) {
                  if(dbReadRecord->raven_header()->version() == 1) {
//                    if(sizeof(*(ip_address_record.get()))!=value.size()) {
//                       throw std::runtime_error("ip_address_record size does not match the record size=" + std::to_string(sizeof(*(ip_address_record.get()))) +" db size=" + std::to_string(value.size()) + " transaction id " + messageMetaDataStruct.getTransactionId().toHexString());
//                    }
//                        if(!seekValidation(dbReadRecord)) {}
                        if(!seekValidation(reinterpret_cast<const raven::generated::record_ip_address_record_V1 *> (dbReadRecord))) {}
                        else
//                    memcpy(ip_address_record.get(),value.data(), value.size());
                    memcpy(ip_address_record,value.data(), value.size());
                  } else if(dbReadRecord->raven_header()->version() < 1) {
                      const raven::base::RecordInfo *recordInfo = raven::base::LookupRecord("ip_address_record",
                          1);
                      flatbuffers::FlatBufferBuilder fbb;
                      auto upgradedDBRecord = recordInfo->upgraderFunctions.at(dbReadRecord->raven_header()->version())(fbb, reinterpret_cast<const flatbuffers::Table *>(dbReadRecord));
                      fbb.Finish(flatbuffers::Offset<raven::base::fbs::GenericRecordHeaderOverlay>(upgradedDBRecord));
                      auto Dbuff = fbb.Release();
                      if(upgradedDBRecord) {
                        if(!seekValidation(raven::generated::Getrecord_ip_address_record_V1 (Dbuff.data()))) {}
                        else
//                          ip_address_record.reset(static_cast<record_ip_address_record_V1 *>(upgradedDBRecord.release()));
                         ip_address_record = raven::generated::UnPackrecord_ip_address_record_V1(raven::generated::Getrecord_ip_address_record_V1(Dbuff.data())).get();
                      } else {
                          LOG(ERROR)<<"Error in module while trying to upgrade record " << messageMetaDataStruct.getTransactionId().toHexString();
                      }
                  } else {
                      auto recordInfoDowngrade = raven::base::LookupRecord("ip_address_record",dbReadRecord->raven_header()->version());
                      flatbuffers::FlatBufferBuilder fbb;
                      auto downgradedDBRecord = recordInfoDowngrade->downgraderFunctions.at(1 /* version wanted */)(fbb, reinterpret_cast<const flatbuffers::Table *>(dbReadRecord));
                      fbb.Finish(flatbuffers::Offset<raven::base::fbs::GenericRecordHeaderOverlay>(downgradedDBRecord));
                      auto Dbuff = fbb.Release();
                      if(downgradedDBRecord) {
//                        if(!seekValidation(downgradedDBRecord.get())) {}
                            if(!seekValidation(raven::generated::Getrecord_ip_address_record_V1 (Dbuff.data()))) {}
                        else
//                         ip_address_record.reset(static_cast<record_ip_address_record_V1 *>(downgradedDBRecord.release()));
                         ip_address_record = raven::generated::UnPackrecord_ip_address_record_V1(raven::generated::Getrecord_ip_address_record_V1(Dbuff.data())).get();
                      }
                  }
              } else
                LOG(ERROR)<<"Pointer was empty " << messageMetaDataStruct.getTransactionId().toHexString();
          }
          else {
              VLOG(1)<<"No data found while trying to retrieve data from misc_database ip_address_record" << rStatus.ToString() << " " << messageMetaDataStruct.getTransactionId().toHexString();
          }
      }
  rollupAtInit();
      rollupAtEnd();
  } else {
  // Remote processing
      flatbuffers::FlatBufferBuilder fbb;
      auto smInput = serializeModuleInputFB(fbb);
      fbb.Finish(smInput);
      auto payload = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
      std::weak_ptr<raven::base::Transaction> wpTxn;
      if(transaction) {
          wpTxn = transaction->weak_from_this();
      }
      LOG(INFO) << "Sending module for remote processing " << messageMetaDataStruct.getTransactionId().toHexString() << " transaction shared_ptr = " << !!transaction;
      return SendPayloadToRemoteDBResource("module", "misc_database",keyColocation[0],std::move(payload), messageMetaDataStruct).thenValue([this,transaction = (transaction!=nullptr), wpTxn = std::move(wpTxn)](std::unique_ptr<flatbuffers::DetachedBuffer> response) -> folly::Future<folly::Unit> {
        std::shared_ptr<raven::base::Transaction> sp;
        if(transaction) {
            sp = wpTxn.lock();
            if(!sp) return folly::make_exception_wrapper<std::runtime_error>("Transaction no longer exists");
        }
        LOG(INFO) << "Response received from remote node" << messageMetaDataStruct.getTransactionId().toHexString() << " transaction shared_ptr = " << !!sp;
        std::lock_guard lg(responseLock);
        if(!timedout) {
            this->deserializeModuleOutput(this,std::move(response));
                flatbuffers::FlatBufferBuilder fbb_ip_address_record;
          auto offset_ip_address_record = raven::generated::Createrecord_ip_address_record_V1(fbb_ip_address_record, ip_address_record);
          fbb_ip_address_record.Finish(offset_ip_address_record);
          auto BUFFER_ip_address_record = fbb_ip_address_record.Release();
          OUT_ip_address_record = raven::generated::Getrecord_ip_address_record_V1(BUFFER_ip_address_record.data());

        }
        return folly::Unit();
      });
  }
  return folly::Unit();
}

void raven::generated::module_ip_retrieval_V1::rollupAtInit() {
  
}

void raven::generated::module_ip_retrieval_V1::rollupAtEnd() {
  
}









#include "raven-engine/base/Artifact.h"





static bool module_ip_retrieval_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
      raven::base::Module::RegisterModule(
                const_cast<raven::base::ModuleInfo *>(raven::generated::module_ip_retrieval_V1::StaticGetModuleInfo()
        ));
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      raven::base::Module::UnRegisterModule(raven::generated::module_ip_retrieval_V1::StaticGetModuleInfo());
    }
    return true;
}

extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_ip_retrieval_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_ip_retrieval_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Module",     std::vector({    std::string("ip_retrieval"),
})
, 1, module_ip_retrieval_V1_StatusChangeCallBack, "module_ip_retrieval_V1");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("ip_address_record"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("misc_database"),
    std::string("dbr_ip_address_record"),
})
, "DatabaseRecord", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("misc_database"),
})
, "Database", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("txn_auth_input"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void module_ip_retrieval_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_ip_retrieval_V1());
     if(!artifact) return;
     module_ip_retrieval_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     module_ip_retrieval_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void module_ip_retrieval_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_ip_retrieval_V1());
    if(!artifact) return;
     module_ip_retrieval_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     module_ip_retrieval_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
