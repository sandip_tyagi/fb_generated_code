


#include "module_customer_transaction_history_rollup_V1.h"
#include "lib/include/custom_date.h"
#include <folly/GLog.h>

using namespace raven::types; // bring in the chrono operator


static std::unique_ptr<raven::base::Module> createNew_module_customer_transaction_history_rollup_V1(raven::base::Transaction *txn) {
    return std::make_unique<raven::generated::module_customer_transaction_history_rollup_V1>(txn);
}






static raven::base::ModuleInfo *GetModuleInfo_module_customer_transaction_history_rollup_V1() {
static raven::base::ModuleInfo moduleInfo = {
    "customer_transaction_history_rollup", // Module Name
    "VARIABLE", // Module Type
    // INPUT RECORDS
    {
          {"customer_data","customer_data",1,false},
          {"card_data","card_data",1,false},
          {"txn_auth_input","txn_auth_input",1,false},
    },
    // OUTPUT RECORDS
    {
        {"customer_level_rollup","customer_level_rollup",1,false},
        {"account_level_rollup","account_level_rollup",1,false},
        {"card_level_rollup","card_level_rollup",1,false},
    },
    1, // Module Version
    "customer_database", //resourceName - database name. Empty string if no database
      true, // is colocation relevant?
    true, // readOnly - no updates to database
    true, // isSlave ok?
    std::chrono::microseconds(0), // timeout
    std::chrono::microseconds(0), // roll up timeout
0, // max roll up count
    1, // module_hash
    createNew_module_customer_transaction_history_rollup_V1, // New default module
    std::make_shared<::raven::base::ModuleMetrics>("customer_transaction_history_rollup",1),
    nullptr, // module trace information
    google::WARNING, // module log severity
    nullptr
    };
    return &moduleInfo;
}

const raven::base::ModuleInfo *raven::generated::module_customer_transaction_history_rollup_V1::getModuleInfo() const {
    return GetModuleInfo_module_customer_transaction_history_rollup_V1();
}

const raven::base::ModuleInfo *raven::generated::module_customer_transaction_history_rollup_V1::StaticGetModuleInfo() {
return GetModuleInfo_module_customer_transaction_history_rollup_V1();
}

 bool raven::generated::module_customer_transaction_history_rollup_V1::setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
        std::string_view recordId,
        const flatbuffers::Table *record) {
    /* Will be used for remote executing a module */
using namespace std::string_view_literals;

    if(recordId == "customer_data"sv) {
        customer_data = Getrecord_customer_data_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "card_data"sv) {
        card_data = Getrecord_card_data_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "txn_auth_input"sv) {
        txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "customer_level_rollup"sv) {
    RG_Ptr_customer_level_rollup = std::move(*UnPackrecord_customer_level_rollup_V1(Getrecord_customer_level_rollup_V1(reinterpret_cast<const void *>(record))));
    flatbufferStorage.push_back(std::move(detachedBuffer));
        return true;
    }
    if(recordId == "account_level_rollup"sv) {
    RG_Ptr_account_level_rollup = std::move(*UnPackrecord_account_level_rollup_V1(Getrecord_account_level_rollup_V1(reinterpret_cast<const void *>(record))));
    flatbufferStorage.push_back(std::move(detachedBuffer));
        return true;
    }
    if(recordId == "card_level_rollup"sv) {
    RG_Ptr_card_level_rollup = std::move(*UnPackrecord_card_level_rollup_V1(Getrecord_card_level_rollup_V1(reinterpret_cast<const void *>(record))));
    flatbufferStorage.push_back(std::move(detachedBuffer));
        return true;
    }
return false;
}

uint64_t raven::generated::module_customer_transaction_history_rollup_V1::getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const {
using namespace std::string_view_literals;
    if(recordId == "customer_level_rollup"sv) {
        auto offset = raven::generated::Createrecord_customer_level_rollup_V1(fbb, customer_level_rollup);
        return fbb.GetSize();
   }
    if(recordId == "account_level_rollup"sv) {
        auto offset = raven::generated::Createrecord_account_level_rollup_V1(fbb, account_level_rollup);
        return fbb.GetSize();
   }
    if(recordId == "card_level_rollup"sv) {
        auto offset = raven::generated::Createrecord_card_level_rollup_V1(fbb, card_level_rollup);
        return fbb.GetSize();
   }
        return 0;
}

std::pair<const uint8_t *, uint64_t> raven::generated::module_customer_transaction_history_rollup_V1::getRecordOutputPtr(std::string_view recordId)  {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "customer_level_rollup"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_customer_level_rollup_V1(fbb, customer_level_rollup);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
    flatbufferStorage.push_back(std::move(detachedBuffer));
    return pair;
    }
    if(recordId == "account_level_rollup"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_account_level_rollup_V1(fbb, account_level_rollup);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
    flatbufferStorage.push_back(std::move(detachedBuffer));
    return pair;
    }
    if(recordId == "card_level_rollup"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_card_level_rollup_V1(fbb, card_level_rollup);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
    flatbufferStorage.push_back(std::move(detachedBuffer));
    return pair;
    }
return {nullptr,0};
}

std::unique_ptr<flatbuffers::DetachedBuffer> raven::generated::module_customer_transaction_history_rollup_V1::getRecordOutputDetachedPtr(std::string_view recordId) const {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "customer_level_rollup"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_customer_level_rollup_V1(fbb, customer_level_rollup);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    return std::move(detachedBuffer);
    }
    if(recordId == "account_level_rollup"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_account_level_rollup_V1(fbb, account_level_rollup);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    return std::move(detachedBuffer);
    }
    if(recordId == "card_level_rollup"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_card_level_rollup_V1(fbb, card_level_rollup);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    return std::move(detachedBuffer);
    }
return nullptr;
}


std::pair<const uint8_t *, uint64_t> raven::generated::module_customer_transaction_history_rollup_V1::getRecordInputPtr(std::string_view recordId) {
            if(recordId == "customer_data"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_customer_data_V1(fbb, customer_data->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "card_data"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_card_data_V1(fbb, card_data->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "txn_auth_input"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_txn_auth_input_V1(fbb, txn_auth_input->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
    return {nullptr,0};
}









folly::Future<folly::Unit> raven::generated::module_customer_transaction_history_rollup_V1::run() {
      if(!(true)) {
        LOG(INFO)<<"Skipping module for transaction " << messageMetaDataStruct.getTransactionId().toHexString();
        return folly::Unit();
    }
    

  /* Update colocation logic */
  std::vector<std::string> keyColocation;
  database_record_dbr_customer_txn_history::Colocation customer_txn_historyColocation = {};
  {
      database_record_dbr_customer_txn_history::Colocation * colocation= &customer_txn_history_Key.colocation;
        fb_write(colocation,colocation_0) = fb_read(customer_data,customer_number);

      colocation->normalize();
      keyColocation.emplace_back(reinterpret_cast<const char *>(colocation),sizeof(customer_txn_history_Key.colocation));
      if(keyColocation[keyColocation.size()-1]!=keyColocation[0]) {
        LOG(ERROR) << "Colocation keys are not matching for all database records in customer_transaction_history_rollup transaction id " << messageMetaDataStruct.getTransactionId().toHexString();
        throw std::runtime_error("Colocation keys are not matching for all database records in customer_transaction_history_rollup transaction id " + messageMetaDataStruct.getTransactionId().toHexString());
      }
      customer_txn_historyColocation = *colocation;
  }
  std::shared_ptr<raven::base::VirtualApplicationDB> db = GetResourceAppDBPointerUseCase("customer_database", keyColocation[0], messageMetaDataStruct.getTransactionId(), admin::DBUseCase::Read);
  if(db) {
  // Local processing
  rollupAtInit();

      database_record_dbr_customer_txn_history::Key dbStartKey = {};
      database_record_dbr_customer_txn_history::Key dbEndKey = {};
      dbStartKey.colocation = customer_txn_historyColocation;
      database_record_dbr_customer_txn_history::Prefix * prefix = &dbStartKey.prefix;
      database_record_dbr_customer_txn_history::Sort * sort_key_begin = &dbStartKey.sort;
      database_record_dbr_customer_txn_history::Sort SortKeyEnd = {};
      database_record_dbr_customer_txn_history::Sort * sort_key_end = &dbEndKey.sort;

      {  // Current transaction logic
          RG_record_customer_txn_history_V1 rollUpRecord = {};
          RG_record_customer_txn_history_V1 *customer_txn_history = &rollUpRecord;
            fb_write(customer_txn_history,account_number) = fb_read(card_data,account_number);
  fb_write(customer_txn_history,amount) = fb_read(txn_auth_input,amount);
  fb_write(customer_txn_history,auth_score) = raven::missing;
  fb_write(customer_txn_history,card_number) = fb_read(card_data,card_number);
  fb_write(customer_txn_history,currency_code) = fb_read(txn_auth_input,currency_code);
  fb_write(customer_txn_history,customer_number) = fb_read(customer_data,customer_number);
  fb_write(customer_txn_history,device_id) = fb_read(txn_auth_input,device_id);
  fb_write(customer_txn_history,fraud_score) = raven::missing;
  fb_write(customer_txn_history,ip_address) = fb_read(txn_auth_input,ip_address);
  fb_write(customer_txn_history,merchant_id) = fb_read(txn_auth_input,merchant_id);
  fb_write(customer_txn_history,trace_number) = fb_read(txn_auth_input,trace_number);
  fb_write_date(customer_txn_history,transaction_date) = fb_read_date(txn_auth_input,transaction_date);
  fb_write(customer_txn_history,transaction_decision) = raven::missing;
  fb_write_time(customer_txn_history,transaction_time) = fb_read_time(txn_auth_input,transaction_time);
  fb_write(customer_txn_history,transaction_type) = fb_read(txn_auth_input,transaction_type);
  fb_write(customer_txn_history,zip_code) = fb_read(txn_auth_input,zip_code);

          VLOG(3)<<"Current Transaction being processed through roll-up logic " << messageMetaDataStruct.getTransactionId().toHexString();
          flatbuffers::FlatBufferBuilder fbb;
          auto customer_txn_history_offset = raven::generated::Createrecord_customer_txn_history_V1(fbb, customer_txn_history);
          fbb.Finish(customer_txn_history_offset);
          auto customer_txn_history_ptr = raven::generated::Getrecord_customer_txn_history_V1(fbb.GetBufferPointer());
          rollupEachCompute(customer_txn_history_ptr,true);
      } // End current transaction logic
      // Key setting logic from blockly
        fb_write(prefix,customer_number) = fb_read(customer_data,customer_number);
  fb_write_date(sort_key_begin,transaction_date) = raven::missing;
  fb_write_time(sort_key_begin,transaction_time) = raven::missing;
  fb_write_date(sort_key_end,transaction_date) = raven::missing;
  fb_write_time(sort_key_end,transaction_time) = raven::missing;

      // End key setting logic from blockly
      dbStartKey.normalize();

      auto rollUpDB = db->getDB();
      if(!rollUpDB) throw std::runtime_error("No database found for rollup db " + messageMetaDataStruct.getTransactionId().toHexString());
      auto cfHandle = db->getColumnFamilyHandle("dbr_customer_txn_history");
      if(cfHandle==nullptr) throw std::runtime_error("No column family dbr_customer_txn_history found " + messageMetaDataStruct.getTransactionId().toHexString());
      const raven::base::RecordInfo *recordInfo = raven::base::LookupRecord("customer_txn_history",
      1);
      rocksdb::ReadOptions readOptions;
      readOptions.prefix_same_as_start = true;
      rocksdb::Slice upperBound;
      if(!dbEndKey.sort.isEmpty()) {
          dbEndKey.colocation = dbStartKey.colocation;
          dbEndKey.prefix = dbStartKey.prefix;
          dbEndKey.sort.normalize();
          upperBound = rocksdb::Slice(reinterpret_cast<const char *>(&dbEndKey), sizeof(dbEndKey));
/*          if constexpr (DCHECK_IS_ON()){
            auto databaseRecord = raven::base::DatabaseRecord::GetDatabaseRecord("customer_database", "dbr_customer_txn_history");
            if(databaseRecord){
                DLOG(INFO)<<"Upper bound set to " <<upperBound.ToString(true) << " " <<
                    databaseRecord->GetJsonFromKey(upperBound).dump();
            }
          }
*/
          readOptions.iterate_upper_bound = &upperBound;
      }

      /* Now let us setup the database iterator */
      std::unique_ptr<rocksdb::Iterator> dbIter(rollUpDB->NewIterator(readOptions, cfHandle));
      if(!dbStartKey.sort.isEmpty()){
        dbIter->Seek(rocksdb::Slice(reinterpret_cast<const char*>(&dbStartKey),sizeof(dbStartKey)));
      } else {
        dbIter->Seek(rocksdb::Slice(reinterpret_cast<const char*>(&dbStartKey),sizeof(dbStartKey.colocation)+sizeof(dbStartKey.prefix)));
      }
      uint32_t rollUpCount = 0;
      bool errorPrinted = false;
      auto rollUpStartTime = std::chrono::system_clock::now();
      while(dbIter->Valid()) {
              // Process the current record
              //
              //
              rollUpCount++;
/*              if constexpr (DCHECK_IS_ON()) {
                  auto databaseRecord = raven::base::DatabaseRecord::GetDatabaseRecord("customer_database", "dbr_customer_txn_history");
                  if(databaseRecord) {
                      DLOG(INFO) <<"RollUp record key read " <<dbIter->key().ToString(true) << " " <<
                        databaseRecord->GetJsonFromKey(dbIter->key()).dump();
                  }
              }
*/

              const raven::base::fbs::GenericRecordHeaderOverlay * dbReadRecord = raven::base::fbs::GetGenericRecordHeaderOverlay(dbIter->value().data());
              if(dbReadRecord->raven_header()->version()==1) {
                      VLOG(4)<<"Read roll-up record " << messageMetaDataStruct.getTransactionId().toHexString() << " - " << recordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(dbReadRecord));
                      rollupEachCompute(reinterpret_cast<const raven::generated::record_customer_txn_history_V1 *>(dbReadRecord),false);
              } else if(dbReadRecord->raven_header()->version() < 1) {
//                      std::string value(dbIter->value().data(),dbIter->value().size());
//                      auto dbRec = reinterpret_cast<raven::base::Record *>(value.data());
//                      dbRec->size = static_cast<uint32_t>(dbIter->value().size());
                      try {
                        flatbuffers::FlatBufferBuilder fbb;
                        flatbuffers::uoffset_t upgradedDBRecord = recordInfo->upgraderFunctions.at(dbReadRecord->raven_header()->version())(fbb,reinterpret_cast<const flatbuffers::Table*>(dbReadRecord));
                        fbb.Finish(flatbuffers::Offset<raven::generated::record_customer_txn_history_V1>(upgradedDBRecord));
//                        rollupEachCompute(static_cast<const raven::generated::record_customer_txn_history_V1 *>(upgradedDBRecord.get()),false);
                        rollupEachCompute(raven::generated::Getrecord_customer_txn_history_V1(fbb.GetBufferPointer()),false);
                      } catch(std::exception &e) {
                        if(!errorPrinted) {
                            LOG(WARNING)<<"Got an error while trying to upgrade " << e.what() << " ignoring and continuing."<<messageMetaDataStruct.getTransactionId().toHexString();
                            errorPrinted = true;
                        }
                      }
              } else {
                      std::string value(dbIter->value().data(),dbIter->value().size());
//                      auto dbRec = reinterpret_cast<raven::base::Record *>(value.data());
//                      dbRec->size = static_cast<uint32_t>(dbIter->value().size());
                      auto recordInfoDowngrade = raven::base::LookupRecord("customer_txn_history",dbReadRecord->raven_header()->version());
                      try {
                        flatbuffers::FlatBufferBuilder fbb;
                        flatbuffers::uoffset_t downgradedDBRecord = recordInfoDowngrade->downgraderFunctions.at(1 /* version wanted */)(fbb,reinterpret_cast<const flatbuffers::Table*>(dbReadRecord));
//                        auto downgradedDBRecord = recordInfoDowngrade->downgraderFunctions.at(1 /* version wanted */)(fbb,dbRec);
                        fbb.Finish(flatbuffers::Offset<raven::generated::record_customer_txn_history_V1>(downgradedDBRecord));
                        rollupEachCompute(raven::generated::Getrecord_customer_txn_history_V1(fbb.GetBufferPointer()),false);
//                        rollupEachCompute(static_cast<const raven::generated::record_customer_txn_history_V1 *>(downgradedDBRecord.get()),false);
                      } catch(std::exception &e) {
                        if(!errorPrinted) {
                            LOG(WARNING)<<"Got an error while trying to downgrade " << e.what() << " ignoring and continuing. "<<messageMetaDataStruct.getTransactionId().toHexString();
                            errorPrinted = true;
                        }
                      }
              }
              if(exceededMaxTime) break;
              if( (rollUpCount % 0x4000) == 0) {
                  LOG(WARNING) << "RollUp count exceeded " << rollUpCount <<" for " << messageMetaDataStruct.getTransactionId().toHexString();
              }
              dbIter->Next();
      }
      auto rollUpEndTime = std::chrono::system_clock::now();
      LOG(INFO) << "RollUp records processed " << rollUpCount << ' ' << messageMetaDataStruct.getTransactionId().toHexString()
      << " roll up time taken : " << std::chrono::duration_cast<std::chrono::microseconds>(rollUpEndTime-rollUpStartTime).count() << " us exceededMaxTime = " << exceededMaxTime;
      rollupAtEnd();
  } else {
  // Remote processing
      flatbuffers::FlatBufferBuilder fbb;
      auto smInput = serializeModuleInputFB(fbb);
      fbb.Finish(smInput);
      auto payload = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
      std::weak_ptr<raven::base::Transaction> wpTxn;
      if(transaction) {
          wpTxn = transaction->weak_from_this();
      }
      LOG(INFO) << "Sending module for remote processing " << messageMetaDataStruct.getTransactionId().toHexString() << " transaction shared_ptr = " << !!transaction;
      return SendPayloadToRemoteDBResource("module", "customer_database",keyColocation[0],std::move(payload), messageMetaDataStruct).thenValue([this,transaction = (transaction!=nullptr), wpTxn = std::move(wpTxn)](std::unique_ptr<flatbuffers::DetachedBuffer> response) -> folly::Future<folly::Unit> {
        std::shared_ptr<raven::base::Transaction> sp;
        if(transaction) {
            sp = wpTxn.lock();
            if(!sp) return folly::make_exception_wrapper<std::runtime_error>("Transaction no longer exists");
        }
        LOG(INFO) << "Response received from remote node" << messageMetaDataStruct.getTransactionId().toHexString() << " transaction shared_ptr = " << !!sp;
        std::lock_guard lg(responseLock);
        if(!timedout) {
            this->deserializeModuleOutput(this,std::move(response));
                flatbuffers::FlatBufferBuilder fbb_customer_level_rollup;
          auto offset_customer_level_rollup = raven::generated::Createrecord_customer_level_rollup_V1(fbb_customer_level_rollup, customer_level_rollup);
          fbb_customer_level_rollup.Finish(offset_customer_level_rollup);
          auto BUFFER_customer_level_rollup = fbb_customer_level_rollup.Release();
          OUT_customer_level_rollup = raven::generated::Getrecord_customer_level_rollup_V1(BUFFER_customer_level_rollup.data());
          flatbuffers::FlatBufferBuilder fbb_account_level_rollup;
          auto offset_account_level_rollup = raven::generated::Createrecord_account_level_rollup_V1(fbb_account_level_rollup, account_level_rollup);
          fbb_account_level_rollup.Finish(offset_account_level_rollup);
          auto BUFFER_account_level_rollup = fbb_account_level_rollup.Release();
          OUT_account_level_rollup = raven::generated::Getrecord_account_level_rollup_V1(BUFFER_account_level_rollup.data());
          flatbuffers::FlatBufferBuilder fbb_card_level_rollup;
          auto offset_card_level_rollup = raven::generated::Createrecord_card_level_rollup_V1(fbb_card_level_rollup, card_level_rollup);
          fbb_card_level_rollup.Finish(offset_card_level_rollup);
          auto BUFFER_card_level_rollup = fbb_card_level_rollup.Release();
          OUT_card_level_rollup = raven::generated::Getrecord_card_level_rollup_V1(BUFFER_card_level_rollup.data());

        }
        return folly::Unit();
      });
  }
  return folly::Unit();
}

void raven::generated::module_customer_transaction_history_rollup_V1::rollupAtInit() {
  std::string test_js_raven_temp_temp="";
/* begin code for account_level_rollup.test_js */
  test_js_raven_temp_temp = u8"5>3"sv;
  fb_write(account_level_rollup,test_js) = raven::base::Module::ExecuteJavaScriptExpression(test_js_raven_temp_temp);

/* end code for account_level_rollup.test_js*/
/* customer_level_rollup.customer_count_same_merchant */ 
/* VARIABLE AGGREGATION INIT */

/* account_level_rollup.unique_merchant_cnt30 */ 
/* VARIABLE AGGREGATION INIT */

/* card_level_rollup.card_count_txn_30days */ 
/* VARIABLE AGGREGATION INIT */

}
void raven::generated::module_customer_transaction_history_rollup_V1::rollupEachCompute(
    const raven::generated::record_customer_txn_history_V1
    *customer_txn_history, bool currentTxn [[maybe_unused]]) {
    if(customer_txn_history==nullptr) throw std::runtime_error("Could not execute rollupEach compute " + messageMetaDataStruct.getTransactionId().toHexString());

    struct TD {
        long current_transaction = raven::missing;                  // raven::types::raven_sys_date_time
        long accum_transaction = raven::missing;                    // raven::types::raven_sys_date_time
        long time_difference = raven::missing;                      // std::chrono::duration<double,std::milli>
        float time_difference_calendar_seconds = raven::missing;
        float time_difference_calendar_minutes = raven::missing;
        float time_difference_calendar_hours = raven::missing;
        float time_difference_calendar_days = raven::missing;
        float time_difference_calendar_months = raven::missing;
        float time_difference_calendar_years = raven::missing;
    } TimeDifference;
    TD *time_difference = &TimeDifference;
      fb_write_date_time(time_difference,current_transaction) = (fb_read_date(txn_auth_input,transaction_date) + fb_read_time(txn_auth_input,transaction_time));
  fb_write_date_time(time_difference,accum_transaction) = (fb_read_date(customer_txn_history,transaction_date) + fb_read_time(customer_txn_history,transaction_time));


    if(fb_read_date_time(time_difference,current_transaction) != raven::missing && fb_read_date_time(time_difference,accum_transaction) != raven::missing) {
    auto acmDays = std::chrono::floor<date::days>(fb_read_date_time(time_difference,accum_transaction));
    auto acmYmd = date::year_month_day{acmDays};
    auto curDays = std::chrono::floor<date::days>(fb_read_date_time(time_difference,current_transaction));
    auto curYmd = date::year_month_day{curDays};
    time_difference->time_difference_calendar_seconds = (std::chrono::floor<std::chrono::seconds>(fb_read_date_time(time_difference,current_transaction)) - std::chrono::floor<std::chrono::seconds>(fb_read_date_time(time_difference,accum_transaction))).count();
    time_difference->time_difference_calendar_minutes = (std::chrono::floor<std::chrono::minutes>(fb_read_date_time(time_difference,current_transaction)) - std::chrono::floor<std::chrono::minutes>(fb_read_date_time(time_difference,accum_transaction))).count();
    time_difference->time_difference_calendar_hours = (std::chrono::floor<std::chrono::hours>(fb_read_date_time(time_difference,current_transaction)) - std::chrono::floor<std::chrono::hours>(fb_read_date_time(time_difference,accum_transaction))).count();
    time_difference->time_difference_calendar_years = (curYmd.year()-acmYmd.year()).count();
    time_difference->time_difference_calendar_months = static_cast<unsigned int>(curYmd.month())+0L-static_cast<unsigned int>(acmYmd.month()) + time_difference->time_difference_calendar_years*12;
    time_difference->time_difference_calendar_days = (curDays-acmDays).count();
    fb_write_time(time_difference,time_difference) = fb_read_date_time(time_difference,current_transaction)-fb_read_date_time(time_difference,accum_transaction);
    } else {

    fb_write_time(time_difference,time_difference) =  std::chrono::milliseconds(0);
    }


    
exceededMaxTime = true;
if(fb_read_time(time_difference,time_difference) < std::chrono::seconds(0)) exceededMaxTime = false;
if(fb_read_time(time_difference,time_difference) < std::chrono::seconds(2592000)) exceededMaxTime = false;
if((!currentTxn)&&(fb_read_time(time_difference,time_difference) > std::chrono::seconds(0) && fb_read_time(time_difference,time_difference) < std::chrono::seconds(0))) {
if(((fb_read(customer_txn_history,merchant_id) == fb_read(txn_auth_input,merchant_id)))) {
/* customer_level_rollup.customer_count_same_merchant */
  /* VARIABLE AGGREGATION COMPUTE */
 if(1!=raven::missing) {temp_variable_200++;}
}
}
if((!currentTxn)&&(fb_read_time(time_difference,time_difference) > std::chrono::seconds(0) && fb_read_time(time_difference,time_difference) < std::chrono::seconds(2592000))) {
if((true)) {
/* account_level_rollup.unique_merchant_cnt30 */
  /* VARIABLE AGGREGATION COMPUTE */
 /* COUNT UNIQUE */
temp_variable_400.insert(value_for_insert_to_map(fb_read(customer_txn_history,merchant_id)));
}
if(((fb_read(txn_auth_input,card_number) == fb_read(customer_txn_history,card_number)))) {
/* card_level_rollup.card_count_txn_30days */
  /* VARIABLE AGGREGATION COMPUTE */
 if(1!=raven::missing) {temp_variable_800++;}
}
}
}

void raven::generated::module_customer_transaction_history_rollup_V1::rollupAtEnd() {
  /* customer_level_rollup.customer_count_same_merchant */ 
/* VARIABLE AGGREGATION FINAL */ 

/* account_level_rollup.unique_merchant_cnt30 */ 
/* VARIABLE AGGREGATION FINAL */ 

/* card_level_rollup.card_count_txn_30days */ 
/* VARIABLE AGGREGATION FINAL */ 
/* customer_level_rollup.customer_count_same_merchant */ 
   fb_write(customer_level_rollup,customer_count_same_merchant) = (/* TODO  temp_variable_200 count*/ temp_variable_200
  );

/* account_level_rollup.unique_merchant_cnt30 */ 
   fb_write(account_level_rollup,unique_merchant_cnt30) = (/* COUNT UNIQUE */
  /* TODO  temp_variable_400 count_unique*/ temp_variable_400.size()
  /* TODO  count_unique*/
  );

/* card_level_rollup.card_count_txn_30days */ 
   fb_write(card_level_rollup,card_count_txn_30days) = (/* TODO  temp_variable_800 count*/ temp_variable_800
  );

}









#include "raven-engine/base/Artifact.h"





static bool module_customer_transaction_history_rollup_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
      raven::base::Module::RegisterModule(
                const_cast<raven::base::ModuleInfo *>(raven::generated::module_customer_transaction_history_rollup_V1::StaticGetModuleInfo()
        ));
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      raven::base::Module::UnRegisterModule(raven::generated::module_customer_transaction_history_rollup_V1::StaticGetModuleInfo());
    }
    return true;
}

extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_customer_transaction_history_rollup_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_customer_transaction_history_rollup_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Module",     std::vector({    std::string("customer_transaction_history_rollup"),
})
, 1, module_customer_transaction_history_rollup_V1_StatusChangeCallBack, "module_customer_transaction_history_rollup_V1");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("account_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_database"),
    std::string("dbr_customer_txn_history"),
})
, "DatabaseRecord", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("card_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("txn_auth_input"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("card_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_database"),
})
, "Database", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void module_customer_transaction_history_rollup_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_customer_transaction_history_rollup_V1());
     if(!artifact) return;
     module_customer_transaction_history_rollup_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     module_customer_transaction_history_rollup_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void module_customer_transaction_history_rollup_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_customer_transaction_history_rollup_V1());
    if(!artifact) return;
     module_customer_transaction_history_rollup_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     module_customer_transaction_history_rollup_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
