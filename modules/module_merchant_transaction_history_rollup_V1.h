#pragma once





#include "raven-engine/base/Module.h"
#include "raven-engine/base/ModuleMetrics.h"
#include "raven-engine/base/RavenSwitches.h"
#include "folly/Conv.h"
#include "raven-engine/base/VirtualApplicationDB.h"
#include <rocksdb/db.h>
#include "../records/record_txn_auth_input_V1.h"
#include "../records/record_account_data_V1.h"
#include "../records/record_merchant_level_rollup_V1.h"
#include "../database_records/database_record_merchant_database_dbr_merchant_txn_history.h"
#include "../records/record_merchant_txn_history_V1.h"

#include "raven-engine/common/Counter.h"

namespace raven::generated {

class module_merchant_transaction_history_rollup_V1 : public raven::base::Module {
public:
    base::ModuleInfo * moduleInfo = nullptr;
const record_txn_auth_input_V1 *txn_auth_input = nullptr;
int SG_txn_auth_input;
const record_account_data_V1 *account_data = nullptr;
int SG_account_data;
RG_record_merchant_level_rollup_V1 RG_Ptr_merchant_level_rollup;
RG_record_merchant_level_rollup_V1 *merchant_level_rollup = &RG_Ptr_merchant_level_rollup;
// flatbuffers::DetachedBuffer BUFFER_merchant_level_rollup;
const record_merchant_level_rollup_V1 *OUT_merchant_level_rollup = nullptr;

module_merchant_transaction_history_rollup_V1 () : Module(nullptr) {
    moduleInfo = const_cast<base::ModuleInfo *>(getModuleInfo());
}
const base::ModuleInfo *getModuleInfo() const override;
void init() override {
    if(txn_auth_input==nullptr) {
    txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_txn_auth_input_V1().first));
    }
    if(account_data==nullptr) {
    account_data = Getrecord_account_data_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_account_data_V1().first));
    }
return;
}
    bool setRecord(std::string_view recordId, const flatbuffers::Table *record) override {
using namespace std::string_view_literals;
if(recordId == "txn_auth_input"sv) {
    txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(record));
return true;
}
if(recordId == "account_data"sv) {
    account_data = Getrecord_account_data_V1(reinterpret_cast<const void *>(record));
return true;
}
return false;
}

std::optional<uint64_t> colocationHash() override {
return std::optional<uint64_t>();
}
 bool setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
            std::string_view recordId,
            const flatbuffers::Table *record) override ;
 std::unique_ptr<flatbuffers::DetachedBuffer> getRecordOutputDetachedPtr(std::string_view recordId) const override;
 uint64_t getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const override;
 std::pair<const uint8_t *, uint64_t> getRecordOutputPtr(std::string_view recordId) override;
 std::pair<const uint8_t *, uint64_t> getRecordInputPtr(std::string_view recordId) override;

module_merchant_transaction_history_rollup_V1 (raven::base::Transaction *txn) : Module(txn) {
    moduleInfo = const_cast<base::ModuleInfo *>(getModuleInfo());
}
folly::Future<folly::Unit> run() override;

static const base::ModuleInfo * StaticGetModuleInfo();

    database_record_dbr_merchant_txn_history::Key merchant_txn_history_Key;





/* merchant_level_rollup.unique_cards_10min */
 /* VARIABLE AGGREGATION DECLARATION */
/* COUNT UNIQUE */
 std::unordered_set<std::string> temp_variable_100;
bool exceededMaxTime = false;

void rollupAtInit();

void rollupEachCompute(const raven::generated::record_merchant_txn_history_V1
    *merchant_txn_history, bool currentTxn);

void rollupAtEnd();
};
}
