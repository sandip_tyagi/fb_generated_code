#pragma once





#include "raven-engine/base/Module.h"
#include "raven-engine/base/ModuleMetrics.h"
#include "raven-engine/base/RavenSwitches.h"
#include "folly/Conv.h"
#include "raven-engine/base/VirtualApplicationDB.h"
#include <rocksdb/db.h>
#include "../records/record_customer_data_V1.h"
#include "../records/record_card_data_V1.h"
#include "../records/record_txn_auth_input_V1.h"
#include "../records/record_customer_level_rollup_V1.h"
#include "../records/record_account_level_rollup_V1.h"
#include "../records/record_card_level_rollup_V1.h"
#include "../database_records/database_record_customer_database_dbr_customer_txn_history.h"
#include "../records/record_customer_txn_history_V1.h"

#include "raven-engine/common/Counter.h"

namespace raven::generated {

class module_customer_transaction_history_rollup_V1 : public raven::base::Module {
public:
    base::ModuleInfo * moduleInfo = nullptr;
const record_customer_data_V1 *customer_data = nullptr;
int SG_customer_data;
const record_card_data_V1 *card_data = nullptr;
int SG_card_data;
const record_txn_auth_input_V1 *txn_auth_input = nullptr;
int SG_txn_auth_input;
RG_record_customer_level_rollup_V1 RG_Ptr_customer_level_rollup;
RG_record_customer_level_rollup_V1 *customer_level_rollup = &RG_Ptr_customer_level_rollup;
// flatbuffers::DetachedBuffer BUFFER_customer_level_rollup;
const record_customer_level_rollup_V1 *OUT_customer_level_rollup = nullptr;
RG_record_account_level_rollup_V1 RG_Ptr_account_level_rollup;
RG_record_account_level_rollup_V1 *account_level_rollup = &RG_Ptr_account_level_rollup;
// flatbuffers::DetachedBuffer BUFFER_account_level_rollup;
const record_account_level_rollup_V1 *OUT_account_level_rollup = nullptr;
RG_record_card_level_rollup_V1 RG_Ptr_card_level_rollup;
RG_record_card_level_rollup_V1 *card_level_rollup = &RG_Ptr_card_level_rollup;
// flatbuffers::DetachedBuffer BUFFER_card_level_rollup;
const record_card_level_rollup_V1 *OUT_card_level_rollup = nullptr;

module_customer_transaction_history_rollup_V1 () : Module(nullptr) {
    moduleInfo = const_cast<base::ModuleInfo *>(getModuleInfo());
}
const base::ModuleInfo *getModuleInfo() const override;
void init() override {
    if(customer_data==nullptr) {
    customer_data = Getrecord_customer_data_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_customer_data_V1().first));
    }
    if(card_data==nullptr) {
    card_data = Getrecord_card_data_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_card_data_V1().first));
    }
    if(txn_auth_input==nullptr) {
    txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(raven::generated::getDefault_record_txn_auth_input_V1().first));
    }
return;
}
    bool setRecord(std::string_view recordId, const flatbuffers::Table *record) override {
using namespace std::string_view_literals;
if(recordId == "customer_data"sv) {
    customer_data = Getrecord_customer_data_V1(reinterpret_cast<const void *>(record));
return true;
}
if(recordId == "card_data"sv) {
    card_data = Getrecord_card_data_V1(reinterpret_cast<const void *>(record));
return true;
}
if(recordId == "txn_auth_input"sv) {
    txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(record));
return true;
}
return false;
}

std::optional<uint64_t> colocationHash() override {
return std::optional<uint64_t>();
}
 bool setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
            std::string_view recordId,
            const flatbuffers::Table *record) override ;
 std::unique_ptr<flatbuffers::DetachedBuffer> getRecordOutputDetachedPtr(std::string_view recordId) const override;
 uint64_t getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const override;
 std::pair<const uint8_t *, uint64_t> getRecordOutputPtr(std::string_view recordId) override;
 std::pair<const uint8_t *, uint64_t> getRecordInputPtr(std::string_view recordId) override;

module_customer_transaction_history_rollup_V1 (raven::base::Transaction *txn) : Module(txn) {
    moduleInfo = const_cast<base::ModuleInfo *>(getModuleInfo());
}
folly::Future<folly::Unit> run() override;

static const base::ModuleInfo * StaticGetModuleInfo();

    database_record_dbr_customer_txn_history::Key customer_txn_history_Key;





/* customer_level_rollup.customer_count_same_merchant */
 /* VARIABLE AGGREGATION DECLARATION */
double temp_variable_200 = 0;
/* account_level_rollup.unique_merchant_cnt30 */
 /* VARIABLE AGGREGATION DECLARATION */
/* COUNT UNIQUE */
 std::unordered_set<std::string> temp_variable_400;
/* card_level_rollup.card_count_txn_30days */
 /* VARIABLE AGGREGATION DECLARATION */
double temp_variable_800 = 0;
bool exceededMaxTime = false;

void rollupAtInit();

void rollupEachCompute(const raven::generated::record_customer_txn_history_V1
    *customer_txn_history, bool currentTxn);

void rollupAtEnd();
};
}
