


#include "module_auth_logging_module_V1.h"
#include "lib/include/custom_date.h"
#include <folly/GLog.h>

using namespace raven::types; // bring in the chrono operator




#include <spdlog/async.h>
#include <spdlog/sinks/interval_file_sink.h>
#include <spdlog/logger.h>
#include <boost/filesystem.hpp>


static std::shared_ptr<spdlog::logger> getNewLogger();
static std::unique_ptr<raven::base::Module> createNew_module_auth_logging_module_V1(raven::base::Transaction *txn) {
    return std::make_unique<raven::generated::module_auth_logging_module_V1>(txn);
}






static raven::base::ModuleInfo *GetModuleInfo_module_auth_logging_module_V1() {
static raven::base::ModuleInfo moduleInfo = {
    "auth_logging_module", // Module Name
    "LOGGING", // Module Type
    // INPUT RECORDS
    {
          {"account_data","account_data",1,false},
          {"account_level_rollup","account_level_rollup",1,false},
          {"card_data","card_data",1,false},
          {"card_level_rollup","card_level_rollup",1,false},
          {"fraud_ruleset_result","fraud_ruleset_result",1,false},
          {"ip_address_record","ip_address_record",1,false},
          {"merchant_level_rollup","merchant_level_rollup",1,false},
          {"auth_score","raven_gbm_model_output",1,false},
          {"fraud_score","raven_gbm_model_output",1,false},
          {"txn_auth_input","txn_auth_input",1,false},
          {"txn_auth_output","txn_auth_output",1,false},
          {"auth_ruleset_result","auth_ruleset_result",1,false},
          {"customer_level_rollup","customer_level_rollup",1,false},
          {"customer_data","customer_data",1,false},
    },
    // OUTPUT RECORDS
    {
    },
    1, // Module Version
    "", //resourceName - database name. Empty string if no database
      true, // is colocation relevant?
    true, // readOnly - no updates to database
    true, // isSlave ok?
    std::chrono::microseconds(0), // timeout
    std::chrono::microseconds(0), // roll up timeout
0, // max roll up count
    1, // module_hash
    createNew_module_auth_logging_module_V1, // New default module
    std::make_shared<::raven::base::ModuleMetrics>("auth_logging_module",1),
    nullptr, // module trace information
    google::WARNING, // module log severity
    getNewLogger() // Logger for logging module
    };
    return &moduleInfo;
}

const raven::base::ModuleInfo *raven::generated::module_auth_logging_module_V1::getModuleInfo() const {
    return GetModuleInfo_module_auth_logging_module_V1();
}

const raven::base::ModuleInfo *raven::generated::module_auth_logging_module_V1::StaticGetModuleInfo() {
return GetModuleInfo_module_auth_logging_module_V1();
}

 bool raven::generated::module_auth_logging_module_V1::setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
        std::string_view recordId,
        const flatbuffers::Table *record) {
    /* Will be used for remote executing a module */
using namespace std::string_view_literals;

    if(recordId == "account_data"sv) {
        account_data = Getrecord_account_data_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "account_level_rollup"sv) {
        account_level_rollup = Getrecord_account_level_rollup_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "card_data"sv) {
        card_data = Getrecord_card_data_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "card_level_rollup"sv) {
        card_level_rollup = Getrecord_card_level_rollup_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "fraud_ruleset_result"sv) {
        fraud_ruleset_result = Getrecord_fraud_ruleset_result_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "ip_address_record"sv) {
        ip_address_record = Getrecord_ip_address_record_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "merchant_level_rollup"sv) {
        merchant_level_rollup = Getrecord_merchant_level_rollup_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "auth_score"sv) {
        auth_score = Getrecord_raven_gbm_model_output_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "fraud_score"sv) {
        fraud_score = Getrecord_raven_gbm_model_output_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "txn_auth_input"sv) {
        txn_auth_input = Getrecord_txn_auth_input_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "txn_auth_output"sv) {
        txn_auth_output = Getrecord_txn_auth_output_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "auth_ruleset_result"sv) {
        auth_ruleset_result = Getrecord_auth_ruleset_result_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "customer_level_rollup"sv) {
        customer_level_rollup = Getrecord_customer_level_rollup_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "customer_data"sv) {
        customer_data = Getrecord_customer_data_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
return false;
}

uint64_t raven::generated::module_auth_logging_module_V1::getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const {
using namespace std::string_view_literals;
        return 0;
}

std::pair<const uint8_t *, uint64_t> raven::generated::module_auth_logging_module_V1::getRecordOutputPtr(std::string_view recordId)  {
using namespace std::string_view_literals;
    /* No record as output */
return {nullptr,0};
}

std::unique_ptr<flatbuffers::DetachedBuffer> raven::generated::module_auth_logging_module_V1::getRecordOutputDetachedPtr(std::string_view recordId) const {
using namespace std::string_view_literals;
    /* No record as output */
return nullptr;
}


std::pair<const uint8_t *, uint64_t> raven::generated::module_auth_logging_module_V1::getRecordInputPtr(std::string_view recordId) {
            if(recordId == "account_data"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_account_data_V1(fbb, account_data->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "account_level_rollup"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_account_level_rollup_V1(fbb, account_level_rollup->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "card_data"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_card_data_V1(fbb, card_data->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "card_level_rollup"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_card_level_rollup_V1(fbb, card_level_rollup->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "fraud_ruleset_result"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_fraud_ruleset_result_V1(fbb, fraud_ruleset_result->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "ip_address_record"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_ip_address_record_V1(fbb, ip_address_record->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "merchant_level_rollup"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_merchant_level_rollup_V1(fbb, merchant_level_rollup->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "auth_score"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_raven_gbm_model_output_V1(fbb, auth_score->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "fraud_score"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_raven_gbm_model_output_V1(fbb, fraud_score->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "txn_auth_input"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_txn_auth_input_V1(fbb, txn_auth_input->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "txn_auth_output"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_txn_auth_output_V1(fbb, txn_auth_output->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "auth_ruleset_result"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_auth_ruleset_result_V1(fbb, auth_ruleset_result->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "customer_level_rollup"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_customer_level_rollup_V1(fbb, customer_level_rollup->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
        if(recordId == "customer_data"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_customer_data_V1(fbb, customer_data->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
    return {nullptr,0};
}








static std::shared_ptr<spdlog::logger> getNewLogger() {
        try {
    using namespace std;
    boost::filesystem::path logPath = FLAGS_module_logging_directory;
    logPath /= "auth_logging_module";
    if (!boost::filesystem::exists(logPath)){
      boost::filesystem::create_directory(logPath);
    }
    if (!boost::filesystem::is_directory(logPath)) {
            LOG(ERROR) << "While trying to initialize a logger for auth_logging_module version - 1 - " << logPath
            << " is not a directory.";
            return nullptr;
    }
    logPath = logPath / "auth_logging_module_V1.log";
    auto rotating_logger =
    spdlog::interval_logger_mt<spdlog::async_factory>("MODULE_auth_logging_module_V1"s,
    logPath.string(),
    1,
    0);
    if(rotating_logger) {
        rotating_logger->set_pattern("%v");
        spdlog::flush_every(std::chrono::seconds(5));
    }

        return rotating_logger;
    } catch(std::exception &e) {
            LOG(ERROR)<<"ERROR while initializing logger "<<e.what();
            return nullptr;
    }
}




folly::Future<folly::Unit> raven::generated::module_auth_logging_module_V1::run() {
    if(!(true)) {
        LOG(INFO)<<"Skipping module for transaction " << messageMetaDataStruct.getTransactionId().toHexString();
        return folly::Unit();
    }
    

nlohmann::json log = nlohmann::json::object();
log["RAVEN_LOGGING_TIME"] = std::chrono::system_clock::now();
log["RAVEN_TRANSACTION_ID"] = messageMetaDataStruct.getTransactionId().toHexString();
    {
    const auto pRecordInfo = raven::base::LookupRecord("account_data", 1);
    if (pRecordInfo && account_data) {
            if(pRecordInfo->toJSON){
              try{
                log["account_data"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(account_data));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for account_data - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["account_data"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of account_data for version " << account_data->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["account_data"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("account_level_rollup", 1);
    if (pRecordInfo && account_level_rollup) {
            if(pRecordInfo->toJSON){
              try{
                log["account_level_rollup"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(account_level_rollup));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for account_level_rollup - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["account_level_rollup"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of account_level_rollup for version " << account_level_rollup->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["account_level_rollup"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("card_data", 1);
    if (pRecordInfo && card_data) {
            if(pRecordInfo->toJSON){
              try{
                log["card_data"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(card_data));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for card_data - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["card_data"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of card_data for version " << card_data->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["card_data"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("card_level_rollup", 1);
    if (pRecordInfo && card_level_rollup) {
            if(pRecordInfo->toJSON){
              try{
                log["card_level_rollup"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(card_level_rollup));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for card_level_rollup - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["card_level_rollup"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of card_level_rollup for version " << card_level_rollup->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["card_level_rollup"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("fraud_ruleset_result", 1);
    if (pRecordInfo && fraud_ruleset_result) {
            if(pRecordInfo->toJSON){
              try{
                log["fraud_ruleset_result"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(fraud_ruleset_result));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for fraud_ruleset_result - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["fraud_ruleset_result"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of fraud_ruleset_result for version " << fraud_ruleset_result->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["fraud_ruleset_result"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("ip_address_record", 1);
    if (pRecordInfo && ip_address_record) {
            if(pRecordInfo->toJSON){
              try{
                log["ip_address_record"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(ip_address_record));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for ip_address_record - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["ip_address_record"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of ip_address_record for version " << ip_address_record->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["ip_address_record"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("merchant_level_rollup", 1);
    if (pRecordInfo && merchant_level_rollup) {
            if(pRecordInfo->toJSON){
              try{
                log["merchant_level_rollup"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(merchant_level_rollup));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for merchant_level_rollup - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["merchant_level_rollup"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of merchant_level_rollup for version " << merchant_level_rollup->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["merchant_level_rollup"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("raven_gbm_model_output", 1);
    if (pRecordInfo && auth_score) {
            if(pRecordInfo->toJSON){
              try{
                log["auth_score"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(auth_score));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for auth_score - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["auth_score"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of raven_gbm_model_output for version " << auth_score->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["auth_score"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("raven_gbm_model_output", 1);
    if (pRecordInfo && fraud_score) {
            if(pRecordInfo->toJSON){
              try{
                log["fraud_score"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(fraud_score));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for fraud_score - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["fraud_score"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of raven_gbm_model_output for version " << fraud_score->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["fraud_score"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("txn_auth_input", 1);
    if (pRecordInfo && txn_auth_input) {
            if(pRecordInfo->toJSON){
              try{
                log["txn_auth_input"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(txn_auth_input));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for txn_auth_input - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["txn_auth_input"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of txn_auth_input for version " << txn_auth_input->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["txn_auth_input"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("txn_auth_output", 1);
    if (pRecordInfo && txn_auth_output) {
            if(pRecordInfo->toJSON){
              try{
                log["txn_auth_output"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(txn_auth_output));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for txn_auth_output - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["txn_auth_output"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of txn_auth_output for version " << txn_auth_output->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["txn_auth_output"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("auth_ruleset_result", 1);
    if (pRecordInfo && auth_ruleset_result) {
            if(pRecordInfo->toJSON){
              try{
                log["auth_ruleset_result"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(auth_ruleset_result));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for auth_ruleset_result - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["auth_ruleset_result"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of auth_ruleset_result for version " << auth_ruleset_result->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["auth_ruleset_result"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("customer_level_rollup", 1);
    if (pRecordInfo && customer_level_rollup) {
            if(pRecordInfo->toJSON){
              try{
                log["customer_level_rollup"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(customer_level_rollup));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for customer_level_rollup - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["customer_level_rollup"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of customer_level_rollup for version " << customer_level_rollup->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["customer_level_rollup"] = nullptr;
    }
    }
    {
    const auto pRecordInfo = raven::base::LookupRecord("customer_data", 1);
    if (pRecordInfo && customer_data) {
            if(pRecordInfo->toJSON){
              try{
                log["customer_data"]=pRecordInfo->toJSON(reinterpret_cast<const flatbuffers::Table *>(customer_data));


              } catch(std::exception &e){
                LOG(ERROR)<<"Error while trying to write log for customer_data - txn id="<< messageMetaDataStruct.getTransactionId().toHexString() << " - " << e.what();
                log["customer_data"] = nullptr;
              }
          } else {
             FB_LOG_EVERY_MS(WARNING, 10000) << "Could not find definition of customer_data for version " << customer_data->raven_header()->version()
                    << ". Logging from module auth_logging_module is incomplete.";
          }
    } else {
       log["customer_data"] = nullptr;
    }
    }
const auto logger = getModuleInfo()->logger;
if(!logger) {
    FB_LOG_EVERY_MS(WARNING, 1000) << "No logger found for module " << getModuleInfo()->module_name << " version "
    << getModuleInfo()->version << " Trying again - success = "
    << static_cast<bool>(const_cast<raven::base::ModuleInfo *>(getModuleInfo())->logger =
    getNewLogger());
}
if (logger) {
   logger->info(log.flatten().dump(-1,' ', false, nlohmann::json::error_handler_t::replace));
}
return folly::Unit();
}









#include "raven-engine/base/Artifact.h"





static bool module_auth_logging_module_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
      raven::base::Module::RegisterModule(
                const_cast<raven::base::ModuleInfo *>(raven::generated::module_auth_logging_module_V1::StaticGetModuleInfo()
        ));
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      raven::base::Module::UnRegisterModule(raven::generated::module_auth_logging_module_V1::StaticGetModuleInfo());
    }
    return true;
}

extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_auth_logging_module_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_auth_logging_module_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Module",     std::vector({    std::string("auth_logging_module"),
})
, 1, module_auth_logging_module_V1_StatusChangeCallBack, "module_auth_logging_module_V1");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("merchant_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("txn_auth_output"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("ip_address_record"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("raven_gbm_model_output"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("account_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("auth_ruleset_result"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("account_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("card_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("txn_auth_input"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("card_level_rollup"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("fraud_ruleset_result"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void module_auth_logging_module_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_auth_logging_module_V1());
     if(!artifact) return;
     module_auth_logging_module_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     module_auth_logging_module_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void module_auth_logging_module_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_auth_logging_module_V1());
    if(!artifact) return;
     module_auth_logging_module_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     module_auth_logging_module_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
