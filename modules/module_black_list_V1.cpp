


#include "module_black_list_V1.h"
#include "lib/include/custom_date.h"
#include <folly/GLog.h>

using namespace raven::types; // bring in the chrono operator


static std::unique_ptr<raven::base::Module> createNew_module_black_list_V1(raven::base::Transaction *txn) {
    return std::make_unique<raven::generated::module_black_list_V1>(txn);
}






static raven::base::ModuleInfo *GetModuleInfo_module_black_list_V1() {
static raven::base::ModuleInfo moduleInfo = {
    "black_list", // Module Name
    "DATA", // Module Type
    // INPUT RECORDS
    {
          {"black_list_input","black_list_input",1,false},
    },
    // OUTPUT RECORDS
    {
        {"black_list_entry","black_list_entry",1,false},
    },
    1, // Module Version
    "", //resourceName - database name. Empty string if no database
      true, // is colocation relevant?
    true, // readOnly - no updates to database
    true, // isSlave ok?
    std::chrono::microseconds(0), // timeout
    std::chrono::microseconds(0), // roll up timeout
0, // max roll up count
    1, // module_hash
    createNew_module_black_list_V1, // New default module
    std::make_shared<::raven::base::ModuleMetrics>("black_list",1),
    nullptr, // module trace information
    google::WARNING, // module log severity
    nullptr
    };
    return &moduleInfo;
}

const raven::base::ModuleInfo *raven::generated::module_black_list_V1::getModuleInfo() const {
    return GetModuleInfo_module_black_list_V1();
}

const raven::base::ModuleInfo *raven::generated::module_black_list_V1::StaticGetModuleInfo() {
return GetModuleInfo_module_black_list_V1();
}

 bool raven::generated::module_black_list_V1::setRecord(std::unique_ptr<flatbuffers::DetachedBuffer> detachedBuffer,
        std::string_view recordId,
        const flatbuffers::Table *record) {
    /* Will be used for remote executing a module */
using namespace std::string_view_literals;

    if(recordId == "black_list_input"sv) {
        black_list_input = Getrecord_black_list_input_V1(reinterpret_cast<const void *>(record));
        flatbufferStorage.push_back(std::move(detachedBuffer));
    return true;
    }
    if(recordId == "black_list_entry"sv) {
    RG_Ptr_black_list_entry = std::move(*UnPackrecord_black_list_entry_V1(Getrecord_black_list_entry_V1(reinterpret_cast<const void *>(record))));
    flatbufferStorage.push_back(std::move(detachedBuffer));
        return true;
    }
return false;
}

uint64_t raven::generated::module_black_list_V1::getRecordOutputPtr(std::string_view recordId, flatbuffers::FlatBufferBuilder &fbb) const {
using namespace std::string_view_literals;
    if(recordId == "black_list_entry"sv) {
        auto offset = raven::generated::Createrecord_black_list_entry_V1(fbb, black_list_entry);
        return fbb.GetSize();
   }
        return 0;
}

std::pair<const uint8_t *, uint64_t> raven::generated::module_black_list_V1::getRecordOutputPtr(std::string_view recordId)  {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "black_list_entry"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_black_list_entry_V1(fbb, black_list_entry);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
    flatbufferStorage.push_back(std::move(detachedBuffer));
    return pair;
    }
return {nullptr,0};
}

std::unique_ptr<flatbuffers::DetachedBuffer> raven::generated::module_black_list_V1::getRecordOutputDetachedPtr(std::string_view recordId) const {
using namespace std::string_view_literals;
    /* No record as output */
    if(recordId == "black_list_entry"sv) {
    flatbuffers::FlatBufferBuilder fbb;
    auto offset = raven::generated::Createrecord_black_list_entry_V1(fbb, black_list_entry);
    auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
    return std::move(detachedBuffer);
    }
return nullptr;
}


std::pair<const uint8_t *, uint64_t> raven::generated::module_black_list_V1::getRecordInputPtr(std::string_view recordId) {
            if(recordId == "black_list_input"sv) {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_black_list_input_V1(fbb, black_list_input->UnPack());
        auto detachedBuffer = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
        auto pair = std::make_pair<const uint8_t *, uint64_t>(detachedBuffer->data(), detachedBuffer->size());
        flatbufferStorage.push_back(std::move(detachedBuffer));
        return pair;
        }
    return {nullptr,0};
}








folly::Future<folly::Unit> raven::generated::module_black_list_V1::run() {
      if(!(true)) {
        LOG(INFO)<<"Skipping module for transaction " << messageMetaDataStruct.getTransactionId().toHexString();
        return folly::Unit();
    }
    

  /* Update colocation logic */
    std::vector<std::string> keyColocation;
    std::shared_ptr<raven::base::VirtualApplicationDB> db = GetResourceAppDBPointerUseCase("${module.databaseName}", keyColocation[0], messageMetaDataStruct.getTransactionId(), admin::DBUseCase::Write, raven::db::DBRole::MASTER);
    if(db) {
    // Local processing


    } else {
    // Remote processing
      flatbuffers::FlatBufferBuilder fbb;
      auto smInput = serializeModuleInputFB(fbb);
      fbb.Finish(smInput);
      auto payload = std::make_unique<flatbuffers::DetachedBuffer>(fbb.Release());
      std::weak_ptr<raven::base::Transaction> wpTxn;
      if(transaction) {
          wpTxn = transaction->weak_from_this();
      }
      LOG(INFO) << "Sending module for remote processing " << messageMetaDataStruct.getTransactionId().toHexString() << " transaction shared_ptr = " << !!transaction;
      return SendPayloadToRemoteDBResource("module", "${module.databaseName}",keyColocation[0],std::move(payload), messageMetaDataStruct).thenValue([this,transaction = (transaction!=nullptr), wpTxn = std::move(wpTxn)](std::unique_ptr<flatbuffers::DetachedBuffer> response) -> folly::Future<folly::Unit> {
        std::shared_ptr<raven::base::Transaction> sp;
        if(transaction) {
            sp = wpTxn.lock();
            if(!sp) return folly::make_exception_wrapper<std::runtime_error>("Transaction no longer exists");
        }
        LOG(INFO) << "Response received from remote node" << messageMetaDataStruct.getTransactionId().toHexString() << " transaction shared_ptr = " << !!sp;
        std::lock_guard lg(responseLock);
        if(!timedout) {
            this->deserializeModuleOutput(this,std::move(response));
            flatbuffers::FlatBufferBuilder fbb_black_list_entry;
            auto offset_black_list_entry = raven::generated::Createrecord_black_list_entry_V1(fbb_black_list_entry, black_list_entry);
                fbb_black_list_entry.Finish(offset_black_list_entry);
                auto BUFFER_black_list_entry = fbb_black_list_entry.Release();
                OUT_black_list_entry = raven::generated::Getrecord_black_list_entry_V1(BUFFER_black_list_entry.data());
        }
        return folly::Unit();
      });

    }
    return folly::Unit();
}









#include "raven-engine/base/Artifact.h"





static bool module_black_list_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
      raven::base::Module::RegisterModule(
                const_cast<raven::base::ModuleInfo *>(raven::generated::module_black_list_V1::StaticGetModuleInfo()
        ));
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      raven::base::Module::UnRegisterModule(raven::generated::module_black_list_V1::StaticGetModuleInfo());
    }
    return true;
}

extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_black_list_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_module_black_list_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Module",     std::vector({    std::string("black_list"),
})
, 1, module_black_list_V1_StatusChangeCallBack, "module_black_list_V1");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("black_list_input"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("black_list_entry"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void module_black_list_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_black_list_V1());
     if(!artifact) return;
     module_black_list_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     module_black_list_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void module_black_list_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_module_black_list_V1());
    if(!artifact) return;
     module_black_list_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     module_black_list_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
