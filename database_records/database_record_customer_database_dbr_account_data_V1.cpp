












#include "database_record_customer_database_dbr_account_data.h"
#include "../records/record_account_data_V1.h"
#include "raven-engine/base/RavenSwitches.h"

using namespace raven::types; // bring in the chrono operator

namespace raven::generated {
static std::string database_record_dbr_account_data_V1_key_extractor(raven::base::Record *record) {
  if (record->version != 1 || record->size != sizeof(raven::generated::record_account_data_V1)) {
    throw raven::base::invalid_version_exception(
        "Expected version 1 but found " + std::to_string(static_cast<uint32_t>(record->version)));
  }
  database_record_dbr_account_data::Key key;
  record_account_data_V1 *rec = reinterpret_cast<raven::generated::record_account_data_V1 *>(record);
  // following fields have been populated by velocity template loop :
  key.colocation.account_number() = fb_read(rec,account_number);
  key.prefix.account_number() = fb_read(rec,account_number);

  // end of loop

  return std::string(reinterpret_cast<char *>(&key), sizeof(key));
}
static rocksdb::CompactionFilter::Decision database_record_dbr_account_data_V1_compaction_filter(int level,
    const rocksdb::Slice & /* key */,
    rocksdb::CompactionFilter::ValueType value_type,
    const rocksdb::Slice &existing_value,
    std::string * /*new_value*/,
    std::string */* skip_until*/) {

  if(value_type!=rocksdb::CompactionFilter::ValueType::kValue) {
    return   rocksdb::CompactionFilter::Decision::kKeep;
  }
  auto account_data = reinterpret_cast<const raven::generated::record_account_data_V1 *>(existing_value.data());

  struct {
    rocksdb::CompactionFilter::Decision decision = rocksdb::CompactionFilter::Decision::kKeep;
    rocksdb::CompactionFilter::Decision keep = rocksdb::CompactionFilter::Decision::kKeep;
    rocksdb::CompactionFilter::Decision remove = rocksdb::CompactionFilter::Decision::kRemove;
    rocksdb::CompactionFilter::Decision changeValue = rocksdb::CompactionFilter::Decision::kChangeValue;
  } raven_compaction_filter_object;
  auto raven_compaction_filter = &raven_compaction_filter_object;
  if(sizeof(raven::generated::record_account_data_V1) != existing_value.size()) {
    throw std::runtime_error("Invalid existing record in db is does not match expected size");
  }
    fb_write(raven_compaction_filter,decision) = fb_read(raven_compaction_filter,keep);

  return raven_compaction_filter->decision;
}
}







#include "raven-engine/base/Artifact.h"





static bool database_record_customer_database_dbr_account_data_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
      raven::base::DatabaseRecord::AddKeyExtractorFunction(raven::generated::getDatabaseName_customer_database(),
                                                           raven::generated::getDatabaseRecordName_dbr_account_data(),
                                                           1,
                                                           raven::generated::database_record_dbr_account_data_V1_key_extractor);
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
            raven::base::DatabaseRecord::AddCompactionFilterFunction(raven::generated::getDatabaseName_customer_database(),
            raven::generated::getDatabaseRecordName_dbr_account_data(),
            1,
            raven::generated::database_record_dbr_account_data_V1_compaction_filter);
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
          raven::base::DatabaseRecord::RemoveCompactionFilterFunction(raven::generated::getDatabaseName_customer_database(),
                                                                 raven::generated::getDatabaseRecordName_dbr_account_data(),
                                                                 1,
                                                                 raven::generated::database_record_dbr_account_data_V1_compaction_filter);
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
              raven::base::DatabaseRecord::RemoveKeyExtractorFunction(raven::generated::getDatabaseName_customer_database(),
                                                         raven::generated::getDatabaseRecordName_dbr_account_data(),
                                                         1,
                                                        raven::generated::database_record_dbr_account_data_V1_key_extractor);
    }
    return true;
}


extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_database_record_customer_database_dbr_account_data_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_database_record_customer_database_dbr_account_data_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("DatabaseRecord",     std::vector({    std::string("customer_database"),
    std::string("dbr_account_data"),
})
, 1, database_record_customer_database_dbr_account_data_V1_StatusChangeCallBack, "database_record_customer_database_dbr_account_data_V1");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("account_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_database"),
})
, "Database", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_database"),
    std::string("dbr_account_data"),
})
, "DatabaseRecord"};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void database_record_customer_database_dbr_account_data_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_database_record_customer_database_dbr_account_data_V1());
     if(!artifact) return;
     database_record_customer_database_dbr_account_data_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     database_record_customer_database_dbr_account_data_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void database_record_customer_database_dbr_account_data_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_database_record_customer_database_dbr_account_data_V1());
    if(!artifact) return;
     database_record_customer_database_dbr_account_data_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     database_record_customer_database_dbr_account_data_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif

