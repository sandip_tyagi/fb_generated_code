





#include "database_record_customer_database_dbr_card_data.h"
#include "../records/record_card_data_V1.h"
#include "lib/include/Error.h"
#include "lib/include/rstring.h"
#include "raven-engine/base/TemplateStaticComparator.h"


namespace raven::generated {

const char *getDatabaseRecordName_dbr_card_data() {
  return "dbr_card_data";
}

static std::string colocation_from_vector_dbr_card_data(const std::vector<std::string> &v) {
  if (v.size() != 1) {
    throw raven::base::raven_runtime_error(
        "Invalid number of fields. Expected 1 field(s) for colocation in dbr_card_data database record but got "
            + std::to_string(v.size()));
  }
  database_record_dbr_card_data::Colocation colocation = {};
  assign_string_to(v[0], colocation.card_number());
  colocation.normalize();
  return std::string(reinterpret_cast<char *>(&colocation), sizeof(colocation));
}

static std::vector<std::string> colocation_to_vector_dbr_card_data(const std::string &s) {
  std::vector<std::string> v;
  database_record_dbr_card_data::Colocation colocation = {};
  if (s.length() != sizeof(colocation)) {
    throw raven::base::raven_runtime_error(
        "Colocation length should be " + std::to_string(sizeof(colocation)) + " but found "
            + std::to_string(s.length()));
  }
  memcpy(&colocation, s.data(), sizeof(colocation));
  v.push_back(assign_to_string(colocation.card_number()));
  return v;
}

static std::string prefix_from_vector_dbr_card_data(const std::vector<std::string> &v) {
  if (v.size() != 1) {
    throw raven::base::raven_runtime_error(
        "Invalid number of fields. Expected 0 field(s) for prefix in dbr_card_data database record but got "
            + std::to_string(v.size()));
  }
  database_record_dbr_card_data::Prefix prefix = {};
  assign_string_to(v[0], prefix.card_number());
  prefix.normalize();
  return std::string(reinterpret_cast<char *>(&prefix), sizeof(prefix));
}


static std::vector<std::string> prefix_to_vector_dbr_card_data(const std::string &s) {
  std::vector<std::string> v;
  database_record_dbr_card_data::Prefix prefix = {};
  if (s.length() != sizeof(prefix)) {
    throw raven::base::raven_runtime_error(
        "Prefix length should be " + std::to_string(sizeof(prefix)) + " but found "
            + std::to_string(s.length()));
  }
  memcpy(&prefix, s.data(), sizeof(prefix));
  v.push_back(assign_to_string(prefix.card_number()));
  return v;
}


static std::string sort_from_vector_dbr_card_data(const std::vector<std::string> &v) {
  if (v.size() != 0) {
    throw raven::base::raven_runtime_error(
        "Invalid number of fields. Expected 0 field(s) for sort in dbr_card_data database record but got "
            + std::to_string(v.size()));
  }
  return std::string();
}

static std::vector<std::string> sort_to_vector_dbr_card_data(const std::string &s [[maybe_unused]]) {
  std::vector<std::string> v;
  return v;
}

static raven::base::DatabaseRecord *get_dbr_card_data_base() {
  static auto comparator = raven::generated::database_record_dbr_card_data_Comparator();
  static raven::base::DatabaseRecord database_record_dbr_card_data
      {raven::generated::getDatabaseName_customer_database(),  // Database Name
       getDatabaseRecordName_dbr_card_data(),  // Database Record Name
       getRecordName_card_data_V1(),
       sizeof(raven::generated::database_record_dbr_card_data::Colocation),
       sizeof(raven::generated::database_record_dbr_card_data::Prefix),
       {
       },
       true, // In memory flag
       colocation_from_vector_dbr_card_data,
       colocation_to_vector_dbr_card_data,
       prefix_from_vector_dbr_card_data,
       prefix_to_vector_dbr_card_data,
       sort_from_vector_dbr_card_data,
       sort_to_vector_dbr_card_data,
       &comparator
      };
  return &database_record_dbr_card_data;
}
}







#include "raven-engine/base/Artifact.h"





static bool database_record_customer_database_dbr_card_data_V0_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
        auto database_record_dbr_card_data = raven::generated::get_dbr_card_data_base();
        raven::base::DatabaseRecord::AddDatabaseRecord(database_record_dbr_card_data->getDatabaseName(),
                                                       database_record_dbr_card_data->getDataRecordName(),
                                                       database_record_dbr_card_data);
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      auto database_record_dbr_card_data = raven::generated::get_dbr_card_data_base();
      raven::base::DatabaseRecord::RemoveDatabaseRecord(database_record_dbr_card_data->getDatabaseName(),
                                                        database_record_dbr_card_data->getDataRecordName(),
                                                        database_record_dbr_card_data);
    }
    return true;
}


extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_database_record_customer_database_dbr_card_data();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_database_record_customer_database_dbr_card_data() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("DatabaseRecord",     std::vector({    std::string("customer_database"),
    std::string("dbr_card_data"),
})
,  database_record_customer_database_dbr_card_data_V0_StatusChangeCallBack, "database_record_customer_database_dbr_card_data");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("card_data"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_database"),
})
, "Database", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void database_record_customer_database_dbr_card_data_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_database_record_customer_database_dbr_card_data());
     if(!artifact) return;
     database_record_customer_database_dbr_card_data_V0_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     database_record_customer_database_dbr_card_data_V0_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void database_record_customer_database_dbr_card_data_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_database_record_customer_database_dbr_card_data());
    if(!artifact) return;
     database_record_customer_database_dbr_card_data_V0_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     database_record_customer_database_dbr_card_data_V0_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif


