





#include "database_record_misc_database_dbr_ip_address_record.h"
#include "../records/record_ip_address_record_V1.h"
#include "lib/include/Error.h"
#include "lib/include/rstring.h"
#include "raven-engine/base/TemplateStaticComparator.h"


namespace raven::generated {

const char *getDatabaseRecordName_dbr_ip_address_record() {
  return "dbr_ip_address_record";
}

static std::string colocation_from_vector_dbr_ip_address_record(const std::vector<std::string> &v) {
  if (v.size() != 1) {
    throw raven::base::raven_runtime_error(
        "Invalid number of fields. Expected 1 field(s) for colocation in dbr_ip_address_record database record but got "
            + std::to_string(v.size()));
  }
  database_record_dbr_ip_address_record::Colocation colocation = {};
  assign_string_to(v[0], colocation.ip_address_first_octet());
  colocation.normalize();
  return std::string(reinterpret_cast<char *>(&colocation), sizeof(colocation));
}

static std::vector<std::string> colocation_to_vector_dbr_ip_address_record(const std::string &s) {
  std::vector<std::string> v;
  database_record_dbr_ip_address_record::Colocation colocation = {};
  if (s.length() != sizeof(colocation)) {
    throw raven::base::raven_runtime_error(
        "Colocation length should be " + std::to_string(sizeof(colocation)) + " but found "
            + std::to_string(s.length()));
  }
  memcpy(&colocation, s.data(), sizeof(colocation));
  v.push_back(assign_to_string(colocation.ip_address_first_octet()));
  return v;
}

static std::string prefix_from_vector_dbr_ip_address_record(const std::vector<std::string> &v) {
  if (v.size() != 1) {
    throw raven::base::raven_runtime_error(
        "Invalid number of fields. Expected 1 field(s) for prefix in dbr_ip_address_record database record but got "
            + std::to_string(v.size()));
  }
  database_record_dbr_ip_address_record::Prefix prefix = {};
  assign_string_to(v[0], prefix.ip_address_first_octet());
  prefix.normalize();
  return std::string(reinterpret_cast<char *>(&prefix), sizeof(prefix));
}


static std::vector<std::string> prefix_to_vector_dbr_ip_address_record(const std::string &s) {
  std::vector<std::string> v;
  database_record_dbr_ip_address_record::Prefix prefix = {};
  if (s.length() != sizeof(prefix)) {
    throw raven::base::raven_runtime_error(
        "Prefix length should be " + std::to_string(sizeof(prefix)) + " but found "
            + std::to_string(s.length()));
  }
  memcpy(&prefix, s.data(), sizeof(prefix));
  v.push_back(assign_to_string(prefix.ip_address_first_octet()));
  return v;
}


static std::string sort_from_vector_dbr_ip_address_record(const std::vector<std::string> &v) {
  if (v.size() != 1) {
    throw raven::base::raven_runtime_error(
        "Invalid number of fields. Expected 1 field(s) for sort in dbr_ip_address_record database record but got "
            + std::to_string(v.size()));
  }
  database_record_dbr_ip_address_record::Sort sort = {};
  assign_string_to(v[0], sort.ip_address_begin());
  sort.normalize();
  return std::string(reinterpret_cast<char *>(&sort), sizeof(sort));
}

static std::vector<std::string> sort_to_vector_dbr_ip_address_record(const std::string &s [[maybe_unused]]) {
  std::vector<std::string> v;
  database_record_dbr_ip_address_record::Sort sort = {};
  if (s.length() != sizeof(sort)) {
    throw raven::base::raven_runtime_error(
        "Prefix length should be " + std::to_string(sizeof(sort)) + " but found "
            + std::to_string(s.length()));
  }
  memcpy(&sort, s.data(), sizeof(sort));
  v.push_back(assign_to_string(sort.ip_address_begin()));
  return v;
}

static raven::base::DatabaseRecord *get_dbr_ip_address_record_base() {
  static auto comparator = raven::generated::database_record_dbr_ip_address_record_Comparator();
  static raven::base::DatabaseRecord database_record_dbr_ip_address_record
      {raven::generated::getDatabaseName_misc_database(),  // Database Name
       getDatabaseRecordName_dbr_ip_address_record(),  // Database Record Name
       getRecordName_ip_address_record_V1(),
       sizeof(raven::generated::database_record_dbr_ip_address_record::Colocation),
       sizeof(raven::generated::database_record_dbr_ip_address_record::Prefix),
       {
           {sizeof(raven::generated::database_record_dbr_ip_address_record::Sort().ip_address_begin()),
            raven::base::SortOrder::DESCENDING},  // Sort Keys
       },
       true, // In memory flag
       colocation_from_vector_dbr_ip_address_record,
       colocation_to_vector_dbr_ip_address_record,
       prefix_from_vector_dbr_ip_address_record,
       prefix_to_vector_dbr_ip_address_record,
       sort_from_vector_dbr_ip_address_record,
       sort_to_vector_dbr_ip_address_record,
       &comparator
      };
  return &database_record_dbr_ip_address_record;
}
}







#include "raven-engine/base/Artifact.h"





static bool database_record_misc_database_dbr_ip_address_record_V0_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
        auto database_record_dbr_ip_address_record = raven::generated::get_dbr_ip_address_record_base();
        raven::base::DatabaseRecord::AddDatabaseRecord(database_record_dbr_ip_address_record->getDatabaseName(),
                                                       database_record_dbr_ip_address_record->getDataRecordName(),
                                                       database_record_dbr_ip_address_record);
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      auto database_record_dbr_ip_address_record = raven::generated::get_dbr_ip_address_record_base();
      raven::base::DatabaseRecord::RemoveDatabaseRecord(database_record_dbr_ip_address_record->getDatabaseName(),
                                                        database_record_dbr_ip_address_record->getDataRecordName(),
                                                        database_record_dbr_ip_address_record);
    }
    return true;
}


extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_database_record_misc_database_dbr_ip_address_record();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_database_record_misc_database_dbr_ip_address_record() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("DatabaseRecord",     std::vector({    std::string("misc_database"),
    std::string("dbr_ip_address_record"),
})
,  database_record_misc_database_dbr_ip_address_record_V0_StatusChangeCallBack, "database_record_misc_database_dbr_ip_address_record");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("ip_address_record"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("misc_database"),
})
, "Database", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void database_record_misc_database_dbr_ip_address_record_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_database_record_misc_database_dbr_ip_address_record());
     if(!artifact) return;
     database_record_misc_database_dbr_ip_address_record_V0_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     database_record_misc_database_dbr_ip_address_record_V0_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void database_record_misc_database_dbr_ip_address_record_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_database_record_misc_database_dbr_ip_address_record());
    if(!artifact) return;
     database_record_misc_database_dbr_ip_address_record_V0_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     database_record_misc_database_dbr_ip_address_record_V0_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif


