#pragma once


#include "raven-engine/base/DatabaseRecord.h"
#include "raven-engine/base/Record.h"
#include "../databases/database_customer_database.h"
#include "lib/include/rstring.h"
#include "lib/include/big_endian.h"
#include "lib/include/key_encoder.h"
#include "lib/include/key_decoder.h"
#include "lib/include/key_comparator.h"

namespace raven::generated {
using namespace raven::types;
#pragma pack(push, 1)
class database_record_dbr_account_data {
 public:
  struct Colocation {
                    string colocation_0 = raven::missing;
                string &account_number() { return colocation_0; }
//                const string &account_number() const { return colocation_0; }

        void normalize() {
        }
    bool isEmpty() {
              if(!(account_number() == raven::missing)) return false;
      return true;
    }
  };

  struct Prefix {

            string prefix_0 = raven::missing;
            string &account_number() { return prefix_0; }
//            const string &account_number() const { return prefix_0; }

    void normalize() {
    }

    bool isEmpty() {
            if(!(account_number() == raven::missing)) return false;
        return true;
    }

  };


  struct Key {
    Colocation colocation;
    Prefix prefix;


    void normalize() {
      // Normalize the key - i.e. remove any padding characters
      colocation.normalize();
      prefix.normalize();
    }

    bool isEmpty() {
      return colocation.isEmpty() && prefix.isEmpty() ;
    }

    std::string getKey(){
            raven::db::key_coder::KeyBuilder keyBuilder;
            auto key = keyBuilder.clear()
                        .withDBRecordId(200)
                        .append(colocation.account_number())
                        .append(prefix.account_number())
                        .build();
    }

    bool parseKey(std::string_view key) {
        raven::db::key_coder::KeyDecoder keyDecoder(reinterpret_cast<const uint8_t*>(key.data()),key.size());
                    colocation.account_number()=keyDecoder.decode<string>();
                    prefix.account_number()=keyDecoder.decode<string>();
    }

  };
};
class database_record_dbr_account_data_Comparator : public rocksdb::Comparator {
private:
    std::string name_;
public:

    database_record_dbr_account_data_Comparator() = default;


    /**
     * Compare two keys based on colocation, prefix length, and sort length along taking into consideration the sort
     * order
     * @param a Key 1
     * @param b Key 2
     * @return 0 if keys are equal, >0 if key 1 should be placed after key 2, <0 if key 2 should be placed after key 1
     */
    int Compare(const rocksdb::Slice &a, const rocksdb::Slice &b) const override {
      return raven::db::key_coder::KeyComparator()(a, b);
    }

    /**
     * Checks if both keys are equal
     * @param a
     * @param b
     * @return true if both keys are equal
     */
    bool Equal(const rocksdb::Slice &a, const rocksdb::Slice &b) const override {
        return Compare(a,b) == 0;
    }

    /**
     * Name of the comparator. Used when rocksdb creates the options file
     * @return Name of the comparator
     */
    const char *Name() const override {
        return "customer_database_dbr_account_data_comparator";
    }

    // Advanced functions: these are used to reduce the space requirements
    // for internal data structures like index blocks.

    // If *start < limit, changes *start to a short string in [start,limit).
    // Simple comparator implementations may return with *start unchanged,
    // i.e., an implementation of this method that does nothing is correct.
    void FindShortestSeparator(
    std::string * /*start*/,
    const rocksdb::Slice & /*limit*/) const override {}

    // Changes *key to a short string >= *key.
    // Simple comparator implementations may return with *key unchanged,
    // i.e., an implementation of this method that does nothing is correct.
    void FindShortSuccessor(std::string * /*key*/) const override {
    }

    };



#pragma pack(pop)
const char *getDatabaseRecordName_dbr_account_data();
}
