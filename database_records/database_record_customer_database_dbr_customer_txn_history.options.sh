#!/bin/bash
set -e
set -o pipefail
echo 'Adding database record options to database'
if [ ! -f "database_record_customer_database_dbr_customer_txn_history.options" ] ; then
  echo 'This script needs to be run from database_records directory'
  exit 1
fi
if [ ! -f "../databases/customer_database.options" ] ; then
  echo 'Database options file not found'
  exit 1
fi
if [ ! -f ../_build/options/customer_database.options ] ; then
    echo 'Database options needs to be built first'
    exit 1
fi

cat database_record_customer_database_dbr_customer_txn_history.options >> ../_build/options/customer_database.options
