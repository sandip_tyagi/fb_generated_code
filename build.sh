#!/bin/bash
set -e
set -o pipefail
if [[ -z "$CMAKE_BUILD_TYPE" ]]; then
  CMAKE_BUILD_TYPE=Debug
fi
if [[ -z "$RAVEN_LIBS" ]]; then
  if [[ -f "raven_env.sh" ]]; then
    . ./raven_env.sh
  else
    echo "RAVEN_LIBS not defined. Please ensure that environment variables are set or a raven_env.sh is present in the current directory" 1>&2
    exit 1
  fi
fi

if [[ -z "$NUM_CORES" ]]; then
  NUM_CORES=$(nproc 2>/dev/null || echo 6)
  export NUM_CORES=${NUM_CORES}
fi

echo Build type CMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
echo Build will use ${NUM_CORES} cores

CURRDIR="$PWD"
cd "$(dirname "$0")"
if [[ "$PWD" != "$CURRDIR" ]]; then
  echo "build needs to be run from the source directory"
  exit 1
fi
. raven_env.sh
COMMIT_ID=$(git rev-parse HEAD)
# configuration

UNIQUE_DIRS=$(ls */*.partial_cmake | xargs dirname | uniq)

RAVEN_FBS_INCLUDE=`find $RAVEN_INCLUDE -name "RecordHeader.fbs"|head -1|xargs -r dirname`

cat >CMakeLists.txt <<EOF
cmake_minimum_required(VERSION 3.8)

project(RAVEN-BUILD-${COMMIT_ID})

set(AUTHOR "support@valebridgetech.com")

#-----------------------------------------------------------------------------
# Enable ccache if not already enabled by symlink masquerading and if no other
# CMake compiler launchers are already defined
#-----------------------------------------------------------------------------
find_program(CCACHE_EXECUTABLE ccache)
mark_as_advanced(CCACHE_EXECUTABLE)
if (CCACHE_EXECUTABLE)
    foreach (LANG C CXX)
        if (NOT DEFINED CMAKE_\${LANG}_COMPILER_LAUNCHER AND NOT CMAKE_\${LANG}_COMPILER MATCHES ".*/ccache$")
            message(STATUS "Enabling ccache for \${LANG}")
            set(CMAKE_\${LANG}_COMPILER_LAUNCHER \${CCACHE_EXECUTABLE} CACHE STRING "")
        endif ()
    endforeach ()
endif ()

list(APPEND CMAKE_MODULE_PATH "\${PROJECT_SOURCE_DIR}/cmake")
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(CMAKE_CXX_FLAGS "\${CMAKE_CXX_FLAGS} -grecord-gcc-switches")
set(CMAKE_CXX_FLAGS "\${CMAKE_CXX_FLAGS} -std=c++17 -fno-omit-frame-pointer -g -pedantic -pipe -fpermissive")
set(CMAKE_CXX_FLAGS "\${CMAKE_CXX_FLAGS} -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wmissing-declarations -Wmissing-include-dirs -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-overflow=5 -Wswitch-default -Wundef -Wzero-as-null-pointer-constant -Wabi")
set(CMAKE_CXX_FLAGS "\${CMAKE_CXX_FLAGS} -Werror=format-security")
set(CMAKE_CXX_FLAGS_DEBUG "\${CMAKE_CXX_FLAGS_DEBUG} -Og")
set(CMAKE_CXX_FLAGS_RELEASE "\${CMAKE_CXX_FLAGS_RELEASE} -O3")
set(CMAKE_SHARED_LINKER_FLAGS "\${CMAKE_SHARED_LINKER_FLAGS} -z defs -z now -z relro")
set(CMAKE_EXE_LINKER_FLAGS "\${CMAKE_EXE_LINKER_FLAGS} -z defs -z now -z relro")
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(RAVEN_FBS_INCLUDE "${RAVEN_FBS_INCLUDE}")
if(DEFINED ENV{RAVEN_AUTO_REGISTER_ARTIFACTS})
message("RAVEN_AUTO_REGISTER_ARTIFACTS is defined in the environment.")
add_definitions(-DRAVEN_AUTO_REGISTER_ARTIFACTS)
endif()

find_package(FlatBuffers REQUIRED)
add_definitions(-DFLATBUFFERS_NAN_DEFAULTS -DFLATBUFFERS_HAS_STRING_VIEW)

SET(CMAKE_EXPORT_COMPILE_COMMANDS ON)
link_directories($RAVEN_LIBS)
include_directories(SYSTEM $RAVEN_INCLUDE)
#include_directories(${CMAKE_CURRENT_BINARY_DIR}/records)
#include_directories(\${CMAKE_CURRENT_BINARY_DIR}/records)
include_directories(\${CMAKE_CURRENT_BINARY_DIR})

EOF
echo "$UNIQUE_DIRS" | sort | while read line; do
  echo 'add_subdirectory('${line}')' >>CMakeLists.txt
  cd "$line" && (echo 'set(CMAKE_INCLUDE_CURRENT_DIR ON)' && cat *.partial_cmake) >CMakeLists.txt && cd ..
done
echo "Generating cmake files is complete."

if [[ ! -d _artifacts ]]; then
  mkdir _artifacts
else
  rm -Rf _artifacts/*
fi

if [[ -d _build ]]; then
  find _build -name '*.so' -or -name '*.options' -or -name '*.sh' | xargs -r rm
fi

if [[ ! -d _build ]]; then
  if which ninja >/dev/null 2>&1; then
    mkdir _build && cd _build && cmake -DCMAKE_SKIP_RPATH=TRUE -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -GNinja .. && ninja && cd .. || exit 1
  else
    mkdir _build && cd _build && cmake -DCMAKE_SKIP_RPATH=TRUE -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} .. && nice make -j ${NUM_CORES} && cd .. || exit 1
  fi
else
  if which ninja >/dev/null 2>&1; then
    cd _build && ninja -k999 && cd .. || exit 1
  else
    cd _build && nice make -j ${NUM_CORES} -k && cd .. || exit 1
  fi
fi

if [[ -d databases ]]; then
  (cd databases && ls *.sh | while read line; do
    . ./"$line"
  done)
fi
if [[ -d database_records ]]; then
  (cd database_records && ls *.sh | while read line; do
    . ./"$line"
  done)
fi
find _build -name '*.so' | xargs -I{} cp {} _artifacts
if [[ -d _build/options ]]; then
  cp -r _build/options _artifacts/options
fi
echo ""

echo ARTIFACTS_DIRECTORY=$PWD/_artifacts
