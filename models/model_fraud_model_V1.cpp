#include "model_fraud_model_V1.h"
#include <iostream>
#include <cmath>
#include <string_view>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <fstream>
#include <vector>
#include <sstream>
#include <limits>
#include <algorithm>
#include <glog/logging.h>
#include <folly/String.h>
#include <folly/Likely.h>
#include "lib/include/missing.h"
#include "raven-engine/base/Model.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wpedantic"
using namespace std::literals;
using json = nlohmann::json;
namespace raven::generated {
enum class r_ec_TARGET :  uint16_t{
__RAVEN__MISSING__,
r_0,
r_1
};
enum class r_ec_V106 :  uint16_t{
__RAVEN__MISSING__,
r_00,
r_22,
r_88,
r_ZZ,
r_01,
r_89,
r_02,
r_24,
r_04,
r_05,
r_06,
r_91,
r_71,
r_72,
r_51,
r_73,
r_96,
r_EMPTY,
r_53,
r_75,
r_32,
r_54,
r_76,
r_55,
r_56,
r_57,
r_14,
r_36,
r_58,
r_N7,
r_F1,
r_60,
r_61,
r_40,
r_62,
r_41,
r_85,
r_86,
r_21,
r_65
};
enum class r_ec_V140 :  uint16_t{
__RAVEN__MISSING__,
r_A,
r_C,
r_D,
r_E,
r_V,
r_I,
r_J,
r_EMPTY,
r_M
};
enum class r_ec_V60 :  uint16_t{
__RAVEN__MISSING__,
r_0,
r_1,
r_2,
r_EMPTY
};
enum class r_ec_V41 :  uint16_t{
__RAVEN__MISSING__,
r_Y,
r_EMPTY,
r_N
};
enum class r_ec_V42 :  uint16_t{
__RAVEN__MISSING__,
r_Y,
r_EMPTY,
r_N
};
enum class r_ec_V23 :  uint16_t{
__RAVEN__MISSING__,
r_A,
r_B,
r_C,
r_D,
r_E,
r_Z
};
enum class r_ec_V12 :  uint16_t{
__RAVEN__MISSING__,
r_00,
r_88,
r_01,
r_89,
r_02,
r_04,
r_06,
r_91,
r_71,
r_72,
r_51,
r_73,
r_96,
r_53,
r_75,
r_54,
r_76,
r_55,
r_56,
r_57,
r_14,
r_36,
r_58,
r_N7,
r_F1,
r_61,
r_62,
r_41,
r_85,
r_86,
r_65
};
enum class r_ec_V38 :  uint16_t{
__RAVEN__MISSING__,
r_D,
r_E,
r_U,
r_W,
r_X,
r_J,
r_K,
r_L,
r_EMPTY,
r_N
};
enum class r_ec_V189 :  uint16_t{
__RAVEN__MISSING__,
r_Y,
r_N
};
enum class r_ec_V28 :  uint16_t{
__RAVEN__MISSING__,
r_E,
r_V,
r_J,
r_M
};
struct pmml_input_struct_1{
double V6;
r_ec_V106 V106;
r_ec_V140 V140;
r_ec_V60 V60;
double V20;
r_ec_V42 V42;
r_ec_V12 V12;
r_ec_V23 V23;
double V47;
double V104;
r_ec_V38 V38;
double V101;
r_ec_V189 V189;
r_ec_V28 V28;
};

struct pmml_input_struct_2{
};

struct pmml_input_struct_3{
};

struct pmml_input_struct_4{
};

struct pmml_input_struct_5{
};

struct pmml_input_struct_6{
};

struct pmml_output_struct{
int64_t Node1=std::numeric_limits<int>::min();
double Response1=NAN;
int64_t Node3=std::numeric_limits<int>::min();
double PROB_0=NAN;
double Response4=NAN;
double PROB_1=NAN;
int64_t Node2=std::numeric_limits<int>::min();
double Response3=NAN;
int64_t PREDICTION=std::numeric_limits<int>::min();
double Response2=NAN;
int64_t Node4=std::numeric_limits<int>::min();
int64_t TARGET=std::numeric_limits<int>::min();
};
static std::string toString(const pmml_output_struct &o);
static r_ec_TARGET r_ec_TARGET_from_string(const std::string &str) {
if (str == "0"sv) return r_ec_TARGET::r_0;
if (str == "1"sv) return r_ec_TARGET::r_1;
return r_ec_TARGET::__RAVEN__MISSING__;
}
static std::string r_ec_TARGET_to_string(const r_ec_TARGET val) {
switch (val) {
case r_ec_TARGET::r_0:
return "0"s;
case r_ec_TARGET::r_1:
return "1"s;
default:
return std::string();
}
}
static r_ec_V106 r_ec_V106_from_string(const std::string &str) {
if (str == "00"sv) return r_ec_V106::r_00;
if (str == "01"sv) return r_ec_V106::r_01;
if (str == "02"sv) return r_ec_V106::r_02;
if (str == "04"sv) return r_ec_V106::r_04;
if (str == "05"sv) return r_ec_V106::r_05;
if (str == "06"sv) return r_ec_V106::r_06;
if (str == "14"sv) return r_ec_V106::r_14;
if (str == "21"sv) return r_ec_V106::r_21;
if (str == "22"sv) return r_ec_V106::r_22;
if (str == "24"sv) return r_ec_V106::r_24;
if (str == "32"sv) return r_ec_V106::r_32;
if (str == "36"sv) return r_ec_V106::r_36;
if (str == "40"sv) return r_ec_V106::r_40;
if (str == "41"sv) return r_ec_V106::r_41;
if (str == "51"sv) return r_ec_V106::r_51;
if (str == "53"sv) return r_ec_V106::r_53;
if (str == "54"sv) return r_ec_V106::r_54;
if (str == "55"sv) return r_ec_V106::r_55;
if (str == "56"sv) return r_ec_V106::r_56;
if (str == "57"sv) return r_ec_V106::r_57;
if (str == "58"sv) return r_ec_V106::r_58;
if (str == "60"sv) return r_ec_V106::r_60;
if (str == "61"sv) return r_ec_V106::r_61;
if (str == "62"sv) return r_ec_V106::r_62;
if (str == "65"sv) return r_ec_V106::r_65;
if (str == "71"sv) return r_ec_V106::r_71;
if (str == "72"sv) return r_ec_V106::r_72;
if (str == "73"sv) return r_ec_V106::r_73;
if (str == "75"sv) return r_ec_V106::r_75;
if (str == "76"sv) return r_ec_V106::r_76;
if (str == "85"sv) return r_ec_V106::r_85;
if (str == "86"sv) return r_ec_V106::r_86;
if (str == "88"sv) return r_ec_V106::r_88;
if (str == "89"sv) return r_ec_V106::r_89;
if (str == "91"sv) return r_ec_V106::r_91;
if (str == "96"sv) return r_ec_V106::r_96;
if (str == "EMPTY"sv) return r_ec_V106::r_EMPTY;
if (str == "F1"sv) return r_ec_V106::r_F1;
if (str == "N7"sv) return r_ec_V106::r_N7;
if (str == "ZZ"sv) return r_ec_V106::r_ZZ;
return r_ec_V106::__RAVEN__MISSING__;
}
static std::string r_ec_V106_to_string(const r_ec_V106 val) {
switch (val) {
case r_ec_V106::r_00:
return "00"s;
case r_ec_V106::r_01:
return "01"s;
case r_ec_V106::r_02:
return "02"s;
case r_ec_V106::r_04:
return "04"s;
case r_ec_V106::r_05:
return "05"s;
case r_ec_V106::r_06:
return "06"s;
case r_ec_V106::r_14:
return "14"s;
case r_ec_V106::r_21:
return "21"s;
case r_ec_V106::r_22:
return "22"s;
case r_ec_V106::r_24:
return "24"s;
case r_ec_V106::r_32:
return "32"s;
case r_ec_V106::r_36:
return "36"s;
case r_ec_V106::r_40:
return "40"s;
case r_ec_V106::r_41:
return "41"s;
case r_ec_V106::r_51:
return "51"s;
case r_ec_V106::r_53:
return "53"s;
case r_ec_V106::r_54:
return "54"s;
case r_ec_V106::r_55:
return "55"s;
case r_ec_V106::r_56:
return "56"s;
case r_ec_V106::r_57:
return "57"s;
case r_ec_V106::r_58:
return "58"s;
case r_ec_V106::r_60:
return "60"s;
case r_ec_V106::r_61:
return "61"s;
case r_ec_V106::r_62:
return "62"s;
case r_ec_V106::r_65:
return "65"s;
case r_ec_V106::r_71:
return "71"s;
case r_ec_V106::r_72:
return "72"s;
case r_ec_V106::r_73:
return "73"s;
case r_ec_V106::r_75:
return "75"s;
case r_ec_V106::r_76:
return "76"s;
case r_ec_V106::r_85:
return "85"s;
case r_ec_V106::r_86:
return "86"s;
case r_ec_V106::r_88:
return "88"s;
case r_ec_V106::r_89:
return "89"s;
case r_ec_V106::r_91:
return "91"s;
case r_ec_V106::r_96:
return "96"s;
case r_ec_V106::r_EMPTY:
return "EMPTY"s;
case r_ec_V106::r_F1:
return "F1"s;
case r_ec_V106::r_N7:
return "N7"s;
case r_ec_V106::r_ZZ:
return "ZZ"s;
default:
return std::string();
}
}
static r_ec_V140 r_ec_V140_from_string(const std::string &str) {
if (str == "A"sv) return r_ec_V140::r_A;
if (str == "C"sv) return r_ec_V140::r_C;
if (str == "D"sv) return r_ec_V140::r_D;
if (str == "E"sv) return r_ec_V140::r_E;
if (str == "EMPTY"sv) return r_ec_V140::r_EMPTY;
if (str == "I"sv) return r_ec_V140::r_I;
if (str == "J"sv) return r_ec_V140::r_J;
if (str == "M"sv) return r_ec_V140::r_M;
if (str == "V"sv) return r_ec_V140::r_V;
return r_ec_V140::__RAVEN__MISSING__;
}
static std::string r_ec_V140_to_string(const r_ec_V140 val) {
switch (val) {
case r_ec_V140::r_A:
return "A"s;
case r_ec_V140::r_C:
return "C"s;
case r_ec_V140::r_D:
return "D"s;
case r_ec_V140::r_E:
return "E"s;
case r_ec_V140::r_EMPTY:
return "EMPTY"s;
case r_ec_V140::r_I:
return "I"s;
case r_ec_V140::r_J:
return "J"s;
case r_ec_V140::r_M:
return "M"s;
case r_ec_V140::r_V:
return "V"s;
default:
return std::string();
}
}
static r_ec_V60 r_ec_V60_from_string(const std::string &str) {
if (str == "0"sv) return r_ec_V60::r_0;
if (str == "1"sv) return r_ec_V60::r_1;
if (str == "2"sv) return r_ec_V60::r_2;
if (str == "EMPTY"sv) return r_ec_V60::r_EMPTY;
return r_ec_V60::__RAVEN__MISSING__;
}
static std::string r_ec_V60_to_string(const r_ec_V60 val) {
switch (val) {
case r_ec_V60::r_0:
return "0"s;
case r_ec_V60::r_1:
return "1"s;
case r_ec_V60::r_2:
return "2"s;
case r_ec_V60::r_EMPTY:
return "EMPTY"s;
default:
return std::string();
}
}
static r_ec_V41 r_ec_V41_from_string(const std::string &str) {
if (str == "EMPTY"sv) return r_ec_V41::r_EMPTY;
if (str == "N"sv) return r_ec_V41::r_N;
if (str == "Y"sv) return r_ec_V41::r_Y;
return r_ec_V41::__RAVEN__MISSING__;
}
static std::string r_ec_V41_to_string(const r_ec_V41 val) {
switch (val) {
case r_ec_V41::r_EMPTY:
return "EMPTY"s;
case r_ec_V41::r_N:
return "N"s;
case r_ec_V41::r_Y:
return "Y"s;
default:
return std::string();
}
}
static r_ec_V42 r_ec_V42_from_string(const std::string &str) {
if (str == "EMPTY"sv) return r_ec_V42::r_EMPTY;
if (str == "N"sv) return r_ec_V42::r_N;
if (str == "Y"sv) return r_ec_V42::r_Y;
return r_ec_V42::__RAVEN__MISSING__;
}
static std::string r_ec_V42_to_string(const r_ec_V42 val) {
switch (val) {
case r_ec_V42::r_EMPTY:
return "EMPTY"s;
case r_ec_V42::r_N:
return "N"s;
case r_ec_V42::r_Y:
return "Y"s;
default:
return std::string();
}
}
static r_ec_V23 r_ec_V23_from_string(const std::string &str) {
if (str == "A"sv) return r_ec_V23::r_A;
if (str == "B"sv) return r_ec_V23::r_B;
if (str == "C"sv) return r_ec_V23::r_C;
if (str == "D"sv) return r_ec_V23::r_D;
if (str == "E"sv) return r_ec_V23::r_E;
if (str == "Z"sv) return r_ec_V23::r_Z;
return r_ec_V23::__RAVEN__MISSING__;
}
static std::string r_ec_V23_to_string(const r_ec_V23 val) {
switch (val) {
case r_ec_V23::r_A:
return "A"s;
case r_ec_V23::r_B:
return "B"s;
case r_ec_V23::r_C:
return "C"s;
case r_ec_V23::r_D:
return "D"s;
case r_ec_V23::r_E:
return "E"s;
case r_ec_V23::r_Z:
return "Z"s;
default:
return std::string();
}
}
static r_ec_V12 r_ec_V12_from_string(const std::string &str) {
if (str == "00"sv) return r_ec_V12::r_00;
if (str == "01"sv) return r_ec_V12::r_01;
if (str == "02"sv) return r_ec_V12::r_02;
if (str == "04"sv) return r_ec_V12::r_04;
if (str == "06"sv) return r_ec_V12::r_06;
if (str == "14"sv) return r_ec_V12::r_14;
if (str == "36"sv) return r_ec_V12::r_36;
if (str == "41"sv) return r_ec_V12::r_41;
if (str == "51"sv) return r_ec_V12::r_51;
if (str == "53"sv) return r_ec_V12::r_53;
if (str == "54"sv) return r_ec_V12::r_54;
if (str == "55"sv) return r_ec_V12::r_55;
if (str == "56"sv) return r_ec_V12::r_56;
if (str == "57"sv) return r_ec_V12::r_57;
if (str == "58"sv) return r_ec_V12::r_58;
if (str == "61"sv) return r_ec_V12::r_61;
if (str == "62"sv) return r_ec_V12::r_62;
if (str == "65"sv) return r_ec_V12::r_65;
if (str == "71"sv) return r_ec_V12::r_71;
if (str == "72"sv) return r_ec_V12::r_72;
if (str == "73"sv) return r_ec_V12::r_73;
if (str == "75"sv) return r_ec_V12::r_75;
if (str == "76"sv) return r_ec_V12::r_76;
if (str == "85"sv) return r_ec_V12::r_85;
if (str == "86"sv) return r_ec_V12::r_86;
if (str == "88"sv) return r_ec_V12::r_88;
if (str == "89"sv) return r_ec_V12::r_89;
if (str == "91"sv) return r_ec_V12::r_91;
if (str == "96"sv) return r_ec_V12::r_96;
if (str == "F1"sv) return r_ec_V12::r_F1;
if (str == "N7"sv) return r_ec_V12::r_N7;
return r_ec_V12::__RAVEN__MISSING__;
}
static std::string r_ec_V12_to_string(const r_ec_V12 val) {
switch (val) {
case r_ec_V12::r_00:
return "00"s;
case r_ec_V12::r_01:
return "01"s;
case r_ec_V12::r_02:
return "02"s;
case r_ec_V12::r_04:
return "04"s;
case r_ec_V12::r_06:
return "06"s;
case r_ec_V12::r_14:
return "14"s;
case r_ec_V12::r_36:
return "36"s;
case r_ec_V12::r_41:
return "41"s;
case r_ec_V12::r_51:
return "51"s;
case r_ec_V12::r_53:
return "53"s;
case r_ec_V12::r_54:
return "54"s;
case r_ec_V12::r_55:
return "55"s;
case r_ec_V12::r_56:
return "56"s;
case r_ec_V12::r_57:
return "57"s;
case r_ec_V12::r_58:
return "58"s;
case r_ec_V12::r_61:
return "61"s;
case r_ec_V12::r_62:
return "62"s;
case r_ec_V12::r_65:
return "65"s;
case r_ec_V12::r_71:
return "71"s;
case r_ec_V12::r_72:
return "72"s;
case r_ec_V12::r_73:
return "73"s;
case r_ec_V12::r_75:
return "75"s;
case r_ec_V12::r_76:
return "76"s;
case r_ec_V12::r_85:
return "85"s;
case r_ec_V12::r_86:
return "86"s;
case r_ec_V12::r_88:
return "88"s;
case r_ec_V12::r_89:
return "89"s;
case r_ec_V12::r_91:
return "91"s;
case r_ec_V12::r_96:
return "96"s;
case r_ec_V12::r_F1:
return "F1"s;
case r_ec_V12::r_N7:
return "N7"s;
default:
return std::string();
}
}
static r_ec_V38 r_ec_V38_from_string(const std::string &str) {
if (str == "D"sv) return r_ec_V38::r_D;
if (str == "E"sv) return r_ec_V38::r_E;
if (str == "EMPTY"sv) return r_ec_V38::r_EMPTY;
if (str == "J"sv) return r_ec_V38::r_J;
if (str == "K"sv) return r_ec_V38::r_K;
if (str == "L"sv) return r_ec_V38::r_L;
if (str == "N"sv) return r_ec_V38::r_N;
if (str == "U"sv) return r_ec_V38::r_U;
if (str == "W"sv) return r_ec_V38::r_W;
if (str == "X"sv) return r_ec_V38::r_X;
return r_ec_V38::__RAVEN__MISSING__;
}
static std::string r_ec_V38_to_string(const r_ec_V38 val) {
switch (val) {
case r_ec_V38::r_D:
return "D"s;
case r_ec_V38::r_E:
return "E"s;
case r_ec_V38::r_EMPTY:
return "EMPTY"s;
case r_ec_V38::r_J:
return "J"s;
case r_ec_V38::r_K:
return "K"s;
case r_ec_V38::r_L:
return "L"s;
case r_ec_V38::r_N:
return "N"s;
case r_ec_V38::r_U:
return "U"s;
case r_ec_V38::r_W:
return "W"s;
case r_ec_V38::r_X:
return "X"s;
default:
return std::string();
}
}
static r_ec_V189 r_ec_V189_from_string(const std::string &str) {
if (str == "N"sv) return r_ec_V189::r_N;
if (str == "Y"sv) return r_ec_V189::r_Y;
return r_ec_V189::__RAVEN__MISSING__;
}
static std::string r_ec_V189_to_string(const r_ec_V189 val) {
switch (val) {
case r_ec_V189::r_N:
return "N"s;
case r_ec_V189::r_Y:
return "Y"s;
default:
return std::string();
}
}
static r_ec_V28 r_ec_V28_from_string(const std::string &str) {
if (str == "E"sv) return r_ec_V28::r_E;
if (str == "J"sv) return r_ec_V28::r_J;
if (str == "M"sv) return r_ec_V28::r_M;
if (str == "V"sv) return r_ec_V28::r_V;
return r_ec_V28::__RAVEN__MISSING__;
}
static std::string r_ec_V28_to_string(const r_ec_V28 val) {
switch (val) {
case r_ec_V28::r_E:
return "E"s;
case r_ec_V28::r_J:
return "J"s;
case r_ec_V28::r_M:
return "M"s;
case r_ec_V28::r_V:
return "V"s;
default:
return std::string();
}
}
static void to_json(json &j, const pmml_input_struct_4 &s) {
}
static void to_json(json &j, const pmml_input_struct_5 &s) {
}
static void to_json(json &j, const pmml_input_struct_2 &s) {
}
static void to_json(json &j, const pmml_input_struct_3 &s) {
}
static void to_json(json &j, const pmml_input_struct_6 &s) {
}
static void to_json(json &j, const pmml_input_struct_1 &s) {
j["V6"]=s.V6;
j["V106"]=r_ec_V106_to_string(s.V106);
j["V140"]=r_ec_V140_to_string(s.V140);
j["V60"]=r_ec_V60_to_string(s.V60);
j["V20"]=s.V20;
j["V42"]=r_ec_V42_to_string(s.V42);
j["V12"]=r_ec_V12_to_string(s.V12);
j["V23"]=r_ec_V23_to_string(s.V23);
j["V47"]=s.V47;
j["V104"]=s.V104;
j["V38"]=r_ec_V38_to_string(s.V38);
j["V101"]=s.V101;
j["V189"]=r_ec_V189_to_string(s.V189);
j["V28"]=r_ec_V28_to_string(s.V28);
}
static void from_json(const json &j, pmml_input_struct_4 &s) {
}
static void from_json(const json &j, pmml_input_struct_5 &s) {
}
static void from_json(const json &j, pmml_input_struct_2 &s) {
}
static void from_json(const json &j, pmml_input_struct_3 &s) {
}
static void from_json(const json &j, pmml_input_struct_6 &s) {
}
static void from_json(const json &j, pmml_input_struct_1 &s) {

    if(!j.at("V6").is_null()) {
s.V6=j.at("V6").get<double>();
    }

    if(!j.at("V106").is_null()) {
s.V106=r_ec_V106_from_string(j.at("V106").get<std::string>());
    }

    if(!j.at("V140").is_null()) {
s.V140=r_ec_V140_from_string(j.at("V140").get<std::string>());
    }

    if(!j.at("V60").is_null()) {
s.V60=r_ec_V60_from_string(j.at("V60").get<std::string>());
    }

    if(!j.at("V20").is_null()) {
s.V20=j.at("V20").get<double>();
    }

    if(!j.at("V42").is_null()) {
s.V42=r_ec_V42_from_string(j.at("V42").get<std::string>());
    }

    if(!j.at("V12").is_null()) {
s.V12=r_ec_V12_from_string(j.at("V12").get<std::string>());
    }

    if(!j.at("V23").is_null()) {
s.V23=r_ec_V23_from_string(j.at("V23").get<std::string>());
    }

    if(!j.at("V47").is_null()) {
s.V47=j.at("V47").get<double>();
    }

    if(!j.at("V104").is_null()) {
s.V104=j.at("V104").get<double>();
    }

    if(!j.at("V38").is_null()) {
s.V38=r_ec_V38_from_string(j.at("V38").get<std::string>());
    }

    if(!j.at("V101").is_null()) {
s.V101=j.at("V101").get<double>();
    }

    if(!j.at("V189").is_null()) {
s.V189=r_ec_V189_from_string(j.at("V189").get<std::string>());
    }

    if(!j.at("V28").is_null()) {
s.V28=r_ec_V28_from_string(j.at("V28").get<std::string>());
    }
}
static std::string toString(const pmml_output_struct &o){
return "TARGET:"+std::to_string(o.TARGET);
}
static void to_json(json &j, const pmml_output_struct &o){
j["TARGET"]=o.TARGET;
j["Node1"]=o.Node1;
j["PROB_0"]=o.PROB_0;
j["PROB_1"]=o.PROB_1;
j["Response2"]=o.Response2;
j["Response1"]=o.Response1;
j["Node4"]=o.Node4;
j["Node3"]=o.Node3;
j["Response4"]=o.Response4;
j["Node2"]=o.Node2;
j["Response3"]=o.Response3;
j["PREDICTION"]=o.PREDICTION;
}
static void  MS_1(pmml_input_struct_1 & inp_c_1,  pmml_output_struct & out){
if(inp_c_1.V6==raven::missing) inp_c_1.V6= static_cast<double>(5651.000000);

if(inp_c_1.V106==raven::missing) inp_c_1.V106=r_ec_V106::r_00;

if(inp_c_1.V20==raven::missing) inp_c_1.V20= static_cast<double>(2256350.000000);

if(inp_c_1.V42==raven::missing) inp_c_1.V42=r_ec_V42::r_Y;

if(inp_c_1.V12==raven::missing) inp_c_1.V12=r_ec_V12::r_00;

if(inp_c_1.V23==raven::missing) inp_c_1.V23=r_ec_V23::r_Z;

if(inp_c_1.V47==raven::missing) inp_c_1.V47= static_cast<double>(2007.000000);

if(inp_c_1.V104==raven::missing) inp_c_1.V104= static_cast<double>(1033.000000);

if(inp_c_1.V38==raven::missing) inp_c_1.V38=r_ec_V38::r_EMPTY;

if(inp_c_1.V101==raven::missing) inp_c_1.V101= static_cast<double>(77676.000000);

if(inp_c_1.V189==raven::missing) inp_c_1.V189=r_ec_V189::r_N;

if(inp_c_1.V28==raven::missing) inp_c_1.V28=r_ec_V28::r_M;

}
static void  MS_2(pmml_input_struct_1 & inp_c_1, pmml_input_struct_2 & inp_c_2,  pmml_output_struct & out){

}
static void treeNode_Tree1_lvl_2(pmml_input_struct_1 & inp_c_1,pmml_input_struct_2 & inp_c_2, pmml_output_struct & out){
double score = NAN; int64_t entityId = std::numeric_limits<int>::min();
do{
if((((const uint8_t[]){0x43,0x60}[getUnderlyingValue(inp_c_1.V38)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V38)%8)))!=0)){
if(((inp_c_1.V38== raven::missing)||(((const uint8_t[]){0x7e,0xe0}[getUnderlyingValue(inp_c_1.V38)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V38)%8)))!=0))){
score =-3.596571;entityId =1;break;
}
else if((true)){
if(((inp_c_1.V12== raven::missing)||(((const uint8_t[]){0x7f,0xfd,0x7f,0xff}[getUnderlyingValue(inp_c_1.V12)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V12)%8)))!=0))){
score =-3.493799;entityId =2;break;
}
else if((true)){
score =-3.493799;entityId =3;break;
}
score =-3.492877;entityId =-3;}
score =-3.594113;entityId =-2;}
else if((true)){
if((inp_c_1.V104< static_cast<double>(103721.5))){
if((inp_c_1.V6< static_cast<double>(5733))){
score =-3.493799;entityId =4;break;
}
else if((true)){
score =-3.493799;entityId =5;break;
}
score =-3.355310;entityId =-5;}
else if((true)){
score =-3.493799;entityId =6;break;
}
score =-3.188530;entityId =-4;}
score =-3.593799;entityId =-1;
}while(0);
out.Response1 = score;
out.Node1= entityId;

}
static void treeModel_Tree1_lvl_2(pmml_input_struct_1 & inp_c_1, pmml_output_struct & out){
pmml_input_struct_2 inp_c_2;
MS_2(inp_c_1, inp_c_2,  out);
treeNode_Tree1_lvl_2(inp_c_1, inp_c_2,  out);

}
static void  MS_3(pmml_input_struct_1 & inp_c_1, pmml_input_struct_3 & inp_c_3,  pmml_output_struct & out){

}
static void treeNode_Tree2_lvl_3(pmml_input_struct_1 & inp_c_1,pmml_input_struct_3 & inp_c_3, pmml_output_struct & out){
double score = NAN; int64_t entityId = std::numeric_limits<int>::min();
do{
if(((inp_c_1.V38== raven::missing)||(((const uint8_t[]){0x63,0x60}[getUnderlyingValue(inp_c_1.V38)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V38)%8)))!=0))){
if(((inp_c_1.V38== raven::missing)||(((const uint8_t[]){0x5e,0xe0}[getUnderlyingValue(inp_c_1.V38)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V38)%8)))!=0))){
score =-0.002979;entityId =1;break;
}
else if((true)){
if(((inp_c_1.V106== raven::missing)||(((const uint8_t[]){0x7f,0xff,0xef,0xff,0xff,0x80}[getUnderlyingValue(inp_c_1.V106)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V106)%8)))!=0))){
score =0.100000;entityId =2;break;
}
else if((true)){
score =0.100000;entityId =3;break;
}
score =0.103936;entityId =-3;}
score =-0.000334;entityId =-2;}
else if((true)){
if((inp_c_1.V101< static_cast<double>(24468))){
if((inp_c_1.V189==r_ec_V189::r_Y)){
score =-0.005005;entityId =4;break;
}
else if((true)){
score =0.100000;entityId =5;break;
}
score =1.198155;entityId =-5;}
else if((true)){
score =0.100000;entityId =6;break;
}
score =0.554856;entityId =-4;}
score =0.000019;entityId =-1;
}while(0);
out.Response2 = score;
out.Node2= entityId;

}
static void treeModel_Tree2_lvl_3(pmml_input_struct_1 & inp_c_1, pmml_output_struct & out){
pmml_input_struct_3 inp_c_3;
MS_3(inp_c_1, inp_c_3,  out);
treeNode_Tree2_lvl_3(inp_c_1, inp_c_3,  out);

}
static void  MS_4(pmml_input_struct_1 & inp_c_1, pmml_input_struct_4 & inp_c_4,  pmml_output_struct & out){

}
static void treeNode_Tree3_lvl_4(pmml_input_struct_1 & inp_c_1,pmml_input_struct_4 & inp_c_4, pmml_output_struct & out){
double score = NAN; int64_t entityId = std::numeric_limits<int>::min();
do{
if(((inp_c_1.V38== raven::missing)||(((const uint8_t[]){0x6b,0xe0}[getUnderlyingValue(inp_c_1.V38)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V38)%8)))!=0))){
if(((inp_c_1.V38== raven::missing)||(((const uint8_t[]){0x5e,0x60}[getUnderlyingValue(inp_c_1.V38)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V38)%8)))!=0))){
score =-0.002967;entityId =1;break;
}
else if((true)){
if((inp_c_1.V20< static_cast<double>(363334))){
if(((inp_c_1.V106== raven::missing)||(((const uint8_t[]){0x7f,0xff,0xed,0xfb,0xff,0x80}[getUnderlyingValue(inp_c_1.V106)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V106)%8)))!=0))){
score =0.100000;entityId =2;break;
}
else if((true)){
score =0.100000;entityId =3;break;
}
score =0.488975;entityId =-4;}
else if((true)){
score =0.100000;entityId =4;break;
}
score =0.131406;entityId =-3;}
score =-0.000170;entityId =-2;}
else if((true)){
if((((const uint8_t[]){0x4,0x80}[getUnderlyingValue(inp_c_1.V140)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V140)%8)))!=0)){
score =0.100000;entityId =5;break;
}
else if((true)){
score =0.100000;entityId =6;break;
}
score =0.712033;entityId =-5;}
score =0.000092;entityId =-1;
}while(0);
out.Response3 = score;
out.Node3= entityId;

}
static void treeModel_Tree3_lvl_4(pmml_input_struct_1 & inp_c_1, pmml_output_struct & out){
pmml_input_struct_4 inp_c_4;
MS_4(inp_c_1, inp_c_4,  out);
treeNode_Tree3_lvl_4(inp_c_1, inp_c_4,  out);

}
static void  MS_5(pmml_input_struct_1 & inp_c_1, pmml_input_struct_5 & inp_c_5,  pmml_output_struct & out){

}
static void treeNode_Tree4_lvl_5(pmml_input_struct_1 & inp_c_1,pmml_input_struct_5 & inp_c_5, pmml_output_struct & out){
double score = NAN; int64_t entityId = std::numeric_limits<int>::min();
do{
if((((const uint8_t[]){0x42,0x60}[getUnderlyingValue(inp_c_1.V38)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V38)%8)))!=0)){
score =-0.005004;entityId =1;break;
}
else if((true)){
if(((inp_c_1.V12== raven::missing)||(((const uint8_t[]){0x7f,0xfd,0xff,0xdf}[getUnderlyingValue(inp_c_1.V12)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V12)%8)))!=0))){
if((inp_c_1.V20< static_cast<double>(339920))){
if(((inp_c_1.V106== raven::missing)||(((const uint8_t[]){0x7d,0xbf,0xef,0xfb,0xf7,0x80}[getUnderlyingValue(inp_c_1.V106)/8]&(0x80>>(getUnderlyingValue(inp_c_1.V106)%8)))!=0))){
if((inp_c_1.V47< static_cast<double>(2355))){
score =0.100000;entityId =2;break;
}
else if((true)){
score =0.100000;entityId =3;break;
}
score =0.265290;entityId =-5;}
else if((true)){
score =0.100000;entityId =4;break;
}
score =0.423180;entityId =-4;}
else if((true)){
score =0.100000;entityId =5;break;
}
score =0.108237;entityId =-3;}
else if((true)){
score =0.100000;entityId =6;break;
}
score =0.133619;entityId =-2;}
score =-0.000703;entityId =-1;
}while(0);
out.Response4 = score;
out.Node4= entityId;

}
static void treeModel_Tree4_lvl_5(pmml_input_struct_1 & inp_c_1, pmml_output_struct & out){
pmml_input_struct_5 inp_c_5;
MS_5(inp_c_1, inp_c_5,  out);
treeNode_Tree4_lvl_5(inp_c_1, inp_c_5,  out);

}
static void  MS_6(pmml_input_struct_1 & inp_c_1, pmml_input_struct_6 & inp_c_6,  pmml_output_struct & out){

}
static void regressionEquation_Final_lvl_6(pmml_input_struct_1 & inp_c_1,pmml_input_struct_6 & inp_c_6, pmml_output_struct & out){
out.PROB_1 = (static_cast<double>(1.0)/(1+std::exp(-((0.0 + (out.Response1*2.0+out.Response2*2.0+out.Response3*2.0+out.Response4*2.0))))));

}
static void regressionModel_Final_lvl_6(pmml_input_struct_1 & inp_c_1,  pmml_output_struct & out){
pmml_input_struct_6 inp_c_6;
MS_6(inp_c_1, inp_c_6,  out);
regressionEquation_Final_lvl_6(inp_c_1, inp_c_6,  out);

}
static void Segmentation_2(pmml_input_struct_1 & inp_c_1,  pmml_output_struct & out){
if(true){
treeModel_Tree1_lvl_2( inp_c_1,  out);
}
if(true){
treeModel_Tree2_lvl_3( inp_c_1,  out);
}
if(true){
treeModel_Tree3_lvl_4( inp_c_1,  out);
}
if(true){
treeModel_Tree4_lvl_5( inp_c_1,  out);
}
if(true){
regressionModel_Final_lvl_6( inp_c_1,  out);
}

}
static void miningModel_TreeNet_1_lvl_1(pmml_input_struct_1 & inp_c_1, pmml_output_struct & out){
MS_1(inp_c_1,  out);
Segmentation_2(inp_c_1,  out);
out.TARGET=out.PROB_1<=0.5?1:0;

}
std::string model_exec_json_str_fraud_model_V1(std::string & input){
auto p_json = json::parse(input);
pmml_input_struct_1 inp_c_1 = p_json;
pmml_output_struct out;
auto start_time = std::chrono::system_clock::now();
try {
miningModel_TreeNet_1_lvl_1( inp_c_1,  out);

} catch (const std::exception &e) {
std::cerr << e.what() << std::endl;
}
auto end_time = std::chrono::system_clock::now();
auto time_us = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
return toString(out);
}

nlohmann::json model_exec_json_fraud_model_V1(nlohmann::json & p_json){
pmml_input_struct_1 inp_c_1 = p_json;
pmml_output_struct out;
auto start_time = std::chrono::system_clock::now();
try {
miningModel_TreeNet_1_lvl_1( inp_c_1,  out);

} catch (const std::exception &e) {
std::cerr << e.what() << std::endl;
}
auto end_time = std::chrono::system_clock::now();
auto time_us = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
return out;
}

static json score_model(const json &input, std::function<void(json &)> &traceFunction){
pmml_input_struct_1 inp_c_1 = input;
pmml_output_struct out;
auto start_time = std::chrono::system_clock::now();
try {
miningModel_TreeNet_1_lvl_1( inp_c_1,  out);

if (UNLIKELY(static_cast<bool>(traceFunction))) {
json intermediateScore = json::object();
json inputPostMiningSchema = inp_c_1;
intermediateScore["pmml_input_struct_4"]["Response3"]=out.Response3;
intermediateScore["pmml_input_struct_4"]["Node3"]=out.Node3;
intermediateScore["pmml_input_struct_5"]["Response4"]=out.Response4;
intermediateScore["pmml_input_struct_5"]["Node4"]=out.Node4;
intermediateScore["pmml_input_struct_2"]["Response1"]=out.Response1;
intermediateScore["pmml_input_struct_2"]["Node1"]=out.Node1;
intermediateScore["pmml_input_struct_3"]["Response2"]=out.Response2;
intermediateScore["pmml_input_struct_3"]["Node2"]=out.Node2;
intermediateScore["pmml_input_struct_6"]["PROB_1"]=out.PROB_1;
json debugInfo = {{"intermediateScore", intermediateScore},{"input", input}, {"inputPostMiningSchema", inputPostMiningSchema}};
traceFunction(debugInfo);
}

} catch (const std::exception &e) {
std::cerr << e.what() << std::endl;
}
auto end_time = std::chrono::system_clock::now();
auto time_us = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
VLOG(2)<<"Time taken for model execution = "<<time_us<<" us";
return out;
}
static raven::base::ModelInfo *getModelInfo() {
static raven::base::ModelInfo modelInfo = {
"fraud_model",
"TreeNet_1",
1,
{"V6","V106","V140","V60","V20","V42","V12","V23","V47","V104","V38","V101","V189","V28"},
{"TARGET","Node1","PROB_0","PROB_1","Response2","Response1","Node4","Node3","Response4","Node2","Response3","PREDICTION"},
score_model,
false};
return &modelInfo;
}

}
#pragma GCC diagnostic pop










#include "raven-engine/base/Artifact.h"





static bool model_fraud_model_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
      raven::base::Model::AddModel(raven::generated::getModelInfo());
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
      raven::base::Model::RemoveModel(raven::generated::getModelInfo());
    }
    return true;
}

extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_model_fraud_model_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_model_fraud_model_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Model",     std::vector({    std::string("fraud_model"),
})
, 1, model_fraud_model_V1_StatusChangeCallBack, "model_fraud_model_V1");
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void model_fraud_model_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_model_fraud_model_V1());
     if(!artifact) return;
     model_fraud_model_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     model_fraud_model_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void model_fraud_model_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_model_fraud_model_V1());
    if(!artifact) return;
     model_fraud_model_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     model_fraud_model_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
