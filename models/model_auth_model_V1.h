#pragma once
#include <string>
#include <nlohmann/json.hpp>
namespace raven::generated {
std::string model_exec_json_str_auth_model_V1(std::string & input);
nlohmann::json model_exec_json_auth_model_V1(nlohmann::json & p_json);
}