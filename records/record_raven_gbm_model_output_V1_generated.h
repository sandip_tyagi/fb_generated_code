// automatically generated by the FlatBuffers compiler, do not modify


#ifndef FLATBUFFERS_GENERATED_RECORDRAVENGBMMODELOUTPUTV1_RAVEN_GENERATED_H_
#define FLATBUFFERS_GENERATED_RECORDRAVENGBMMODELOUTPUTV1_RAVEN_GENERATED_H_

#include "flatbuffers/flatbuffers.h"

#include "RavenTypes_generated.h"
#include "RecordHeader_generated.h"

namespace raven {
namespace generated {

struct record_raven_gbm_model_output_V1;
struct RG_record_raven_gbm_model_output_V1;

bool operator==(const RG_record_raven_gbm_model_output_V1 &lhs, const RG_record_raven_gbm_model_output_V1 &rhs);
bool operator!=(const RG_record_raven_gbm_model_output_V1 &lhs, const RG_record_raven_gbm_model_output_V1 &rhs);

inline const flatbuffers::TypeTable *record_raven_gbm_model_output_V1TypeTable();

struct RG_record_raven_gbm_model_output_V1 : public flatbuffers::NativeTable {
  typedef record_raven_gbm_model_output_V1 TableType;
  static FLATBUFFERS_CONSTEXPR const char *GetFullyQualifiedName() {
    return "raven.generated.RG_record_raven_gbm_model_output_V1";
  }
  std::unique_ptr<raven::base::fbs::RecordHeader> raven_header;
  double score;
  RG_record_raven_gbm_model_output_V1()
      : score(0.0) {
  }
};

inline bool operator==(const RG_record_raven_gbm_model_output_V1 &lhs, const RG_record_raven_gbm_model_output_V1 &rhs) {
  return
      (lhs.raven_header == rhs.raven_header) &&
      (lhs.score == rhs.score);
}

inline bool operator!=(const RG_record_raven_gbm_model_output_V1 &lhs, const RG_record_raven_gbm_model_output_V1 &rhs) {
    return !(lhs == rhs);
}


struct record_raven_gbm_model_output_V1 FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef RG_record_raven_gbm_model_output_V1 NativeTableType;
  static const flatbuffers::TypeTable *MiniReflectTypeTable() {
    return record_raven_gbm_model_output_V1TypeTable();
  }
  static FLATBUFFERS_CONSTEXPR const char *GetFullyQualifiedName() {
    return "raven.generated.record_raven_gbm_model_output_V1";
  }
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_RAVEN_HEADER = 4,
    VT_SCORE = 6
  };
  const raven::base::fbs::RecordHeader *raven_header() const {
    return GetStruct<const raven::base::fbs::RecordHeader *>(VT_RAVEN_HEADER);
  }
  raven::base::fbs::RecordHeader *mutable_raven_header() {
    return GetStruct<raven::base::fbs::RecordHeader *>(VT_RAVEN_HEADER);
  }
  double score() const {
    return GetField<double>(VT_SCORE, 0.0);
  }
  bool mutate_score(double _score) {
    return SetField<double>(VT_SCORE, _score, 0.0);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyField<raven::base::fbs::RecordHeader>(verifier, VT_RAVEN_HEADER) &&
           VerifyField<double>(verifier, VT_SCORE) &&
           verifier.EndTable();
  }
  RG_record_raven_gbm_model_output_V1 *UnPack(const flatbuffers::resolver_function_t *_resolver = nullptr) const;
  void UnPackTo(RG_record_raven_gbm_model_output_V1 *_o, const flatbuffers::resolver_function_t *_resolver = nullptr) const;
  static flatbuffers::Offset<record_raven_gbm_model_output_V1> Pack(flatbuffers::FlatBufferBuilder &_fbb, const RG_record_raven_gbm_model_output_V1* _o, const flatbuffers::rehasher_function_t *_rehasher = nullptr);
};

struct record_raven_gbm_model_output_V1Builder {
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_raven_header(const raven::base::fbs::RecordHeader *raven_header) {
    fbb_.AddStruct(record_raven_gbm_model_output_V1::VT_RAVEN_HEADER, raven_header);
  }
  void add_score(double score) {
    fbb_.AddElement<double>(record_raven_gbm_model_output_V1::VT_SCORE, score, 0.0);
  }
  explicit record_raven_gbm_model_output_V1Builder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  record_raven_gbm_model_output_V1Builder &operator=(const record_raven_gbm_model_output_V1Builder &);
  flatbuffers::Offset<record_raven_gbm_model_output_V1> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<record_raven_gbm_model_output_V1>(end);
    return o;
  }
};

inline flatbuffers::Offset<record_raven_gbm_model_output_V1> Createrecord_raven_gbm_model_output_V1(
    flatbuffers::FlatBufferBuilder &_fbb,
    const raven::base::fbs::RecordHeader *raven_header = 0,
    double score = 0.0) {
  record_raven_gbm_model_output_V1Builder builder_(_fbb);
  builder_.add_score(score);
  builder_.add_raven_header(raven_header);
  return builder_.Finish();
}

flatbuffers::Offset<record_raven_gbm_model_output_V1> Createrecord_raven_gbm_model_output_V1(flatbuffers::FlatBufferBuilder &_fbb, const RG_record_raven_gbm_model_output_V1 *_o, const flatbuffers::rehasher_function_t *_rehasher = nullptr);

inline RG_record_raven_gbm_model_output_V1 *record_raven_gbm_model_output_V1::UnPack(const flatbuffers::resolver_function_t *_resolver) const {
  auto _o = new RG_record_raven_gbm_model_output_V1();
  UnPackTo(_o, _resolver);
  return _o;
}

inline void record_raven_gbm_model_output_V1::UnPackTo(RG_record_raven_gbm_model_output_V1 *_o, const flatbuffers::resolver_function_t *_resolver) const {
  (void)_o;
  (void)_resolver;
  { auto _e = raven_header(); if (_e) _o->raven_header = std::unique_ptr<raven::base::fbs::RecordHeader>(new raven::base::fbs::RecordHeader(*_e)); };
  { auto _e = score(); _o->score = _e; };
}

inline flatbuffers::Offset<record_raven_gbm_model_output_V1> record_raven_gbm_model_output_V1::Pack(flatbuffers::FlatBufferBuilder &_fbb, const RG_record_raven_gbm_model_output_V1* _o, const flatbuffers::rehasher_function_t *_rehasher) {
  return Createrecord_raven_gbm_model_output_V1(_fbb, _o, _rehasher);
}

inline flatbuffers::Offset<record_raven_gbm_model_output_V1> Createrecord_raven_gbm_model_output_V1(flatbuffers::FlatBufferBuilder &_fbb, const RG_record_raven_gbm_model_output_V1 *_o, const flatbuffers::rehasher_function_t *_rehasher) {
  (void)_rehasher;
  (void)_o;
  struct _VectorArgs { flatbuffers::FlatBufferBuilder *__fbb; const RG_record_raven_gbm_model_output_V1* __o; const flatbuffers::rehasher_function_t *__rehasher; } _va = { &_fbb, _o, _rehasher}; (void)_va;
  auto _raven_header = _o->raven_header ? _o->raven_header.get() : 0;
  auto _score = _o->score;
  return raven::generated::Createrecord_raven_gbm_model_output_V1(
      _fbb,
      _raven_header,
      _score);
}

inline const flatbuffers::TypeTable *record_raven_gbm_model_output_V1TypeTable() {
  static const flatbuffers::TypeCode type_codes[] = {
    { flatbuffers::ET_SEQUENCE, 0, 0 },
    { flatbuffers::ET_DOUBLE, 0, -1 }
  };
  static const flatbuffers::TypeFunction type_refs[] = {
    raven::base::fbs::RecordHeaderTypeTable
  };
  static const char * const names[] = {
    "raven_header",
    "score"
  };
  static const flatbuffers::TypeTable tt = {
    flatbuffers::ST_TABLE, 2, type_codes, type_refs, nullptr, names
  };
  return &tt;
}

inline const raven::generated::record_raven_gbm_model_output_V1 *Getrecord_raven_gbm_model_output_V1(const void *buf) {
  return flatbuffers::GetRoot<raven::generated::record_raven_gbm_model_output_V1>(buf);
}

inline const raven::generated::record_raven_gbm_model_output_V1 *GetSizePrefixedrecord_raven_gbm_model_output_V1(const void *buf) {
  return flatbuffers::GetSizePrefixedRoot<raven::generated::record_raven_gbm_model_output_V1>(buf);
}

inline record_raven_gbm_model_output_V1 *GetMutablerecord_raven_gbm_model_output_V1(void *buf) {
  return flatbuffers::GetMutableRoot<record_raven_gbm_model_output_V1>(buf);
}

inline bool Verifyrecord_raven_gbm_model_output_V1Buffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifyBuffer<raven::generated::record_raven_gbm_model_output_V1>(nullptr);
}

inline bool VerifySizePrefixedrecord_raven_gbm_model_output_V1Buffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifySizePrefixedBuffer<raven::generated::record_raven_gbm_model_output_V1>(nullptr);
}

inline void Finishrecord_raven_gbm_model_output_V1Buffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<raven::generated::record_raven_gbm_model_output_V1> root) {
  fbb.Finish(root);
}

inline void FinishSizePrefixedrecord_raven_gbm_model_output_V1Buffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<raven::generated::record_raven_gbm_model_output_V1> root) {
  fbb.FinishSizePrefixed(root);
}

inline std::unique_ptr<RG_record_raven_gbm_model_output_V1> UnPackrecord_raven_gbm_model_output_V1(
    const void *buf,
    const flatbuffers::resolver_function_t *res = nullptr) {
  return std::unique_ptr<RG_record_raven_gbm_model_output_V1>(Getrecord_raven_gbm_model_output_V1(buf)->UnPack(res));
}

}  // namespace generated
}  // namespace raven

#endif  // FLATBUFFERS_GENERATED_RECORDRAVENGBMMODELOUTPUTV1_RAVEN_GENERATED_H_
