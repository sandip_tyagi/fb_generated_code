#pragma once
#include"raven-engine/base/Record.h"
#include "lib/include/rstring.h"
#include "lib/include/custom_date.h"
#include "raven-engine/fbs/FlatBuffer.h"
#include "record_txn_auth_input_V1_generated.h"

namespace raven::generated{
using namespace raven::types;

const char*getRecordName_txn_auth_input_V1();
void init_record_txn_auth_input_V1(RG_record_txn_auth_input_V1 *r);
flatbuffers::uoffset_t new_record_txn_auth_input_V1(flatbuffers::FlatBufferBuilder &fbb);
const raven::base::RecordInfo* getRecordInfo_record_txn_auth_input_V1();
std::pair<const flatbuffers::Table *, size_t > getDefault_record_txn_auth_input_V1();
}
