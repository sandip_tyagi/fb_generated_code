// automatically generated by the FlatBuffers compiler, do not modify


#ifndef FLATBUFFERS_GENERATED_RECORDHEADER_RAVEN_BASE_FBS_H_
#define FLATBUFFERS_GENERATED_RECORDHEADER_RAVEN_BASE_FBS_H_

#include "flatbuffers/flatbuffers.h"

#include "RavenTypes_generated.h"

namespace raven {
namespace base {
namespace fbs {

struct RecordHeader;

struct GenericRecordHeaderOverlay;
struct RG_GenericRecordHeaderOverlay;

bool operator==(const RecordHeader &lhs, const RecordHeader &rhs);
bool operator!=(const RecordHeader &lhs, const RecordHeader &rhs);
bool operator==(const RG_GenericRecordHeaderOverlay &lhs, const RG_GenericRecordHeaderOverlay &rhs);
bool operator!=(const RG_GenericRecordHeaderOverlay &lhs, const RG_GenericRecordHeaderOverlay &rhs);

inline const flatbuffers::TypeTable *RecordHeaderTypeTable();

inline const flatbuffers::TypeTable *GenericRecordHeaderOverlayTypeTable();

FLATBUFFERS_MANUALLY_ALIGNED_STRUCT(8) RecordHeader FLATBUFFERS_FINAL_CLASS {
 private:
  uint16_t version_;
  uint16_t flags_;
  uint16_t schema_;
  int8_t update_count_;
  int8_t padding_;
  int64_t timestamp_update_;

 public:
  static const flatbuffers::TypeTable *MiniReflectTypeTable() {
    return RecordHeaderTypeTable();
  }
  static FLATBUFFERS_CONSTEXPR const char *GetFullyQualifiedName() {
    return "raven.base.fbs.RecordHeader";
  }
  RecordHeader() {
    memset(static_cast<void *>(this), 0, sizeof(RecordHeader));
  }
  RecordHeader(uint16_t _version, uint16_t _flags, uint16_t _schema, int8_t _update_count, int8_t _padding, int64_t _timestamp_update)
      : version_(flatbuffers::EndianScalar(_version)),
        flags_(flatbuffers::EndianScalar(_flags)),
        schema_(flatbuffers::EndianScalar(_schema)),
        update_count_(flatbuffers::EndianScalar(_update_count)),
        padding_(flatbuffers::EndianScalar(_padding)),
        timestamp_update_(flatbuffers::EndianScalar(_timestamp_update)) {
  }
  uint16_t version() const {
    return flatbuffers::EndianScalar(version_);
  }
  void mutate_version(uint16_t _version) {
    flatbuffers::WriteScalar(&version_, _version);
  }
  uint16_t flags() const {
    return flatbuffers::EndianScalar(flags_);
  }
  void mutate_flags(uint16_t _flags) {
    flatbuffers::WriteScalar(&flags_, _flags);
  }
  uint16_t schema() const {
    return flatbuffers::EndianScalar(schema_);
  }
  void mutate_schema(uint16_t _schema) {
    flatbuffers::WriteScalar(&schema_, _schema);
  }
  int8_t update_count() const {
    return flatbuffers::EndianScalar(update_count_);
  }
  void mutate_update_count(int8_t _update_count) {
    flatbuffers::WriteScalar(&update_count_, _update_count);
  }
  int8_t padding() const {
    return flatbuffers::EndianScalar(padding_);
  }
  void mutate_padding(int8_t _padding) {
    flatbuffers::WriteScalar(&padding_, _padding);
  }
  int64_t timestamp_update() const {
    return flatbuffers::EndianScalar(timestamp_update_);
  }
  void mutate_timestamp_update(int64_t _timestamp_update) {
    flatbuffers::WriteScalar(&timestamp_update_, _timestamp_update);
  }
};
FLATBUFFERS_STRUCT_END(RecordHeader, 16);

inline bool operator==(const RecordHeader &lhs, const RecordHeader &rhs) {
  return
      (lhs.version() == rhs.version()) &&
      (lhs.flags() == rhs.flags()) &&
      (lhs.schema() == rhs.schema()) &&
      (lhs.update_count() == rhs.update_count()) &&
      (lhs.padding() == rhs.padding()) &&
      (lhs.timestamp_update() == rhs.timestamp_update());
}

inline bool operator!=(const RecordHeader &lhs, const RecordHeader &rhs) {
    return !(lhs == rhs);
}


struct RG_GenericRecordHeaderOverlay : public flatbuffers::NativeTable {
  typedef GenericRecordHeaderOverlay TableType;
  static FLATBUFFERS_CONSTEXPR const char *GetFullyQualifiedName() {
    return "raven.base.fbs.RG_GenericRecordHeaderOverlay";
  }
  std::unique_ptr<RecordHeader> raven_header;
  RG_GenericRecordHeaderOverlay() {
  }
};

inline bool operator==(const RG_GenericRecordHeaderOverlay &lhs, const RG_GenericRecordHeaderOverlay &rhs) {
  return
      (lhs.raven_header == rhs.raven_header);
}

inline bool operator!=(const RG_GenericRecordHeaderOverlay &lhs, const RG_GenericRecordHeaderOverlay &rhs) {
    return !(lhs == rhs);
}


struct GenericRecordHeaderOverlay FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef RG_GenericRecordHeaderOverlay NativeTableType;
  static const flatbuffers::TypeTable *MiniReflectTypeTable() {
    return GenericRecordHeaderOverlayTypeTable();
  }
  static FLATBUFFERS_CONSTEXPR const char *GetFullyQualifiedName() {
    return "raven.base.fbs.GenericRecordHeaderOverlay";
  }
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_RAVEN_HEADER = 4
  };
  const RecordHeader *raven_header() const {
    return GetStruct<const RecordHeader *>(VT_RAVEN_HEADER);
  }
  RecordHeader *mutable_raven_header() {
    return GetStruct<RecordHeader *>(VT_RAVEN_HEADER);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyFieldRequired<RecordHeader>(verifier, VT_RAVEN_HEADER) &&
           verifier.EndTable();
  }
  RG_GenericRecordHeaderOverlay *UnPack(const flatbuffers::resolver_function_t *_resolver = nullptr) const;
  void UnPackTo(RG_GenericRecordHeaderOverlay *_o, const flatbuffers::resolver_function_t *_resolver = nullptr) const;
  static flatbuffers::Offset<GenericRecordHeaderOverlay> Pack(flatbuffers::FlatBufferBuilder &_fbb, const RG_GenericRecordHeaderOverlay* _o, const flatbuffers::rehasher_function_t *_rehasher = nullptr);
};

struct GenericRecordHeaderOverlayBuilder {
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_raven_header(const RecordHeader *raven_header) {
    fbb_.AddStruct(GenericRecordHeaderOverlay::VT_RAVEN_HEADER, raven_header);
  }
  explicit GenericRecordHeaderOverlayBuilder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  GenericRecordHeaderOverlayBuilder &operator=(const GenericRecordHeaderOverlayBuilder &);
  flatbuffers::Offset<GenericRecordHeaderOverlay> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<GenericRecordHeaderOverlay>(end);
    fbb_.Required(o, GenericRecordHeaderOverlay::VT_RAVEN_HEADER);
    return o;
  }
};

inline flatbuffers::Offset<GenericRecordHeaderOverlay> CreateGenericRecordHeaderOverlay(
    flatbuffers::FlatBufferBuilder &_fbb,
    const RecordHeader *raven_header = 0) {
  GenericRecordHeaderOverlayBuilder builder_(_fbb);
  builder_.add_raven_header(raven_header);
  return builder_.Finish();
}

flatbuffers::Offset<GenericRecordHeaderOverlay> CreateGenericRecordHeaderOverlay(flatbuffers::FlatBufferBuilder &_fbb, const RG_GenericRecordHeaderOverlay *_o, const flatbuffers::rehasher_function_t *_rehasher = nullptr);

inline RG_GenericRecordHeaderOverlay *GenericRecordHeaderOverlay::UnPack(const flatbuffers::resolver_function_t *_resolver) const {
  auto _o = new RG_GenericRecordHeaderOverlay();
  UnPackTo(_o, _resolver);
  return _o;
}

inline void GenericRecordHeaderOverlay::UnPackTo(RG_GenericRecordHeaderOverlay *_o, const flatbuffers::resolver_function_t *_resolver) const {
  (void)_o;
  (void)_resolver;
  { auto _e = raven_header(); if (_e) _o->raven_header = std::unique_ptr<RecordHeader>(new RecordHeader(*_e)); };
}

inline flatbuffers::Offset<GenericRecordHeaderOverlay> GenericRecordHeaderOverlay::Pack(flatbuffers::FlatBufferBuilder &_fbb, const RG_GenericRecordHeaderOverlay* _o, const flatbuffers::rehasher_function_t *_rehasher) {
  return CreateGenericRecordHeaderOverlay(_fbb, _o, _rehasher);
}

inline flatbuffers::Offset<GenericRecordHeaderOverlay> CreateGenericRecordHeaderOverlay(flatbuffers::FlatBufferBuilder &_fbb, const RG_GenericRecordHeaderOverlay *_o, const flatbuffers::rehasher_function_t *_rehasher) {
  (void)_rehasher;
  (void)_o;
  struct _VectorArgs { flatbuffers::FlatBufferBuilder *__fbb; const RG_GenericRecordHeaderOverlay* __o; const flatbuffers::rehasher_function_t *__rehasher; } _va = { &_fbb, _o, _rehasher}; (void)_va;
  auto _raven_header = _o->raven_header ? _o->raven_header.get() : 0;
  return raven::base::fbs::CreateGenericRecordHeaderOverlay(
      _fbb,
      _raven_header);
}

inline const flatbuffers::TypeTable *RecordHeaderTypeTable() {
  static const flatbuffers::TypeCode type_codes[] = {
    { flatbuffers::ET_USHORT, 0, -1 },
    { flatbuffers::ET_USHORT, 0, -1 },
    { flatbuffers::ET_USHORT, 0, -1 },
    { flatbuffers::ET_CHAR, 0, -1 },
    { flatbuffers::ET_CHAR, 0, -1 },
    { flatbuffers::ET_LONG, 0, -1 }
  };
  static const int64_t values[] = { 0, 2, 4, 6, 7, 8, 16 };
  static const char * const names[] = {
    "version",
    "flags",
    "schema",
    "update_count",
    "padding",
    "timestamp_update"
  };
  static const flatbuffers::TypeTable tt = {
    flatbuffers::ST_STRUCT, 6, type_codes, nullptr, values, names
  };
  return &tt;
}

inline const flatbuffers::TypeTable *GenericRecordHeaderOverlayTypeTable() {
  static const flatbuffers::TypeCode type_codes[] = {
    { flatbuffers::ET_SEQUENCE, 0, 0 }
  };
  static const flatbuffers::TypeFunction type_refs[] = {
    RecordHeaderTypeTable
  };
  static const char * const names[] = {
    "raven_header"
  };
  static const flatbuffers::TypeTable tt = {
    flatbuffers::ST_TABLE, 1, type_codes, type_refs, nullptr, names
  };
  return &tt;
}

inline const raven::base::fbs::GenericRecordHeaderOverlay *GetGenericRecordHeaderOverlay(const void *buf) {
  return flatbuffers::GetRoot<raven::base::fbs::GenericRecordHeaderOverlay>(buf);
}

inline const raven::base::fbs::GenericRecordHeaderOverlay *GetSizePrefixedGenericRecordHeaderOverlay(const void *buf) {
  return flatbuffers::GetSizePrefixedRoot<raven::base::fbs::GenericRecordHeaderOverlay>(buf);
}

inline GenericRecordHeaderOverlay *GetMutableGenericRecordHeaderOverlay(void *buf) {
  return flatbuffers::GetMutableRoot<GenericRecordHeaderOverlay>(buf);
}

inline bool VerifyGenericRecordHeaderOverlayBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifyBuffer<raven::base::fbs::GenericRecordHeaderOverlay>(nullptr);
}

inline bool VerifySizePrefixedGenericRecordHeaderOverlayBuffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifySizePrefixedBuffer<raven::base::fbs::GenericRecordHeaderOverlay>(nullptr);
}

inline void FinishGenericRecordHeaderOverlayBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<raven::base::fbs::GenericRecordHeaderOverlay> root) {
  fbb.Finish(root);
}

inline void FinishSizePrefixedGenericRecordHeaderOverlayBuffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<raven::base::fbs::GenericRecordHeaderOverlay> root) {
  fbb.FinishSizePrefixed(root);
}

inline std::unique_ptr<RG_GenericRecordHeaderOverlay> UnPackGenericRecordHeaderOverlay(
    const void *buf,
    const flatbuffers::resolver_function_t *res = nullptr) {
  return std::unique_ptr<RG_GenericRecordHeaderOverlay>(GetGenericRecordHeaderOverlay(buf)->UnPack(res));
}

}  // namespace fbs
}  // namespace base
}  // namespace raven

#endif  // FLATBUFFERS_GENERATED_RECORDHEADER_RAVEN_BASE_FBS_H_
