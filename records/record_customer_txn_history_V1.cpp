








#include <lib/include/helper_functions.h>
#include <lib/include/missing.h>
#include "raven-engine/base/Artifact.h"
#include "record_customer_txn_history_V1.h"
#include <folly/GLog.h>

#include "raven-engine/fbs/RavenFlatbufferParser.h"
#include "record_customer_txn_history_V1_generated.h"
#include "raven-engine/schemaLoads/FlatbufferSchemaManager.h"
#include "raven-engine/schemaLoads/FBParserPool.h"
#include <chrono>

#ifndef STRINGIFY
#define STRINGIFY(x) #x
#define TOSTRING(x)STRINGIFY(x)
#endif







#include "raven-engine/base/Artifact.h"




namespace raven::generated{
    using json=std::string;
    using Record=raven::base::Record;
    using RecordInfo=raven::base::RecordInfo;

    using fieldType=std::string;
    using fieldName=std::string;

// prototypes for from and to JSONs begin

    json toJSON_record_customer_txn_history_V1(const flatbuffers::Table *buffer);
    flatbuffers::uoffset_t fromJSON_record_customer_txn_history_V1(flatbuffers::FlatBufferBuilder &fbb, const std::string_view &sv);


// prototypes for from and to JSONs end

// prototypes for upgrade and downgrade follow here




    // prototypes for upgrade and downgrade end here

    const std::vector<std::pair<fieldName, fieldType>>&fieldNames_record_customer_txn_history_V1();

    const char*getRecordName_customer_txn_history_V1(){
    return"customer_txn_history";
    }

void init_record_customer_txn_history_V1(RG_record_customer_txn_history_V1 *r)
    {
        if(!r->raven_header){
            auto now = std::chrono::system_clock::now();
            auto now_microSecond = std::chrono::time_point_cast<std::chrono::microseconds>(now);
            auto value = std::chrono::duration_cast<std::chrono::microseconds>(now_microSecond.time_since_epoch());
            r->raven_header = std::make_unique<raven::base::fbs::RecordHeader>(1,
                                                                                raven::missing,
                                                                                raven::missing,
                                                                                raven::missing,
                                                                                raven::missing,
                                                                                value.count());
        } else{
            r->raven_header->mutate_update_count(r->raven_header->update_count());
            auto now = std::chrono::system_clock::now();
            auto now_microSecond = std::chrono::time_point_cast<std::chrono::microseconds>(now);
            auto value = std::chrono::duration_cast<std::chrono::microseconds>(now_microSecond.time_since_epoch());
            r->raven_header->mutate_timestamp_update(value.count());
        }


            r->transaction_date =raven::missing;
            r->account_number =raven::missing;
            r->amount =raven::missing;
            r->trace_number =raven::missing;
            r->card_number =raven::missing;
            r->device_id =raven::missing;
            r->transaction_decision =raven::missing;
            r->customer_number =raven::missing;
            r->merchant_id =raven::missing;
            r->ip_address =raven::missing;
            r->transaction_type =raven::missing;
            r->fraud_score =raven::missing;
            r->currency_code =raven::missing;
            r->zip_code =raven::missing;
            r->auth_score =raven::missing;
            r->transaction_time =raven::missing;

    }

    flatbuffers::uoffset_t new_record_customer_txn_history_V1(flatbuffers::FlatBufferBuilder &fbb){
        RG_record_customer_txn_history_V1 * p = new RG_record_customer_txn_history_V1;
        Createrecord_customer_txn_history_V1(fbb,p);
        return fbb.GetSize();
    }



    json toJSON_record_customer_txn_history_V1(const flatbuffers::Table *buffer){
        // TODO: check buffer version with variable version: 1
        auto schemaPtr = raven::cluster::FlatbufferSchemaManager::GetSchema(1, "customer_txn_history");
        auto fbsfile = schemaPtr->getBinarySchema();
        auto schema = reflection::GetSchema(fbsfile.c_str());
        std::string Json_string = raven::util::FlatBuffer::ToJSON(reinterpret_cast<const uint8_t *>(buffer), schema);
        return Json_string;
    }



  flatbuffers::uoffset_t fromJSON_record_customer_txn_history_V1(flatbuffers::FlatBufferBuilder &fbb, const std::string_view &sv){
    auto fbPool = raven::cluster::FBParserPool::getInstance();
    auto parser = fbPool->getParserItem("customer_txn_history",1);
    bool status = parser.operator->().Parse(sv.data());
    if (status) {
        fbb.PushBytes(parser.operator*().builder_.GetBufferPointer(), parser.operator*().builder_.GetSize());
        return fbb.GetSize();
    }
    return 0;
  }

   std::pair<const flatbuffers::Table *, size_t > getDefault_record_customer_txn_history_V1(){
      static auto detachedBuffer = [](){
                                flatbuffers::FlatBufferBuilder fbb;
                                raven::generated::new_record_customer_txn_history_V1(fbb);
                                return fbb.Release();
                                }();
      return {reinterpret_cast<const flatbuffers::Table *>(detachedBuffer.data()),detachedBuffer.size()};
   }

   const std::vector<std::pair<fieldName, fieldType>>&fieldNames_record_customer_txn_history_V1(){
    RG_record_customer_txn_history_V1 *v1 =nullptr;
static std::vector<std::pair<fieldName, fieldType>>
    m={
    {"transaction_date",std::string(type_name<decltype(v1->transaction_date)>())},
    {"account_number",std::string(type_name<decltype(v1->account_number)>())},
    {"amount",std::string(type_name<decltype(v1->amount)>())},
    {"trace_number",std::string(type_name<decltype(v1->trace_number)>())},
    {"card_number",std::string(type_name<decltype(v1->card_number)>())},
    {"device_id",std::string(type_name<decltype(v1->device_id)>())},
    {"transaction_decision",std::string(type_name<decltype(v1->transaction_decision)>())},
    {"customer_number",std::string(type_name<decltype(v1->customer_number)>())},
    {"merchant_id",std::string(type_name<decltype(v1->merchant_id)>())},
    {"ip_address",std::string(type_name<decltype(v1->ip_address)>())},
    {"transaction_type",std::string(type_name<decltype(v1->transaction_type)>())},
    {"fraud_score",std::string(type_name<decltype(v1->fraud_score)>())},
    {"currency_code",std::string(type_name<decltype(v1->currency_code)>())},
    {"zip_code",std::string(type_name<decltype(v1->zip_code)>())},
    {"auth_score",std::string(type_name<decltype(v1->auth_score)>())},
    {"transaction_time",std::string(type_name<decltype(v1->transaction_time)>())},
    };
    return m;
    }







const raven::base::RecordInfo* getRecordInfo_record_customer_txn_history_V1(){
    using namespace raven::generated;
static raven::base::RecordInfo recordInfo =
    {{getRecordName_customer_txn_history_V1(), 1},  // Name of record and version
    sizeof(record_customer_txn_history_V1),  // Size of the record
    {
            // {0,upgrade_record_customer_txn_history_V1_from_V0}
            // {-1,upgrade_record_customer_txn_history_V1_from_V-1}
    }, // Upgraders
    {
            // {0, downgrade_record_customer_txn_history_V1_to_V0}
            // {-1, downgrade_record_customer_txn_history_V1_to_V-1}
    }, // Downgraders
    new_record_customer_txn_history_V1, // Allocate a default initialized record
    getDefault_record_customer_txn_history_V1,
    fromJSON_record_customer_txn_history_V1, // From JSON
    toJSON_record_customer_txn_history_V1, // To JSON
    nullptr,  // To CSV
    nullptr,  // From CSV
    fieldNames_record_customer_txn_history_V1(), // Field Name of the record
    false // active
    };
    return&recordInfo;
    }
}


static bool record_customer_txn_history_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
        const raven::base::RecordInfo*recordInfo=raven::generated::getRecordInfo_record_customer_txn_history_V1();
        raven::base::AddRecordInfo(recordInfo->recordKey.recordName,recordInfo->recordKey.version,recordInfo);
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
        raven::base::RecordInfo *recordInfo = const_cast<raven::base::RecordInfo*>(raven::generated::getRecordInfo_record_customer_txn_history_V1());
        recordInfo->active=true;
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
        raven::base::RecordInfo *recordInfo = const_cast<raven::base::RecordInfo*>(raven::generated::getRecordInfo_record_customer_txn_history_V1());
        recordInfo->active=false;
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
        const raven::base::RecordInfo *recordInfo = raven::generated::getRecordInfo_record_customer_txn_history_V1();
        raven::base::RemoveRecordInfo(recordInfo->recordKey.recordName, recordInfo->recordKey.version, recordInfo);
    }
    return true;
}


extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_record_customer_txn_history_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_record_customer_txn_history_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Record",     std::vector({    std::string("customer_txn_history"),
})
, 1, record_customer_txn_history_V1_StatusChangeCallBack, "record_customer_txn_history_V1");
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void record_customer_txn_history_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_record_customer_txn_history_V1());
     if(!artifact) return;
     record_customer_txn_history_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     record_customer_txn_history_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void record_customer_txn_history_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_record_customer_txn_history_V1());
    if(!artifact) return;
     record_customer_txn_history_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     record_customer_txn_history_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
