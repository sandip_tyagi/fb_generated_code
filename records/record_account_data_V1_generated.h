// automatically generated by the FlatBuffers compiler, do not modify


#ifndef FLATBUFFERS_GENERATED_RECORDACCOUNTDATAV1_RAVEN_GENERATED_H_
#define FLATBUFFERS_GENERATED_RECORDACCOUNTDATAV1_RAVEN_GENERATED_H_

#include "flatbuffers/flatbuffers.h"

#include "RavenTypes_generated.h"
#include "RecordHeader_generated.h"

namespace raven {
namespace generated {

struct record_account_data_V1;
struct RG_record_account_data_V1;

bool operator==(const RG_record_account_data_V1 &lhs, const RG_record_account_data_V1 &rhs);
bool operator!=(const RG_record_account_data_V1 &lhs, const RG_record_account_data_V1 &rhs);

inline const flatbuffers::TypeTable *record_account_data_V1TypeTable();

struct RG_record_account_data_V1 : public flatbuffers::NativeTable {
  typedef record_account_data_V1 TableType;
  static FLATBUFFERS_CONSTEXPR const char *GetFullyQualifiedName() {
    return "raven.generated.RG_record_account_data_V1";
  }
  std::unique_ptr<raven::base::fbs::RecordHeader> raven_header;
  std::string account_number;
  double past_due_days;
  bool past_due_ind;
  std::string customer_number;
  std::string billing_zip;
  std::string work_zip;
  std::string home_zip;
  RG_record_account_data_V1()
      : past_due_days(0.0),
        past_due_ind(false) {
  }
};

inline bool operator==(const RG_record_account_data_V1 &lhs, const RG_record_account_data_V1 &rhs) {
  return
      (lhs.raven_header == rhs.raven_header) &&
      (lhs.account_number == rhs.account_number) &&
      (lhs.past_due_days == rhs.past_due_days) &&
      (lhs.past_due_ind == rhs.past_due_ind) &&
      (lhs.customer_number == rhs.customer_number) &&
      (lhs.billing_zip == rhs.billing_zip) &&
      (lhs.work_zip == rhs.work_zip) &&
      (lhs.home_zip == rhs.home_zip);
}

inline bool operator!=(const RG_record_account_data_V1 &lhs, const RG_record_account_data_V1 &rhs) {
    return !(lhs == rhs);
}


struct record_account_data_V1 FLATBUFFERS_FINAL_CLASS : private flatbuffers::Table {
  typedef RG_record_account_data_V1 NativeTableType;
  static const flatbuffers::TypeTable *MiniReflectTypeTable() {
    return record_account_data_V1TypeTable();
  }
  static FLATBUFFERS_CONSTEXPR const char *GetFullyQualifiedName() {
    return "raven.generated.record_account_data_V1";
  }
  enum FlatBuffersVTableOffset FLATBUFFERS_VTABLE_UNDERLYING_TYPE {
    VT_RAVEN_HEADER = 4,
    VT_ACCOUNT_NUMBER = 6,
    VT_PAST_DUE_DAYS = 8,
    VT_PAST_DUE_IND = 10,
    VT_CUSTOMER_NUMBER = 12,
    VT_BILLING_ZIP = 14,
    VT_WORK_ZIP = 16,
    VT_HOME_ZIP = 18
  };
  const raven::base::fbs::RecordHeader *raven_header() const {
    return GetStruct<const raven::base::fbs::RecordHeader *>(VT_RAVEN_HEADER);
  }
  raven::base::fbs::RecordHeader *mutable_raven_header() {
    return GetStruct<raven::base::fbs::RecordHeader *>(VT_RAVEN_HEADER);
  }
  const flatbuffers::String *account_number() const {
    return GetPointer<const flatbuffers::String *>(VT_ACCOUNT_NUMBER);
  }
  flatbuffers::String *mutable_account_number() {
    return GetPointer<flatbuffers::String *>(VT_ACCOUNT_NUMBER);
  }
  double past_due_days() const {
    return GetField<double>(VT_PAST_DUE_DAYS, 0.0);
  }
  bool mutate_past_due_days(double _past_due_days) {
    return SetField<double>(VT_PAST_DUE_DAYS, _past_due_days, 0.0);
  }
  bool past_due_ind() const {
    return GetField<uint8_t>(VT_PAST_DUE_IND, 0) != 0;
  }
  bool mutate_past_due_ind(bool _past_due_ind) {
    return SetField<uint8_t>(VT_PAST_DUE_IND, static_cast<uint8_t>(_past_due_ind), 0);
  }
  const flatbuffers::String *customer_number() const {
    return GetPointer<const flatbuffers::String *>(VT_CUSTOMER_NUMBER);
  }
  flatbuffers::String *mutable_customer_number() {
    return GetPointer<flatbuffers::String *>(VT_CUSTOMER_NUMBER);
  }
  const flatbuffers::String *billing_zip() const {
    return GetPointer<const flatbuffers::String *>(VT_BILLING_ZIP);
  }
  flatbuffers::String *mutable_billing_zip() {
    return GetPointer<flatbuffers::String *>(VT_BILLING_ZIP);
  }
  const flatbuffers::String *work_zip() const {
    return GetPointer<const flatbuffers::String *>(VT_WORK_ZIP);
  }
  flatbuffers::String *mutable_work_zip() {
    return GetPointer<flatbuffers::String *>(VT_WORK_ZIP);
  }
  const flatbuffers::String *home_zip() const {
    return GetPointer<const flatbuffers::String *>(VT_HOME_ZIP);
  }
  flatbuffers::String *mutable_home_zip() {
    return GetPointer<flatbuffers::String *>(VT_HOME_ZIP);
  }
  bool Verify(flatbuffers::Verifier &verifier) const {
    return VerifyTableStart(verifier) &&
           VerifyField<raven::base::fbs::RecordHeader>(verifier, VT_RAVEN_HEADER) &&
           VerifyOffset(verifier, VT_ACCOUNT_NUMBER) &&
           verifier.VerifyString(account_number()) &&
           VerifyField<double>(verifier, VT_PAST_DUE_DAYS) &&
           VerifyField<uint8_t>(verifier, VT_PAST_DUE_IND) &&
           VerifyOffset(verifier, VT_CUSTOMER_NUMBER) &&
           verifier.VerifyString(customer_number()) &&
           VerifyOffset(verifier, VT_BILLING_ZIP) &&
           verifier.VerifyString(billing_zip()) &&
           VerifyOffset(verifier, VT_WORK_ZIP) &&
           verifier.VerifyString(work_zip()) &&
           VerifyOffset(verifier, VT_HOME_ZIP) &&
           verifier.VerifyString(home_zip()) &&
           verifier.EndTable();
  }
  RG_record_account_data_V1 *UnPack(const flatbuffers::resolver_function_t *_resolver = nullptr) const;
  void UnPackTo(RG_record_account_data_V1 *_o, const flatbuffers::resolver_function_t *_resolver = nullptr) const;
  static flatbuffers::Offset<record_account_data_V1> Pack(flatbuffers::FlatBufferBuilder &_fbb, const RG_record_account_data_V1* _o, const flatbuffers::rehasher_function_t *_rehasher = nullptr);
};

struct record_account_data_V1Builder {
  flatbuffers::FlatBufferBuilder &fbb_;
  flatbuffers::uoffset_t start_;
  void add_raven_header(const raven::base::fbs::RecordHeader *raven_header) {
    fbb_.AddStruct(record_account_data_V1::VT_RAVEN_HEADER, raven_header);
  }
  void add_account_number(flatbuffers::Offset<flatbuffers::String> account_number) {
    fbb_.AddOffset(record_account_data_V1::VT_ACCOUNT_NUMBER, account_number);
  }
  void add_past_due_days(double past_due_days) {
    fbb_.AddElement<double>(record_account_data_V1::VT_PAST_DUE_DAYS, past_due_days, 0.0);
  }
  void add_past_due_ind(bool past_due_ind) {
    fbb_.AddElement<uint8_t>(record_account_data_V1::VT_PAST_DUE_IND, static_cast<uint8_t>(past_due_ind), 0);
  }
  void add_customer_number(flatbuffers::Offset<flatbuffers::String> customer_number) {
    fbb_.AddOffset(record_account_data_V1::VT_CUSTOMER_NUMBER, customer_number);
  }
  void add_billing_zip(flatbuffers::Offset<flatbuffers::String> billing_zip) {
    fbb_.AddOffset(record_account_data_V1::VT_BILLING_ZIP, billing_zip);
  }
  void add_work_zip(flatbuffers::Offset<flatbuffers::String> work_zip) {
    fbb_.AddOffset(record_account_data_V1::VT_WORK_ZIP, work_zip);
  }
  void add_home_zip(flatbuffers::Offset<flatbuffers::String> home_zip) {
    fbb_.AddOffset(record_account_data_V1::VT_HOME_ZIP, home_zip);
  }
  explicit record_account_data_V1Builder(flatbuffers::FlatBufferBuilder &_fbb)
        : fbb_(_fbb) {
    start_ = fbb_.StartTable();
  }
  record_account_data_V1Builder &operator=(const record_account_data_V1Builder &);
  flatbuffers::Offset<record_account_data_V1> Finish() {
    const auto end = fbb_.EndTable(start_);
    auto o = flatbuffers::Offset<record_account_data_V1>(end);
    return o;
  }
};

inline flatbuffers::Offset<record_account_data_V1> Createrecord_account_data_V1(
    flatbuffers::FlatBufferBuilder &_fbb,
    const raven::base::fbs::RecordHeader *raven_header = 0,
    flatbuffers::Offset<flatbuffers::String> account_number = 0,
    double past_due_days = 0.0,
    bool past_due_ind = false,
    flatbuffers::Offset<flatbuffers::String> customer_number = 0,
    flatbuffers::Offset<flatbuffers::String> billing_zip = 0,
    flatbuffers::Offset<flatbuffers::String> work_zip = 0,
    flatbuffers::Offset<flatbuffers::String> home_zip = 0) {
  record_account_data_V1Builder builder_(_fbb);
  builder_.add_past_due_days(past_due_days);
  builder_.add_home_zip(home_zip);
  builder_.add_work_zip(work_zip);
  builder_.add_billing_zip(billing_zip);
  builder_.add_customer_number(customer_number);
  builder_.add_account_number(account_number);
  builder_.add_raven_header(raven_header);
  builder_.add_past_due_ind(past_due_ind);
  return builder_.Finish();
}

inline flatbuffers::Offset<record_account_data_V1> Createrecord_account_data_V1Direct(
    flatbuffers::FlatBufferBuilder &_fbb,
    const raven::base::fbs::RecordHeader *raven_header = 0,
    const char *account_number = nullptr,
    double past_due_days = 0.0,
    bool past_due_ind = false,
    const char *customer_number = nullptr,
    const char *billing_zip = nullptr,
    const char *work_zip = nullptr,
    const char *home_zip = nullptr) {
  auto account_number__ = account_number ? _fbb.CreateString(account_number) : 0;
  auto customer_number__ = customer_number ? _fbb.CreateString(customer_number) : 0;
  auto billing_zip__ = billing_zip ? _fbb.CreateString(billing_zip) : 0;
  auto work_zip__ = work_zip ? _fbb.CreateString(work_zip) : 0;
  auto home_zip__ = home_zip ? _fbb.CreateString(home_zip) : 0;
  return raven::generated::Createrecord_account_data_V1(
      _fbb,
      raven_header,
      account_number__,
      past_due_days,
      past_due_ind,
      customer_number__,
      billing_zip__,
      work_zip__,
      home_zip__);
}

flatbuffers::Offset<record_account_data_V1> Createrecord_account_data_V1(flatbuffers::FlatBufferBuilder &_fbb, const RG_record_account_data_V1 *_o, const flatbuffers::rehasher_function_t *_rehasher = nullptr);

inline RG_record_account_data_V1 *record_account_data_V1::UnPack(const flatbuffers::resolver_function_t *_resolver) const {
  auto _o = new RG_record_account_data_V1();
  UnPackTo(_o, _resolver);
  return _o;
}

inline void record_account_data_V1::UnPackTo(RG_record_account_data_V1 *_o, const flatbuffers::resolver_function_t *_resolver) const {
  (void)_o;
  (void)_resolver;
  { auto _e = raven_header(); if (_e) _o->raven_header = std::unique_ptr<raven::base::fbs::RecordHeader>(new raven::base::fbs::RecordHeader(*_e)); };
  { auto _e = account_number(); if (_e) _o->account_number = _e->str(); };
  { auto _e = past_due_days(); _o->past_due_days = _e; };
  { auto _e = past_due_ind(); _o->past_due_ind = _e; };
  { auto _e = customer_number(); if (_e) _o->customer_number = _e->str(); };
  { auto _e = billing_zip(); if (_e) _o->billing_zip = _e->str(); };
  { auto _e = work_zip(); if (_e) _o->work_zip = _e->str(); };
  { auto _e = home_zip(); if (_e) _o->home_zip = _e->str(); };
}

inline flatbuffers::Offset<record_account_data_V1> record_account_data_V1::Pack(flatbuffers::FlatBufferBuilder &_fbb, const RG_record_account_data_V1* _o, const flatbuffers::rehasher_function_t *_rehasher) {
  return Createrecord_account_data_V1(_fbb, _o, _rehasher);
}

inline flatbuffers::Offset<record_account_data_V1> Createrecord_account_data_V1(flatbuffers::FlatBufferBuilder &_fbb, const RG_record_account_data_V1 *_o, const flatbuffers::rehasher_function_t *_rehasher) {
  (void)_rehasher;
  (void)_o;
  struct _VectorArgs { flatbuffers::FlatBufferBuilder *__fbb; const RG_record_account_data_V1* __o; const flatbuffers::rehasher_function_t *__rehasher; } _va = { &_fbb, _o, _rehasher}; (void)_va;
  auto _raven_header = _o->raven_header ? _o->raven_header.get() : 0;
  auto _account_number = _o->account_number.empty() ? 0 : _fbb.CreateString(_o->account_number);
  auto _past_due_days = _o->past_due_days;
  auto _past_due_ind = _o->past_due_ind;
  auto _customer_number = _o->customer_number.empty() ? 0 : _fbb.CreateString(_o->customer_number);
  auto _billing_zip = _o->billing_zip.empty() ? 0 : _fbb.CreateString(_o->billing_zip);
  auto _work_zip = _o->work_zip.empty() ? 0 : _fbb.CreateString(_o->work_zip);
  auto _home_zip = _o->home_zip.empty() ? 0 : _fbb.CreateString(_o->home_zip);
  return raven::generated::Createrecord_account_data_V1(
      _fbb,
      _raven_header,
      _account_number,
      _past_due_days,
      _past_due_ind,
      _customer_number,
      _billing_zip,
      _work_zip,
      _home_zip);
}

inline const flatbuffers::TypeTable *record_account_data_V1TypeTable() {
  static const flatbuffers::TypeCode type_codes[] = {
    { flatbuffers::ET_SEQUENCE, 0, 0 },
    { flatbuffers::ET_STRING, 0, -1 },
    { flatbuffers::ET_DOUBLE, 0, -1 },
    { flatbuffers::ET_BOOL, 0, -1 },
    { flatbuffers::ET_STRING, 0, -1 },
    { flatbuffers::ET_STRING, 0, -1 },
    { flatbuffers::ET_STRING, 0, -1 },
    { flatbuffers::ET_STRING, 0, -1 }
  };
  static const flatbuffers::TypeFunction type_refs[] = {
    raven::base::fbs::RecordHeaderTypeTable
  };
  static const char * const names[] = {
    "raven_header",
    "account_number",
    "past_due_days",
    "past_due_ind",
    "customer_number",
    "billing_zip",
    "work_zip",
    "home_zip"
  };
  static const flatbuffers::TypeTable tt = {
    flatbuffers::ST_TABLE, 8, type_codes, type_refs, nullptr, names
  };
  return &tt;
}

inline const raven::generated::record_account_data_V1 *Getrecord_account_data_V1(const void *buf) {
  return flatbuffers::GetRoot<raven::generated::record_account_data_V1>(buf);
}

inline const raven::generated::record_account_data_V1 *GetSizePrefixedrecord_account_data_V1(const void *buf) {
  return flatbuffers::GetSizePrefixedRoot<raven::generated::record_account_data_V1>(buf);
}

inline record_account_data_V1 *GetMutablerecord_account_data_V1(void *buf) {
  return flatbuffers::GetMutableRoot<record_account_data_V1>(buf);
}

inline bool Verifyrecord_account_data_V1Buffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifyBuffer<raven::generated::record_account_data_V1>(nullptr);
}

inline bool VerifySizePrefixedrecord_account_data_V1Buffer(
    flatbuffers::Verifier &verifier) {
  return verifier.VerifySizePrefixedBuffer<raven::generated::record_account_data_V1>(nullptr);
}

inline void Finishrecord_account_data_V1Buffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<raven::generated::record_account_data_V1> root) {
  fbb.Finish(root);
}

inline void FinishSizePrefixedrecord_account_data_V1Buffer(
    flatbuffers::FlatBufferBuilder &fbb,
    flatbuffers::Offset<raven::generated::record_account_data_V1> root) {
  fbb.FinishSizePrefixed(root);
}

inline std::unique_ptr<RG_record_account_data_V1> UnPackrecord_account_data_V1(
    const void *buf,
    const flatbuffers::resolver_function_t *res = nullptr) {
  return std::unique_ptr<RG_record_account_data_V1>(Getrecord_account_data_V1(buf)->UnPack(res));
}

}  // namespace generated
}  // namespace raven

#endif  // FLATBUFFERS_GENERATED_RECORDACCOUNTDATAV1_RAVEN_GENERATED_H_
