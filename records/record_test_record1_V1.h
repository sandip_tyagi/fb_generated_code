#pragma once
#include"raven-engine/base/Record.h"
#include "lib/include/rstring.h"
#include "lib/include/custom_date.h"
#include "raven-engine/fbs/FlatBuffer.h"
#include "record_test_record1_V1_generated.h"

namespace raven::generated{
using namespace raven::types;

const char*getRecordName_test_record1_V1();
void init_record_test_record1_V1(RG_record_test_record1_V1 *r);
flatbuffers::uoffset_t new_record_test_record1_V1(flatbuffers::FlatBufferBuilder &fbb);
const raven::base::RecordInfo* getRecordInfo_record_test_record1_V1();
std::pair<const flatbuffers::Table *, size_t > getDefault_record_test_record1_V1();
}
