#pragma once










#include "raven-engine/base/Transaction.h"
#include "raven-engine/base/TransactionMetrics.h"
#include "../records/record_txn_auth_input_V1.h"
#include "../records/record_txn_auth_output_V1.h"
#include "../modules/module_account_level_data_V1.h"
#include "../modules/module_auth_model_V1.h"
#include "../modules/module_auth_response_V1.h"
#include "../modules/module_auth_rules_V1.h"
#include "../modules/module_card_level_data_V1.h"
#include "../modules/module_customer_level_data_V1.h"
#include "../modules/module_customer_transaction_history_rollup_V1.h"
#include "../modules/module_fraud_model_V1.h"
#include "../modules/module_fraud_rules_V1.h"
#include "../modules/module_ip_retrieval_V1.h"
#include "../modules/module_merchant_transaction_history_rollup_V1.h"
#include "../modules/module_auth_logging_module_V1.h"
namespace raven::generated {
class transaction_txn_auth_V1 : public raven::base::Transaction {
public:
    transaction_txn_auth_V1();
    bool setRecord(std::string_view recordName, const flatbuffers::Table *record) override ;
    bool setRecord(flatbuffers::DetachedBuffer detachedBuffer,
                            std::string_view recordName,
                            const flatbuffers::Table *record) override;
    flatbuffers::DetachedBuffer getRecord(std::string_view recordName) const override;
    const flatbuffers::Table * getRecordPtr(std::string_view recordName) const override;
    folly::Future<folly::Unit> run() noexcept override;
    ~transaction_txn_auth_V1() override;
private:
    void getDefaultResponse();

//    std::unique_ptr<record_txn_auth_input_V1> RI_txn_input;
    const record_txn_auth_input_V1* RI_txn_input = nullptr;
//    std::unique_ptr<record_txn_auth_output_V1> RO_txn_output;
    const record_txn_auth_output_V1* RO_txn_output = nullptr;
    std::unique_ptr<module_account_level_data_V1> MI_account_level_data;
    std::unique_ptr<module_auth_model_V1> MI_auth_model;
    std::unique_ptr<module_auth_response_V1> MI_auth_response;
    std::unique_ptr<module_auth_rules_V1> MI_auth_rules;
    std::unique_ptr<module_card_level_data_V1> MI_card_level_data;
    std::unique_ptr<module_customer_level_data_V1> MI_customer_level_data;
    std::unique_ptr<module_customer_transaction_history_rollup_V1> MI_customer_transaction_history_rollup;
    std::unique_ptr<module_fraud_model_V1> MI_fraud_model;
    std::unique_ptr<module_fraud_rules_V1> MI_fraud_rules;
    std::unique_ptr<module_ip_retrieval_V1> MI_ip_retrieval;
    std::unique_ptr<module_merchant_transaction_history_rollup_V1> MI_merchant_transaction_history_rollup;
    std::unique_ptr<module_auth_logging_module_V1> MI_auth_logging_module;
};
}
