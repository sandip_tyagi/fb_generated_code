









#include "transaction_txn_auth_V1.h"
#include <folly/executors/GlobalExecutor.h>
#include <folly/futures/FutureSplitter.h>

std::shared_ptr<raven::base::Transaction> createNewTransaction_txn_auth_V1();












static const raven::base::TransactionInfo *getTransactionInfo_txn_auth_V1() {
  using namespace raven::generated;
    static raven::base::TransactionInfo transactionInfo = {
        "txn_auth", //name
        1, //version
        false, //active
        {
        // {module id, module type}
                    {"account_level_data",module_account_level_data_V1::StaticGetModuleInfo()},
            {"auth_model",module_auth_model_V1::StaticGetModuleInfo()},
            {"auth_response",module_auth_response_V1::StaticGetModuleInfo()},
            {"auth_rules",module_auth_rules_V1::StaticGetModuleInfo()},
            {"card_level_data",module_card_level_data_V1::StaticGetModuleInfo()},
            {"customer_level_data",module_customer_level_data_V1::StaticGetModuleInfo()},
            {"customer_transaction_history_rollup",module_customer_transaction_history_rollup_V1::StaticGetModuleInfo()},
            {"fraud_model",module_fraud_model_V1::StaticGetModuleInfo()},
            {"fraud_rules",module_fraud_rules_V1::StaticGetModuleInfo()},
            {"ip_retrieval",module_ip_retrieval_V1::StaticGetModuleInfo()},
            {"merchant_transaction_history_rollup",module_merchant_transaction_history_rollup_V1::StaticGetModuleInfo()},
            {"auth_logging_module",module_auth_logging_module_V1::StaticGetModuleInfo()},
        },
        {
              {
                "raven_input_module","merchant_transaction_history_rollup",
                {
                        {"txn_input","txn_auth_input"},
                }
              },
              {
                "raven_input_module","ip_retrieval",
                {
                        {"txn_input","txn_auth_input"},
                }
              },
              {
                "raven_input_module","fraud_rules",
                {
                        {"txn_input","txn_auth_input"},
                }
              },
              {
                "raven_input_module","fraud_model",
                {
                        {"txn_input","txn_auth_input"},
                }
              },
              {
                "raven_input_module","customer_transaction_history_rollup",
                {
                        {"txn_input","txn_auth_input"},
                }
              },
              {
                "raven_input_module","card_level_data",
                {
                        {"txn_input","txn_auth_input"},
                }
              },
              {
                "raven_input_module","auth_rules",
                {
                        {"txn_input","txn_auth_input"},
                }
              },
              {
                "raven_input_module","auth_response",
                {
                        {"txn_input","txn_auth_input"},
                }
              },
              {
                "raven_input_module","auth_model",
                {
                        {"txn_input","txn_auth_input"},
                }
              },
              {
                "raven_input_module","auth_logging_module",
                {
                        {"txn_input","txn_auth_input"},
                }
              },
              {
                "merchant_transaction_history_rollup","fraud_rules",
                {
                        {"merchant_level_rollup","merchant_level_rollup"},
                }
              },
              {
                "merchant_transaction_history_rollup","fraud_model",
                {
                        {"merchant_level_rollup","merchant_level_rollup"},
                }
              },
              {
                "merchant_transaction_history_rollup","auth_model",
                {
                        {"merchant_level_rollup","merchant_level_rollup"},
                }
              },
              {
                "merchant_transaction_history_rollup","auth_logging_module",
                {
                        {"merchant_level_rollup","merchant_level_rollup"},
                }
              },
              {
                "ip_retrieval","fraud_rules",
                {
                        {"ip_address_record","ip_address_record"},
                }
              },
              {
                "ip_retrieval","auth_logging_module",
                {
                        {"ip_address_record","ip_address_record"},
                }
              },
              {
                "fraud_rules","auth_response",
                {
                        {"fraud_ruleset_result","fraud_ruleset_result"},
                }
              },
              {
                "fraud_rules","auth_logging_module",
                {
                        {"fraud_ruleset_result","fraud_ruleset_result"},
                }
              },
              {
                "fraud_model","fraud_rules",
                {
                        {"raven_gbm_model_output","fraud_score"},
                }
              },
              {
                "fraud_model","auth_logging_module",
                {
                        {"raven_gbm_model_output","fraud_score"},
                }
              },
              {
                "customer_transaction_history_rollup","fraud_rules",
                {
                        {"card_level_rollup","card_level_rollup"},
                        {"account_level_rollup","account_level_rollup"},
                        {"customer_level_rollup","customer_level_rollup"},
                }
              },
              {
                "customer_transaction_history_rollup","fraud_model",
                {
                        {"card_level_rollup","card_level_rollup"},
                        {"account_level_rollup","account_level_rollup"},
                        {"customer_level_rollup","customer_level_rollup"},
                }
              },
              {
                "customer_transaction_history_rollup","auth_rules",
                {
                        {"account_level_rollup","account_level_rollup"},
                        {"card_level_rollup","card_level_rollup"},
                        {"customer_level_rollup","customer_level_rollup"},
                }
              },
              {
                "customer_transaction_history_rollup","auth_model",
                {
                        {"account_level_rollup","account_level_rollup"},
                        {"card_level_rollup","card_level_rollup"},
                        {"customer_level_rollup","customer_level_rollup"},
                }
              },
              {
                "customer_transaction_history_rollup","auth_logging_module",
                {
                        {"account_level_rollup","account_level_rollup"},
                        {"card_level_rollup","card_level_rollup"},
                        {"customer_level_rollup","customer_level_rollup"},
                }
              },
              {
                "customer_level_data","fraud_rules",
                {
                        {"customer_data","customer_data"},
                }
              },
              {
                "customer_level_data","customer_transaction_history_rollup",
                {
                        {"customer_data","customer_data"},
                }
              },
              {
                "customer_level_data","auth_rules",
                {
                        {"customer_data","customer_data"},
                }
              },
              {
                "customer_level_data","auth_logging_module",
                {
                        {"customer_data","customer_data"},
                }
              },
              {
                "card_level_data","fraud_rules",
                {
                        {"card_data","card_data"},
                }
              },
              {
                "card_level_data","customer_transaction_history_rollup",
                {
                        {"card_data","card_data"},
                }
              },
              {
                "card_level_data","customer_level_data",
                {
                        {"card_data","card_data"},
                }
              },
              {
                "card_level_data","auth_rules",
                {
                        {"card_data","card_data"},
                }
              },
              {
                "card_level_data","auth_logging_module",
                {
                        {"card_data","card_data"},
                }
              },
              {
                "card_level_data","account_level_data",
                {
                        {"card_data","card_data"},
                }
              },
              {
                "auth_rules","auth_response",
                {
                        {"auth_ruleset_result","auth_ruleset_result"},
                }
              },
              {
                "auth_rules","auth_logging_module",
                {
                        {"auth_ruleset_result","auth_ruleset_result"},
                }
              },
              {
                "auth_response","raven_output_module",
                {
                        {"txn_auth_output","txn_output"},
                }
              },
              {
                "auth_response","auth_logging_module",
                {
                        {"txn_auth_output","txn_auth_output"},
                }
              },
              {
                "auth_model","auth_rules",
                {
                        {"raven_gbm_model_output","auth_model_score"},
                }
              },
              {
                "auth_model","auth_logging_module",
                {
                        {"raven_gbm_model_output","auth_score"},
                }
              },
              {
                "account_level_data","merchant_transaction_history_rollup",
                {
                        {"account_data","account_data"},
                }
              },
              {
                "account_level_data","fraud_rules",
                {
                        {"account_data","account_data"},
                }
              },
              {
                "account_level_data","auth_rules",
                {
                        {"account_data","account_data"},
                }
              },
              {
                "account_level_data","auth_model",
                {
                        {"account_data","account_data"},
                }
              },
              {
                "account_level_data","auth_logging_module",
                {
                        {"account_data","account_data"},
                }
              },
        },
        std::chrono::milliseconds(2000), // timeout
        {
        }, // Channels
        std::make_shared<raven::base::TransactionMetrics>("txn_auth",1),
        {
      //    {"inp_txn_rid", {getRecordInfo_cust_record_V2(), false}} // Transaction Input Records
              {"txn_input",{getRecordInfo_record_txn_auth_input_V1(), false}},
        },
        {
        // {"inp_txn_rid", {getRecordInfo_cust_record_V2(), false}} // Transaction Output Records
            {"txn_output",{getRecordInfo_record_txn_auth_output_V1(), false}},
        },
        nullptr, //      std::shared_ptr<Transaction> (*CreateNewTransactionBinary)(std::unordered_map<RecordId, RecordData> input);
        nullptr, //      std::shared_ptr<Transaction> (*CreateNewTransactionJSON)(const nlohmann::json &input);
        createNewTransaction_txn_auth_V1 //      std::shared_ptr<Transaction> (*CreateEmptyTransactionWithChannelInfo)();
    };
    return &transactionInfo;
}


bool raven::generated::transaction_txn_auth_V1::setRecord(std::string_view recordName, const flatbuffers::Table *record) {
    if(recordName=="txn_input") {
        RI_txn_input = Getrecord_txn_auth_input_V1 (reinterpret_cast<const void *>(record));
      return true;
    }
    return false;
}

bool raven::generated::transaction_txn_auth_V1::setRecord(flatbuffers::DetachedBuffer detachedBuffer, std::string_view recordName, const flatbuffers::Table *record) {
        if(recordName=="txn_input") {
                RI_txn_input = Getrecord_txn_auth_input_V1 (reinterpret_cast<const void *>(record));
        buffer.push_back(std::move(detachedBuffer));
        return true;
        }
    return false;
}

struct ModuleInfotxn_auth_V1 {
    ::raven::base::ModuleInfo * account_level_data = const_cast<::raven::base::ModuleInfo*>(::raven::generated::module_account_level_data_V1::StaticGetModuleInfo());
    ::raven::base::ModuleInfo * auth_model = const_cast<::raven::base::ModuleInfo*>(::raven::generated::module_auth_model_V1::StaticGetModuleInfo());
    ::raven::base::ModuleInfo * auth_response = const_cast<::raven::base::ModuleInfo*>(::raven::generated::module_auth_response_V1::StaticGetModuleInfo());
    ::raven::base::ModuleInfo * auth_rules = const_cast<::raven::base::ModuleInfo*>(::raven::generated::module_auth_rules_V1::StaticGetModuleInfo());
    ::raven::base::ModuleInfo * card_level_data = const_cast<::raven::base::ModuleInfo*>(::raven::generated::module_card_level_data_V1::StaticGetModuleInfo());
    ::raven::base::ModuleInfo * customer_level_data = const_cast<::raven::base::ModuleInfo*>(::raven::generated::module_customer_level_data_V1::StaticGetModuleInfo());
    ::raven::base::ModuleInfo * customer_transaction_history_rollup = const_cast<::raven::base::ModuleInfo*>(::raven::generated::module_customer_transaction_history_rollup_V1::StaticGetModuleInfo());
    ::raven::base::ModuleInfo * fraud_model = const_cast<::raven::base::ModuleInfo*>(::raven::generated::module_fraud_model_V1::StaticGetModuleInfo());
    ::raven::base::ModuleInfo * fraud_rules = const_cast<::raven::base::ModuleInfo*>(::raven::generated::module_fraud_rules_V1::StaticGetModuleInfo());
    ::raven::base::ModuleInfo * ip_retrieval = const_cast<::raven::base::ModuleInfo*>(::raven::generated::module_ip_retrieval_V1::StaticGetModuleInfo());
    ::raven::base::ModuleInfo * merchant_transaction_history_rollup = const_cast<::raven::base::ModuleInfo*>(::raven::generated::module_merchant_transaction_history_rollup_V1::StaticGetModuleInfo());
    ::raven::base::ModuleInfo * auth_logging_module = const_cast<::raven::base::ModuleInfo*>(::raven::generated::module_auth_logging_module_V1::StaticGetModuleInfo());
};


folly::Future<folly::Unit> raven::generated::transaction_txn_auth_V1::run() noexcept {
    /* */
    static ModuleInfotxn_auth_V1 moduleInfo;
    auto cpuExecutor = folly::getCPUExecutor();
    std::shared_ptr<transaction_txn_auth_V1> txn = std::dynamic_pointer_cast<transaction_txn_auth_V1>(shared_from_this());
    if (!txn) {
       return folly::make_exception_wrapper<std::runtime_error>("Error in casting shared_ptr during transaction");
    }

    /* Create all modules to be executed */
    MI_account_level_data = std::make_unique<module_account_level_data_V1>(this);
    MI_auth_model = std::make_unique<module_auth_model_V1>(this);
    MI_auth_response = std::make_unique<module_auth_response_V1>(this);
    MI_auth_rules = std::make_unique<module_auth_rules_V1>(this);
    MI_card_level_data = std::make_unique<module_card_level_data_V1>(this);
    MI_customer_level_data = std::make_unique<module_customer_level_data_V1>(this);
    MI_customer_transaction_history_rollup = std::make_unique<module_customer_transaction_history_rollup_V1>(this);
    MI_fraud_model = std::make_unique<module_fraud_model_V1>(this);
    MI_fraud_rules = std::make_unique<module_fraud_rules_V1>(this);
    MI_ip_retrieval = std::make_unique<module_ip_retrieval_V1>(this);
    MI_merchant_transaction_history_rollup = std::make_unique<module_merchant_transaction_history_rollup_V1>(this);
    MI_auth_logging_module = std::make_unique<module_auth_logging_module_V1>(this);
    /* End create all modules to be executed */
    /* generate one future for mapping transaction input */
        auto FS_raven_input_module = folly::splitFuture(folly::via(cpuExecutor.get(), [txn] {
                    txn->MI_merchant_transaction_history_rollup->txn_auth_input = txn->RI_txn_input;
                    txn->MI_ip_retrieval->txn_auth_input = txn->RI_txn_input;
                    txn->MI_fraud_rules->txn_auth_input = txn->RI_txn_input;
                    txn->MI_fraud_model->txn_auth_input = txn->RI_txn_input;
                    txn->MI_customer_transaction_history_rollup->txn_auth_input = txn->RI_txn_input;
                    txn->MI_card_level_data->txn_auth_input = txn->RI_txn_input;
                    txn->MI_auth_rules->txn_auth_input = txn->RI_txn_input;
                    txn->MI_auth_response->txn_auth_input = txn->RI_txn_input;
                    txn->MI_auth_model->txn_auth_input = txn->RI_txn_input;
                    txn->MI_auth_logging_module->txn_auth_input = txn->RI_txn_input;
        }));
        auto FS_ip_retrieval_P = folly::collect(
            // in collect block.
        // predecessor {customer_level_data=[card_level_data], fraud_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup], auth_rules=[raven_input_module, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_model, account_level_data], auth_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup, account_level_data], raven_input_module=[], account_level_data=[card_level_data], fraud_rules=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, account_level_data], auth_response=[raven_input_module, fraud_rules, auth_rules], ip_retrieval=[raven_input_module], raven_output_module=[auth_response], card_level_data=[raven_input_module], customer_transaction_history_rollup=[raven_input_module, customer_level_data, card_level_data], merchant_transaction_history_rollup=[raven_input_module, account_level_data], auth_logging_module=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_rules, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_rules, auth_response, auth_model, account_level_data]}
        // module ip_retrieval
                // in for loop
                FS_raven_input_module.getFuture()        ).via(cpuExecutor.get())
        .thenValue([txn](auto unit [[maybe_unused]]) -> folly::Future<folly::Unit> {
            txn->MI_ip_retrieval->init();
        // If there is a timeout set
            auto lFut = txn->MI_ip_retrieval->start();
            if( moduleInfo.ip_retrieval->moduleTimeout > std::chrono::microseconds(0)) {
              lFut = std::move(lFut).within(std::chrono::ceil<std::chrono::milliseconds>(moduleInfo.ip_retrieval->moduleTimeout))
                .thenError<folly::FutureTimeout>([txn](folly::FutureTimeout ){
                    LOG(INFO) << "Module ip_retrieval timedout for transaction " << txn->messageMetaDataStruct.getTransactionId().toHexString();
                    ++moduleInfo.ip_retrieval->metrics->timeoutCountExecution;
                    std::lock_guard lg(txn->MI_ip_retrieval->responseLock);
                    txn->MI_ip_retrieval->timedout = true;
                    return folly::Unit();
                });
            }
            return std::move(lFut).thenError([txn] (folly::exception_wrapper ew) -> folly::Future<folly::Unit> {
              std::lock_guard lg(txn->MI_ip_retrieval->responseLock);
              txn->MI_ip_retrieval->timedout = true;
              LOG(INFO) << "Module ip_retrieval error " <<  txn->messageMetaDataStruct.getTransactionId().toHexString() << " " << ew.what() << " false";
              return ew;
            });
        });


        FS_ip_retrieval_P = std::move(FS_ip_retrieval_P)
        .thenValue([txn](auto unit [[maybe_unused]]) {
//                    txn->MI_fraud_rules->ip_address_record = txn->MI_ip_retrieval->ip_address_record;
//                    txn->MI_fraud_rules->ip_address_record = raven::generated::Getrecord_ip_address_record_V (txn->MI_ip_retrieval->BUFFER_ip_address_record.data());
                    txn->MI_fraud_rules->ip_address_record = txn->MI_ip_retrieval->OUT_ip_address_record;
//                    txn->MI_auth_logging_module->ip_address_record = txn->MI_ip_retrieval->ip_address_record;
//                    txn->MI_auth_logging_module->ip_address_record = raven::generated::Getrecord_ip_address_record_V (txn->MI_ip_retrieval->BUFFER_ip_address_record.data());
                    txn->MI_auth_logging_module->ip_address_record = txn->MI_ip_retrieval->OUT_ip_address_record;
        return folly::Unit();
            });
        auto FS_ip_retrieval = folly::splitFuture(std::move(FS_ip_retrieval_P));

        auto FS_card_level_data_P = folly::collect(
            // in collect block.
        // predecessor {customer_level_data=[card_level_data], fraud_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup], auth_rules=[raven_input_module, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_model, account_level_data], auth_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup, account_level_data], raven_input_module=[], account_level_data=[card_level_data], fraud_rules=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, account_level_data], auth_response=[raven_input_module, fraud_rules, auth_rules], ip_retrieval=[raven_input_module], raven_output_module=[auth_response], card_level_data=[raven_input_module], customer_transaction_history_rollup=[raven_input_module, customer_level_data, card_level_data], merchant_transaction_history_rollup=[raven_input_module, account_level_data], auth_logging_module=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_rules, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_rules, auth_response, auth_model, account_level_data]}
        // module card_level_data
                // in for loop
                FS_raven_input_module.getFuture()        ).via(cpuExecutor.get())
        .thenValue([txn](auto unit [[maybe_unused]]) -> folly::Future<folly::Unit> {
            txn->MI_card_level_data->init();
        // If there is a timeout set
            auto lFut = txn->MI_card_level_data->start();
            if( moduleInfo.card_level_data->moduleTimeout > std::chrono::microseconds(0)) {
              lFut = std::move(lFut).within(std::chrono::ceil<std::chrono::milliseconds>(moduleInfo.card_level_data->moduleTimeout))
                .thenError<folly::FutureTimeout>([txn](folly::FutureTimeout ){
                    LOG(INFO) << "Module card_level_data timedout for transaction " << txn->messageMetaDataStruct.getTransactionId().toHexString();
                    ++moduleInfo.card_level_data->metrics->timeoutCountExecution;
                    std::lock_guard lg(txn->MI_card_level_data->responseLock);
                    txn->MI_card_level_data->timedout = true;
                    return folly::Unit();
                });
            }
            return std::move(lFut).thenError([txn] (folly::exception_wrapper ew) -> folly::Future<folly::Unit> {
              std::lock_guard lg(txn->MI_card_level_data->responseLock);
              txn->MI_card_level_data->timedout = true;
              LOG(INFO) << "Module card_level_data error " <<  txn->messageMetaDataStruct.getTransactionId().toHexString() << " " << ew.what() << " false";
              return ew;
            });
        });


        FS_card_level_data_P = std::move(FS_card_level_data_P)
        .thenValue([txn](auto unit [[maybe_unused]]) {
//                    txn->MI_fraud_rules->card_data = txn->MI_card_level_data->card_data;
//                    txn->MI_fraud_rules->card_data = raven::generated::Getrecord_card_data_V (txn->MI_card_level_data->BUFFER_card_data.data());
                    txn->MI_fraud_rules->card_data = txn->MI_card_level_data->OUT_card_data;
//                    txn->MI_customer_transaction_history_rollup->card_data = txn->MI_card_level_data->card_data;
//                    txn->MI_customer_transaction_history_rollup->card_data = raven::generated::Getrecord_card_data_V (txn->MI_card_level_data->BUFFER_card_data.data());
                    txn->MI_customer_transaction_history_rollup->card_data = txn->MI_card_level_data->OUT_card_data;
//                    txn->MI_customer_level_data->card_data = txn->MI_card_level_data->card_data;
//                    txn->MI_customer_level_data->card_data = raven::generated::Getrecord_card_data_V (txn->MI_card_level_data->BUFFER_card_data.data());
                    txn->MI_customer_level_data->card_data = txn->MI_card_level_data->OUT_card_data;
//                    txn->MI_auth_rules->card_data = txn->MI_card_level_data->card_data;
//                    txn->MI_auth_rules->card_data = raven::generated::Getrecord_card_data_V (txn->MI_card_level_data->BUFFER_card_data.data());
                    txn->MI_auth_rules->card_data = txn->MI_card_level_data->OUT_card_data;
//                    txn->MI_auth_logging_module->card_data = txn->MI_card_level_data->card_data;
//                    txn->MI_auth_logging_module->card_data = raven::generated::Getrecord_card_data_V (txn->MI_card_level_data->BUFFER_card_data.data());
                    txn->MI_auth_logging_module->card_data = txn->MI_card_level_data->OUT_card_data;
//                    txn->MI_account_level_data->card_data = txn->MI_card_level_data->card_data;
//                    txn->MI_account_level_data->card_data = raven::generated::Getrecord_card_data_V (txn->MI_card_level_data->BUFFER_card_data.data());
                    txn->MI_account_level_data->card_data = txn->MI_card_level_data->OUT_card_data;
        return folly::Unit();
            });
        auto FS_card_level_data = folly::splitFuture(std::move(FS_card_level_data_P));

        auto FS_customer_level_data_P = folly::collect(
            // in collect block.
        // predecessor {customer_level_data=[card_level_data], fraud_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup], auth_rules=[raven_input_module, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_model, account_level_data], auth_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup, account_level_data], raven_input_module=[], account_level_data=[card_level_data], fraud_rules=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, account_level_data], auth_response=[raven_input_module, fraud_rules, auth_rules], ip_retrieval=[raven_input_module], raven_output_module=[auth_response], card_level_data=[raven_input_module], customer_transaction_history_rollup=[raven_input_module, customer_level_data, card_level_data], merchant_transaction_history_rollup=[raven_input_module, account_level_data], auth_logging_module=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_rules, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_rules, auth_response, auth_model, account_level_data]}
        // module customer_level_data
                // in for loop
                FS_card_level_data.getFuture()        ).via(cpuExecutor.get())
        .thenValue([txn](auto unit [[maybe_unused]]) -> folly::Future<folly::Unit> {
            txn->MI_customer_level_data->init();
        // If there is a timeout set
            auto lFut = txn->MI_customer_level_data->start();
            if( moduleInfo.customer_level_data->moduleTimeout > std::chrono::microseconds(0)) {
              lFut = std::move(lFut).within(std::chrono::ceil<std::chrono::milliseconds>(moduleInfo.customer_level_data->moduleTimeout))
                .thenError<folly::FutureTimeout>([txn](folly::FutureTimeout ){
                    LOG(INFO) << "Module customer_level_data timedout for transaction " << txn->messageMetaDataStruct.getTransactionId().toHexString();
                    ++moduleInfo.customer_level_data->metrics->timeoutCountExecution;
                    std::lock_guard lg(txn->MI_customer_level_data->responseLock);
                    txn->MI_customer_level_data->timedout = true;
                    return folly::Unit();
                });
            }
            return std::move(lFut).thenError([txn] (folly::exception_wrapper ew) -> folly::Future<folly::Unit> {
              std::lock_guard lg(txn->MI_customer_level_data->responseLock);
              txn->MI_customer_level_data->timedout = true;
              LOG(INFO) << "Module customer_level_data error " <<  txn->messageMetaDataStruct.getTransactionId().toHexString() << " " << ew.what() << " false";
              return ew;
            });
        });


        FS_customer_level_data_P = std::move(FS_customer_level_data_P)
        .thenValue([txn](auto unit [[maybe_unused]]) {
//                    txn->MI_fraud_rules->customer_data = txn->MI_customer_level_data->customer_data;
//                    txn->MI_fraud_rules->customer_data = raven::generated::Getrecord_customer_data_V (txn->MI_customer_level_data->BUFFER_customer_data.data());
                    txn->MI_fraud_rules->customer_data = txn->MI_customer_level_data->OUT_customer_data;
//                    txn->MI_customer_transaction_history_rollup->customer_data = txn->MI_customer_level_data->customer_data;
//                    txn->MI_customer_transaction_history_rollup->customer_data = raven::generated::Getrecord_customer_data_V (txn->MI_customer_level_data->BUFFER_customer_data.data());
                    txn->MI_customer_transaction_history_rollup->customer_data = txn->MI_customer_level_data->OUT_customer_data;
//                    txn->MI_auth_rules->customer_data = txn->MI_customer_level_data->customer_data;
//                    txn->MI_auth_rules->customer_data = raven::generated::Getrecord_customer_data_V (txn->MI_customer_level_data->BUFFER_customer_data.data());
                    txn->MI_auth_rules->customer_data = txn->MI_customer_level_data->OUT_customer_data;
//                    txn->MI_auth_logging_module->customer_data = txn->MI_customer_level_data->customer_data;
//                    txn->MI_auth_logging_module->customer_data = raven::generated::Getrecord_customer_data_V (txn->MI_customer_level_data->BUFFER_customer_data.data());
                    txn->MI_auth_logging_module->customer_data = txn->MI_customer_level_data->OUT_customer_data;
        return folly::Unit();
            });
        auto FS_customer_level_data = folly::splitFuture(std::move(FS_customer_level_data_P));

        auto FS_account_level_data_P = folly::collect(
            // in collect block.
        // predecessor {customer_level_data=[card_level_data], fraud_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup], auth_rules=[raven_input_module, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_model, account_level_data], auth_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup, account_level_data], raven_input_module=[], account_level_data=[card_level_data], fraud_rules=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, account_level_data], auth_response=[raven_input_module, fraud_rules, auth_rules], ip_retrieval=[raven_input_module], raven_output_module=[auth_response], card_level_data=[raven_input_module], customer_transaction_history_rollup=[raven_input_module, customer_level_data, card_level_data], merchant_transaction_history_rollup=[raven_input_module, account_level_data], auth_logging_module=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_rules, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_rules, auth_response, auth_model, account_level_data]}
        // module account_level_data
                // in for loop
                FS_card_level_data.getFuture()        ).via(cpuExecutor.get())
        .thenValue([txn](auto unit [[maybe_unused]]) -> folly::Future<folly::Unit> {
            txn->MI_account_level_data->init();
        // If there is a timeout set
            auto lFut = txn->MI_account_level_data->start();
            if( moduleInfo.account_level_data->moduleTimeout > std::chrono::microseconds(0)) {
              lFut = std::move(lFut).within(std::chrono::ceil<std::chrono::milliseconds>(moduleInfo.account_level_data->moduleTimeout))
                .thenError<folly::FutureTimeout>([txn](folly::FutureTimeout ){
                    LOG(INFO) << "Module account_level_data timedout for transaction " << txn->messageMetaDataStruct.getTransactionId().toHexString();
                    ++moduleInfo.account_level_data->metrics->timeoutCountExecution;
                    std::lock_guard lg(txn->MI_account_level_data->responseLock);
                    txn->MI_account_level_data->timedout = true;
                    return folly::Unit();
                });
            }
            return std::move(lFut).thenError([txn] (folly::exception_wrapper ew) -> folly::Future<folly::Unit> {
              std::lock_guard lg(txn->MI_account_level_data->responseLock);
              txn->MI_account_level_data->timedout = true;
              LOG(INFO) << "Module account_level_data error " <<  txn->messageMetaDataStruct.getTransactionId().toHexString() << " " << ew.what() << " false";
              return ew;
            });
        });


        FS_account_level_data_P = std::move(FS_account_level_data_P)
        .thenValue([txn](auto unit [[maybe_unused]]) {
//                    txn->MI_merchant_transaction_history_rollup->account_data = txn->MI_account_level_data->account_data;
//                    txn->MI_merchant_transaction_history_rollup->account_data = raven::generated::Getrecord_account_data_V (txn->MI_account_level_data->BUFFER_account_data.data());
                    txn->MI_merchant_transaction_history_rollup->account_data = txn->MI_account_level_data->OUT_account_data;
//                    txn->MI_fraud_rules->account_data = txn->MI_account_level_data->account_data;
//                    txn->MI_fraud_rules->account_data = raven::generated::Getrecord_account_data_V (txn->MI_account_level_data->BUFFER_account_data.data());
                    txn->MI_fraud_rules->account_data = txn->MI_account_level_data->OUT_account_data;
//                    txn->MI_auth_rules->account_data = txn->MI_account_level_data->account_data;
//                    txn->MI_auth_rules->account_data = raven::generated::Getrecord_account_data_V (txn->MI_account_level_data->BUFFER_account_data.data());
                    txn->MI_auth_rules->account_data = txn->MI_account_level_data->OUT_account_data;
//                    txn->MI_auth_model->account_data = txn->MI_account_level_data->account_data;
//                    txn->MI_auth_model->account_data = raven::generated::Getrecord_account_data_V (txn->MI_account_level_data->BUFFER_account_data.data());
                    txn->MI_auth_model->account_data = txn->MI_account_level_data->OUT_account_data;
//                    txn->MI_auth_logging_module->account_data = txn->MI_account_level_data->account_data;
//                    txn->MI_auth_logging_module->account_data = raven::generated::Getrecord_account_data_V (txn->MI_account_level_data->BUFFER_account_data.data());
                    txn->MI_auth_logging_module->account_data = txn->MI_account_level_data->OUT_account_data;
        return folly::Unit();
            });
        auto FS_account_level_data = folly::splitFuture(std::move(FS_account_level_data_P));

        auto FS_customer_transaction_history_rollup_P = folly::collect(
            // in collect block.
        // predecessor {customer_level_data=[card_level_data], fraud_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup], auth_rules=[raven_input_module, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_model, account_level_data], auth_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup, account_level_data], raven_input_module=[], account_level_data=[card_level_data], fraud_rules=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, account_level_data], auth_response=[raven_input_module, fraud_rules, auth_rules], ip_retrieval=[raven_input_module], raven_output_module=[auth_response], card_level_data=[raven_input_module], customer_transaction_history_rollup=[raven_input_module, customer_level_data, card_level_data], merchant_transaction_history_rollup=[raven_input_module, account_level_data], auth_logging_module=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_rules, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_rules, auth_response, auth_model, account_level_data]}
        // module customer_transaction_history_rollup
                // in for loop
                FS_raven_input_module.getFuture(),                // in for loop
                FS_customer_level_data.getFuture(),                // in for loop
                FS_card_level_data.getFuture()        ).via(cpuExecutor.get())
        .thenValue([txn](auto unit [[maybe_unused]]) -> folly::Future<folly::Unit> {
            txn->MI_customer_transaction_history_rollup->init();
        // If there is a timeout set
            auto lFut = txn->MI_customer_transaction_history_rollup->start();
            if( moduleInfo.customer_transaction_history_rollup->moduleTimeout > std::chrono::microseconds(0)) {
              lFut = std::move(lFut).within(std::chrono::ceil<std::chrono::milliseconds>(moduleInfo.customer_transaction_history_rollup->moduleTimeout))
                .thenError<folly::FutureTimeout>([txn](folly::FutureTimeout ){
                    LOG(INFO) << "Module customer_transaction_history_rollup timedout for transaction " << txn->messageMetaDataStruct.getTransactionId().toHexString();
                    ++moduleInfo.customer_transaction_history_rollup->metrics->timeoutCountExecution;
                    std::lock_guard lg(txn->MI_customer_transaction_history_rollup->responseLock);
                    txn->MI_customer_transaction_history_rollup->timedout = true;
                    return folly::Unit();
                });
            }
            return std::move(lFut).thenError([txn] (folly::exception_wrapper ew) -> folly::Future<folly::Unit> {
              std::lock_guard lg(txn->MI_customer_transaction_history_rollup->responseLock);
              txn->MI_customer_transaction_history_rollup->timedout = true;
              LOG(INFO) << "Module customer_transaction_history_rollup error " <<  txn->messageMetaDataStruct.getTransactionId().toHexString() << " " << ew.what() << " false";
              return ew;
            });
        });


        FS_customer_transaction_history_rollup_P = std::move(FS_customer_transaction_history_rollup_P)
        .thenValue([txn](auto unit [[maybe_unused]]) {
//                    txn->MI_fraud_rules->card_level_rollup = txn->MI_customer_transaction_history_rollup->card_level_rollup;
//                    txn->MI_fraud_rules->card_level_rollup = raven::generated::Getrecord_card_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_card_level_rollup.data());
                    txn->MI_fraud_rules->card_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_card_level_rollup;
//                    txn->MI_fraud_rules->account_level_rollup = txn->MI_customer_transaction_history_rollup->account_level_rollup;
//                    txn->MI_fraud_rules->account_level_rollup = raven::generated::Getrecord_account_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_account_level_rollup.data());
                    txn->MI_fraud_rules->account_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_account_level_rollup;
//                    txn->MI_fraud_rules->customer_level_rollup = txn->MI_customer_transaction_history_rollup->customer_level_rollup;
//                    txn->MI_fraud_rules->customer_level_rollup = raven::generated::Getrecord_customer_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_customer_level_rollup.data());
                    txn->MI_fraud_rules->customer_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_customer_level_rollup;
//                    txn->MI_fraud_model->card_level_rollup = txn->MI_customer_transaction_history_rollup->card_level_rollup;
//                    txn->MI_fraud_model->card_level_rollup = raven::generated::Getrecord_card_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_card_level_rollup.data());
                    txn->MI_fraud_model->card_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_card_level_rollup;
//                    txn->MI_fraud_model->account_level_rollup = txn->MI_customer_transaction_history_rollup->account_level_rollup;
//                    txn->MI_fraud_model->account_level_rollup = raven::generated::Getrecord_account_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_account_level_rollup.data());
                    txn->MI_fraud_model->account_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_account_level_rollup;
//                    txn->MI_fraud_model->customer_level_rollup = txn->MI_customer_transaction_history_rollup->customer_level_rollup;
//                    txn->MI_fraud_model->customer_level_rollup = raven::generated::Getrecord_customer_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_customer_level_rollup.data());
                    txn->MI_fraud_model->customer_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_customer_level_rollup;
//                    txn->MI_auth_rules->account_level_rollup = txn->MI_customer_transaction_history_rollup->account_level_rollup;
//                    txn->MI_auth_rules->account_level_rollup = raven::generated::Getrecord_account_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_account_level_rollup.data());
                    txn->MI_auth_rules->account_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_account_level_rollup;
//                    txn->MI_auth_rules->card_level_rollup = txn->MI_customer_transaction_history_rollup->card_level_rollup;
//                    txn->MI_auth_rules->card_level_rollup = raven::generated::Getrecord_card_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_card_level_rollup.data());
                    txn->MI_auth_rules->card_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_card_level_rollup;
//                    txn->MI_auth_rules->customer_level_rollup = txn->MI_customer_transaction_history_rollup->customer_level_rollup;
//                    txn->MI_auth_rules->customer_level_rollup = raven::generated::Getrecord_customer_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_customer_level_rollup.data());
                    txn->MI_auth_rules->customer_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_customer_level_rollup;
//                    txn->MI_auth_model->account_level_rollup = txn->MI_customer_transaction_history_rollup->account_level_rollup;
//                    txn->MI_auth_model->account_level_rollup = raven::generated::Getrecord_account_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_account_level_rollup.data());
                    txn->MI_auth_model->account_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_account_level_rollup;
//                    txn->MI_auth_model->card_level_rollup = txn->MI_customer_transaction_history_rollup->card_level_rollup;
//                    txn->MI_auth_model->card_level_rollup = raven::generated::Getrecord_card_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_card_level_rollup.data());
                    txn->MI_auth_model->card_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_card_level_rollup;
//                    txn->MI_auth_model->customer_level_rollup = txn->MI_customer_transaction_history_rollup->customer_level_rollup;
//                    txn->MI_auth_model->customer_level_rollup = raven::generated::Getrecord_customer_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_customer_level_rollup.data());
                    txn->MI_auth_model->customer_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_customer_level_rollup;
//                    txn->MI_auth_logging_module->account_level_rollup = txn->MI_customer_transaction_history_rollup->account_level_rollup;
//                    txn->MI_auth_logging_module->account_level_rollup = raven::generated::Getrecord_account_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_account_level_rollup.data());
                    txn->MI_auth_logging_module->account_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_account_level_rollup;
//                    txn->MI_auth_logging_module->card_level_rollup = txn->MI_customer_transaction_history_rollup->card_level_rollup;
//                    txn->MI_auth_logging_module->card_level_rollup = raven::generated::Getrecord_card_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_card_level_rollup.data());
                    txn->MI_auth_logging_module->card_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_card_level_rollup;
//                    txn->MI_auth_logging_module->customer_level_rollup = txn->MI_customer_transaction_history_rollup->customer_level_rollup;
//                    txn->MI_auth_logging_module->customer_level_rollup = raven::generated::Getrecord_customer_level_rollup_V (txn->MI_customer_transaction_history_rollup->BUFFER_customer_level_rollup.data());
                    txn->MI_auth_logging_module->customer_level_rollup = txn->MI_customer_transaction_history_rollup->OUT_customer_level_rollup;
        return folly::Unit();
            });
        auto FS_customer_transaction_history_rollup = folly::splitFuture(std::move(FS_customer_transaction_history_rollup_P));

        auto FS_merchant_transaction_history_rollup_P = folly::collect(
            // in collect block.
        // predecessor {customer_level_data=[card_level_data], fraud_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup], auth_rules=[raven_input_module, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_model, account_level_data], auth_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup, account_level_data], raven_input_module=[], account_level_data=[card_level_data], fraud_rules=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, account_level_data], auth_response=[raven_input_module, fraud_rules, auth_rules], ip_retrieval=[raven_input_module], raven_output_module=[auth_response], card_level_data=[raven_input_module], customer_transaction_history_rollup=[raven_input_module, customer_level_data, card_level_data], merchant_transaction_history_rollup=[raven_input_module, account_level_data], auth_logging_module=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_rules, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_rules, auth_response, auth_model, account_level_data]}
        // module merchant_transaction_history_rollup
                // in for loop
                FS_raven_input_module.getFuture(),                // in for loop
                FS_account_level_data.getFuture()        ).via(cpuExecutor.get())
        .thenValue([txn](auto unit [[maybe_unused]]) -> folly::Future<folly::Unit> {
            txn->MI_merchant_transaction_history_rollup->init();
        // If there is a timeout set
            auto lFut = txn->MI_merchant_transaction_history_rollup->start();
            if( moduleInfo.merchant_transaction_history_rollup->moduleTimeout > std::chrono::microseconds(0)) {
              lFut = std::move(lFut).within(std::chrono::ceil<std::chrono::milliseconds>(moduleInfo.merchant_transaction_history_rollup->moduleTimeout))
                .thenError<folly::FutureTimeout>([txn](folly::FutureTimeout ){
                    LOG(INFO) << "Module merchant_transaction_history_rollup timedout for transaction " << txn->messageMetaDataStruct.getTransactionId().toHexString();
                    ++moduleInfo.merchant_transaction_history_rollup->metrics->timeoutCountExecution;
                    std::lock_guard lg(txn->MI_merchant_transaction_history_rollup->responseLock);
                    txn->MI_merchant_transaction_history_rollup->timedout = true;
                    return folly::Unit();
                });
            }
            return std::move(lFut).thenError([txn] (folly::exception_wrapper ew) -> folly::Future<folly::Unit> {
              std::lock_guard lg(txn->MI_merchant_transaction_history_rollup->responseLock);
              txn->MI_merchant_transaction_history_rollup->timedout = true;
              LOG(INFO) << "Module merchant_transaction_history_rollup error " <<  txn->messageMetaDataStruct.getTransactionId().toHexString() << " " << ew.what() << " false";
              return ew;
            });
        });


        FS_merchant_transaction_history_rollup_P = std::move(FS_merchant_transaction_history_rollup_P)
        .thenValue([txn](auto unit [[maybe_unused]]) {
//                    txn->MI_fraud_rules->merchant_level_rollup = txn->MI_merchant_transaction_history_rollup->merchant_level_rollup;
//                    txn->MI_fraud_rules->merchant_level_rollup = raven::generated::Getrecord_merchant_level_rollup_V (txn->MI_merchant_transaction_history_rollup->BUFFER_merchant_level_rollup.data());
                    txn->MI_fraud_rules->merchant_level_rollup = txn->MI_merchant_transaction_history_rollup->OUT_merchant_level_rollup;
//                    txn->MI_fraud_model->merchant_level_rollup = txn->MI_merchant_transaction_history_rollup->merchant_level_rollup;
//                    txn->MI_fraud_model->merchant_level_rollup = raven::generated::Getrecord_merchant_level_rollup_V (txn->MI_merchant_transaction_history_rollup->BUFFER_merchant_level_rollup.data());
                    txn->MI_fraud_model->merchant_level_rollup = txn->MI_merchant_transaction_history_rollup->OUT_merchant_level_rollup;
//                    txn->MI_auth_model->merchant_level_rollup = txn->MI_merchant_transaction_history_rollup->merchant_level_rollup;
//                    txn->MI_auth_model->merchant_level_rollup = raven::generated::Getrecord_merchant_level_rollup_V (txn->MI_merchant_transaction_history_rollup->BUFFER_merchant_level_rollup.data());
                    txn->MI_auth_model->merchant_level_rollup = txn->MI_merchant_transaction_history_rollup->OUT_merchant_level_rollup;
//                    txn->MI_auth_logging_module->merchant_level_rollup = txn->MI_merchant_transaction_history_rollup->merchant_level_rollup;
//                    txn->MI_auth_logging_module->merchant_level_rollup = raven::generated::Getrecord_merchant_level_rollup_V (txn->MI_merchant_transaction_history_rollup->BUFFER_merchant_level_rollup.data());
                    txn->MI_auth_logging_module->merchant_level_rollup = txn->MI_merchant_transaction_history_rollup->OUT_merchant_level_rollup;
        return folly::Unit();
            });
        auto FS_merchant_transaction_history_rollup = folly::splitFuture(std::move(FS_merchant_transaction_history_rollup_P));

        auto FS_fraud_model_P = folly::collect(
            // in collect block.
        // predecessor {customer_level_data=[card_level_data], fraud_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup], auth_rules=[raven_input_module, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_model, account_level_data], auth_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup, account_level_data], raven_input_module=[], account_level_data=[card_level_data], fraud_rules=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, account_level_data], auth_response=[raven_input_module, fraud_rules, auth_rules], ip_retrieval=[raven_input_module], raven_output_module=[auth_response], card_level_data=[raven_input_module], customer_transaction_history_rollup=[raven_input_module, customer_level_data, card_level_data], merchant_transaction_history_rollup=[raven_input_module, account_level_data], auth_logging_module=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_rules, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_rules, auth_response, auth_model, account_level_data]}
        // module fraud_model
                // in for loop
                FS_raven_input_module.getFuture(),                // in for loop
                FS_merchant_transaction_history_rollup.getFuture(),                // in for loop
                FS_customer_transaction_history_rollup.getFuture()        ).via(cpuExecutor.get())
        .thenValue([txn](auto unit [[maybe_unused]]) -> folly::Future<folly::Unit> {
            txn->MI_fraud_model->init();
        // If there is a timeout set
            auto lFut = txn->MI_fraud_model->start();
            if( moduleInfo.fraud_model->moduleTimeout > std::chrono::microseconds(0)) {
              lFut = std::move(lFut).within(std::chrono::ceil<std::chrono::milliseconds>(moduleInfo.fraud_model->moduleTimeout))
                .thenError<folly::FutureTimeout>([txn](folly::FutureTimeout ){
                    LOG(INFO) << "Module fraud_model timedout for transaction " << txn->messageMetaDataStruct.getTransactionId().toHexString();
                    ++moduleInfo.fraud_model->metrics->timeoutCountExecution;
                    std::lock_guard lg(txn->MI_fraud_model->responseLock);
                    txn->MI_fraud_model->timedout = true;
                    return folly::Unit();
                });
            }
            return std::move(lFut).thenError([txn] (folly::exception_wrapper ew) -> folly::Future<folly::Unit> {
              std::lock_guard lg(txn->MI_fraud_model->responseLock);
              txn->MI_fraud_model->timedout = true;
              LOG(INFO) << "Module fraud_model error " <<  txn->messageMetaDataStruct.getTransactionId().toHexString() << " " << ew.what() << " false";
              return ew;
            });
        });


        FS_fraud_model_P = std::move(FS_fraud_model_P)
        .thenValue([txn](auto unit [[maybe_unused]]) {
//                    txn->MI_fraud_rules->fraud_score = txn->MI_fraud_model->raven_gbm_model_output;
//                    txn->MI_fraud_rules->fraud_score = raven::generated::Getrecord_raven_gbm_model_output_V (txn->MI_fraud_model->BUFFER_raven_gbm_model_output.data());
                    txn->MI_fraud_rules->fraud_score = txn->MI_fraud_model->OUT_raven_gbm_model_output;
//                    txn->MI_auth_logging_module->fraud_score = txn->MI_fraud_model->raven_gbm_model_output;
//                    txn->MI_auth_logging_module->fraud_score = raven::generated::Getrecord_raven_gbm_model_output_V (txn->MI_fraud_model->BUFFER_raven_gbm_model_output.data());
                    txn->MI_auth_logging_module->fraud_score = txn->MI_fraud_model->OUT_raven_gbm_model_output;
        return folly::Unit();
            });
        auto FS_fraud_model = folly::splitFuture(std::move(FS_fraud_model_P));

        auto FS_auth_model_P = folly::collect(
            // in collect block.
        // predecessor {customer_level_data=[card_level_data], fraud_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup], auth_rules=[raven_input_module, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_model, account_level_data], auth_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup, account_level_data], raven_input_module=[], account_level_data=[card_level_data], fraud_rules=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, account_level_data], auth_response=[raven_input_module, fraud_rules, auth_rules], ip_retrieval=[raven_input_module], raven_output_module=[auth_response], card_level_data=[raven_input_module], customer_transaction_history_rollup=[raven_input_module, customer_level_data, card_level_data], merchant_transaction_history_rollup=[raven_input_module, account_level_data], auth_logging_module=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_rules, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_rules, auth_response, auth_model, account_level_data]}
        // module auth_model
                // in for loop
                FS_raven_input_module.getFuture(),                // in for loop
                FS_merchant_transaction_history_rollup.getFuture(),                // in for loop
                FS_customer_transaction_history_rollup.getFuture(),                // in for loop
                FS_account_level_data.getFuture()        ).via(cpuExecutor.get())
        .thenValue([txn](auto unit [[maybe_unused]]) -> folly::Future<folly::Unit> {
            txn->MI_auth_model->init();
        // If there is a timeout set
            auto lFut = txn->MI_auth_model->start();
            if( moduleInfo.auth_model->moduleTimeout > std::chrono::microseconds(0)) {
              lFut = std::move(lFut).within(std::chrono::ceil<std::chrono::milliseconds>(moduleInfo.auth_model->moduleTimeout))
                .thenError<folly::FutureTimeout>([txn](folly::FutureTimeout ){
                    LOG(INFO) << "Module auth_model timedout for transaction " << txn->messageMetaDataStruct.getTransactionId().toHexString();
                    ++moduleInfo.auth_model->metrics->timeoutCountExecution;
                    std::lock_guard lg(txn->MI_auth_model->responseLock);
                    txn->MI_auth_model->timedout = true;
                    return folly::Unit();
                });
            }
            return std::move(lFut).thenError([txn] (folly::exception_wrapper ew) -> folly::Future<folly::Unit> {
              std::lock_guard lg(txn->MI_auth_model->responseLock);
              txn->MI_auth_model->timedout = true;
              LOG(INFO) << "Module auth_model error " <<  txn->messageMetaDataStruct.getTransactionId().toHexString() << " " << ew.what() << " false";
              return ew;
            });
        });


        FS_auth_model_P = std::move(FS_auth_model_P)
        .thenValue([txn](auto unit [[maybe_unused]]) {
//                    txn->MI_auth_rules->auth_model_score = txn->MI_auth_model->raven_gbm_model_output;
//                    txn->MI_auth_rules->auth_model_score = raven::generated::Getrecord_raven_gbm_model_output_V (txn->MI_auth_model->BUFFER_raven_gbm_model_output.data());
                    txn->MI_auth_rules->auth_model_score = txn->MI_auth_model->OUT_raven_gbm_model_output;
//                    txn->MI_auth_logging_module->auth_score = txn->MI_auth_model->raven_gbm_model_output;
//                    txn->MI_auth_logging_module->auth_score = raven::generated::Getrecord_raven_gbm_model_output_V (txn->MI_auth_model->BUFFER_raven_gbm_model_output.data());
                    txn->MI_auth_logging_module->auth_score = txn->MI_auth_model->OUT_raven_gbm_model_output;
        return folly::Unit();
            });
        auto FS_auth_model = folly::splitFuture(std::move(FS_auth_model_P));

        auto FS_fraud_rules_P = folly::collect(
            // in collect block.
        // predecessor {customer_level_data=[card_level_data], fraud_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup], auth_rules=[raven_input_module, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_model, account_level_data], auth_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup, account_level_data], raven_input_module=[], account_level_data=[card_level_data], fraud_rules=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, account_level_data], auth_response=[raven_input_module, fraud_rules, auth_rules], ip_retrieval=[raven_input_module], raven_output_module=[auth_response], card_level_data=[raven_input_module], customer_transaction_history_rollup=[raven_input_module, customer_level_data, card_level_data], merchant_transaction_history_rollup=[raven_input_module, account_level_data], auth_logging_module=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_rules, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_rules, auth_response, auth_model, account_level_data]}
        // module fraud_rules
                // in for loop
                FS_raven_input_module.getFuture(),                // in for loop
                FS_merchant_transaction_history_rollup.getFuture(),                // in for loop
                FS_ip_retrieval.getFuture(),                // in for loop
                FS_fraud_model.getFuture(),                // in for loop
                FS_customer_transaction_history_rollup.getFuture(),                // in for loop
                FS_customer_level_data.getFuture(),                // in for loop
                FS_card_level_data.getFuture(),                // in for loop
                FS_account_level_data.getFuture()        ).via(cpuExecutor.get())
        .thenValue([txn](auto unit [[maybe_unused]]) -> folly::Future<folly::Unit> {
            txn->MI_fraud_rules->init();
        // If there is a timeout set
            auto lFut = txn->MI_fraud_rules->start();
            if( moduleInfo.fraud_rules->moduleTimeout > std::chrono::microseconds(0)) {
              lFut = std::move(lFut).within(std::chrono::ceil<std::chrono::milliseconds>(moduleInfo.fraud_rules->moduleTimeout))
                .thenError<folly::FutureTimeout>([txn](folly::FutureTimeout ){
                    LOG(INFO) << "Module fraud_rules timedout for transaction " << txn->messageMetaDataStruct.getTransactionId().toHexString();
                    ++moduleInfo.fraud_rules->metrics->timeoutCountExecution;
                    std::lock_guard lg(txn->MI_fraud_rules->responseLock);
                    txn->MI_fraud_rules->timedout = true;
                    return folly::Unit();
                });
            }
            return std::move(lFut).thenError([txn] (folly::exception_wrapper ew) -> folly::Future<folly::Unit> {
              std::lock_guard lg(txn->MI_fraud_rules->responseLock);
              txn->MI_fraud_rules->timedout = true;
              LOG(INFO) << "Module fraud_rules error " <<  txn->messageMetaDataStruct.getTransactionId().toHexString() << " " << ew.what() << " false";
              return ew;
            });
        });


        FS_fraud_rules_P = std::move(FS_fraud_rules_P)
        .thenValue([txn](auto unit [[maybe_unused]]) {
//                    txn->MI_auth_response->fraud_ruleset_result = txn->MI_fraud_rules->fraud_ruleset_result;
//                    txn->MI_auth_response->fraud_ruleset_result = raven::generated::Getrecord_fraud_ruleset_result_V (txn->MI_fraud_rules->BUFFER_fraud_ruleset_result.data());
                    txn->MI_auth_response->fraud_ruleset_result = txn->MI_fraud_rules->OUT_fraud_ruleset_result;
//                    txn->MI_auth_logging_module->fraud_ruleset_result = txn->MI_fraud_rules->fraud_ruleset_result;
//                    txn->MI_auth_logging_module->fraud_ruleset_result = raven::generated::Getrecord_fraud_ruleset_result_V (txn->MI_fraud_rules->BUFFER_fraud_ruleset_result.data());
                    txn->MI_auth_logging_module->fraud_ruleset_result = txn->MI_fraud_rules->OUT_fraud_ruleset_result;
        return folly::Unit();
            });
        auto FS_fraud_rules = folly::splitFuture(std::move(FS_fraud_rules_P));

        auto FS_auth_rules_P = folly::collect(
            // in collect block.
        // predecessor {customer_level_data=[card_level_data], fraud_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup], auth_rules=[raven_input_module, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_model, account_level_data], auth_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup, account_level_data], raven_input_module=[], account_level_data=[card_level_data], fraud_rules=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, account_level_data], auth_response=[raven_input_module, fraud_rules, auth_rules], ip_retrieval=[raven_input_module], raven_output_module=[auth_response], card_level_data=[raven_input_module], customer_transaction_history_rollup=[raven_input_module, customer_level_data, card_level_data], merchant_transaction_history_rollup=[raven_input_module, account_level_data], auth_logging_module=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_rules, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_rules, auth_response, auth_model, account_level_data]}
        // module auth_rules
                // in for loop
                FS_raven_input_module.getFuture(),                // in for loop
                FS_customer_transaction_history_rollup.getFuture(),                // in for loop
                FS_customer_level_data.getFuture(),                // in for loop
                FS_card_level_data.getFuture(),                // in for loop
                FS_auth_model.getFuture(),                // in for loop
                FS_account_level_data.getFuture()        ).via(cpuExecutor.get())
        .thenValue([txn](auto unit [[maybe_unused]]) -> folly::Future<folly::Unit> {
            txn->MI_auth_rules->init();
        // If there is a timeout set
            auto lFut = txn->MI_auth_rules->start();
            if( moduleInfo.auth_rules->moduleTimeout > std::chrono::microseconds(0)) {
              lFut = std::move(lFut).within(std::chrono::ceil<std::chrono::milliseconds>(moduleInfo.auth_rules->moduleTimeout))
                .thenError<folly::FutureTimeout>([txn](folly::FutureTimeout ){
                    LOG(INFO) << "Module auth_rules timedout for transaction " << txn->messageMetaDataStruct.getTransactionId().toHexString();
                    ++moduleInfo.auth_rules->metrics->timeoutCountExecution;
                    std::lock_guard lg(txn->MI_auth_rules->responseLock);
                    txn->MI_auth_rules->timedout = true;
                    return folly::Unit();
                });
            }
            return std::move(lFut).thenError([txn] (folly::exception_wrapper ew) -> folly::Future<folly::Unit> {
              std::lock_guard lg(txn->MI_auth_rules->responseLock);
              txn->MI_auth_rules->timedout = true;
              LOG(INFO) << "Module auth_rules error " <<  txn->messageMetaDataStruct.getTransactionId().toHexString() << " " << ew.what() << " false";
              return ew;
            });
        });


        FS_auth_rules_P = std::move(FS_auth_rules_P)
        .thenValue([txn](auto unit [[maybe_unused]]) {
//                    txn->MI_auth_response->auth_ruleset_result = txn->MI_auth_rules->auth_ruleset_result;
//                    txn->MI_auth_response->auth_ruleset_result = raven::generated::Getrecord_auth_ruleset_result_V (txn->MI_auth_rules->BUFFER_auth_ruleset_result.data());
                    txn->MI_auth_response->auth_ruleset_result = txn->MI_auth_rules->OUT_auth_ruleset_result;
//                    txn->MI_auth_logging_module->auth_ruleset_result = txn->MI_auth_rules->auth_ruleset_result;
//                    txn->MI_auth_logging_module->auth_ruleset_result = raven::generated::Getrecord_auth_ruleset_result_V (txn->MI_auth_rules->BUFFER_auth_ruleset_result.data());
                    txn->MI_auth_logging_module->auth_ruleset_result = txn->MI_auth_rules->OUT_auth_ruleset_result;
        return folly::Unit();
            });
        auto FS_auth_rules = folly::splitFuture(std::move(FS_auth_rules_P));

        auto FS_auth_response_P = folly::collect(
            // in collect block.
        // predecessor {customer_level_data=[card_level_data], fraud_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup], auth_rules=[raven_input_module, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_model, account_level_data], auth_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup, account_level_data], raven_input_module=[], account_level_data=[card_level_data], fraud_rules=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, account_level_data], auth_response=[raven_input_module, fraud_rules, auth_rules], ip_retrieval=[raven_input_module], raven_output_module=[auth_response], card_level_data=[raven_input_module], customer_transaction_history_rollup=[raven_input_module, customer_level_data, card_level_data], merchant_transaction_history_rollup=[raven_input_module, account_level_data], auth_logging_module=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_rules, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_rules, auth_response, auth_model, account_level_data]}
        // module auth_response
                // in for loop
                FS_raven_input_module.getFuture(),                // in for loop
                FS_fraud_rules.getFuture(),                // in for loop
                FS_auth_rules.getFuture()        ).via(cpuExecutor.get())
        .thenValue([txn](auto unit [[maybe_unused]]) -> folly::Future<folly::Unit> {
            txn->MI_auth_response->init();
        // If there is a timeout set
            auto lFut = txn->MI_auth_response->start();
            if( moduleInfo.auth_response->moduleTimeout > std::chrono::microseconds(0)) {
              lFut = std::move(lFut).within(std::chrono::ceil<std::chrono::milliseconds>(moduleInfo.auth_response->moduleTimeout))
                .thenError<folly::FutureTimeout>([txn](folly::FutureTimeout ){
                    LOG(INFO) << "Module auth_response timedout for transaction " << txn->messageMetaDataStruct.getTransactionId().toHexString();
                    ++moduleInfo.auth_response->metrics->timeoutCountExecution;
                    std::lock_guard lg(txn->MI_auth_response->responseLock);
                    txn->MI_auth_response->timedout = true;
                    return folly::Unit();
                });
            }
            return std::move(lFut).thenError([txn] (folly::exception_wrapper ew) -> folly::Future<folly::Unit> {
              std::lock_guard lg(txn->MI_auth_response->responseLock);
              txn->MI_auth_response->timedout = true;
              LOG(INFO) << "Module auth_response error " <<  txn->messageMetaDataStruct.getTransactionId().toHexString() << " " << ew.what() << " false";
              return ew;
            });
        });


        FS_auth_response_P = std::move(FS_auth_response_P)
        .thenValue([txn](auto unit [[maybe_unused]]) {
                    // RAVEN OUTPUT MODULE GETS SET DIFFERENTLY
//                    txn->MI_auth_logging_module->txn_auth_output = txn->MI_auth_response->txn_auth_output;
//                    txn->MI_auth_logging_module->txn_auth_output = raven::generated::Getrecord_txn_auth_output_V (txn->MI_auth_response->BUFFER_txn_auth_output.data());
                    txn->MI_auth_logging_module->txn_auth_output = txn->MI_auth_response->OUT_txn_auth_output;
        return folly::Unit();
            });
        auto FS_auth_response = folly::splitFuture(std::move(FS_auth_response_P));

        auto FS_auth_logging_module_P = folly::collect(
            // in collect block.
        // predecessor {customer_level_data=[card_level_data], fraud_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup], auth_rules=[raven_input_module, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_model, account_level_data], auth_model=[raven_input_module, merchant_transaction_history_rollup, customer_transaction_history_rollup, account_level_data], raven_input_module=[], account_level_data=[card_level_data], fraud_rules=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, account_level_data], auth_response=[raven_input_module, fraud_rules, auth_rules], ip_retrieval=[raven_input_module], raven_output_module=[auth_response], card_level_data=[raven_input_module], customer_transaction_history_rollup=[raven_input_module, customer_level_data, card_level_data], merchant_transaction_history_rollup=[raven_input_module, account_level_data], auth_logging_module=[raven_input_module, merchant_transaction_history_rollup, ip_retrieval, fraud_rules, fraud_model, customer_transaction_history_rollup, customer_level_data, card_level_data, auth_rules, auth_response, auth_model, account_level_data]}
        // module auth_logging_module
                // in for loop
                FS_raven_input_module.getFuture(),                // in for loop
                FS_merchant_transaction_history_rollup.getFuture(),                // in for loop
                FS_ip_retrieval.getFuture(),                // in for loop
                FS_fraud_rules.getFuture(),                // in for loop
                FS_fraud_model.getFuture(),                // in for loop
                FS_customer_transaction_history_rollup.getFuture(),                // in for loop
                FS_customer_level_data.getFuture(),                // in for loop
                FS_card_level_data.getFuture(),                // in for loop
                FS_auth_rules.getFuture(),                // in for loop
                FS_auth_response.getFuture(),                // in for loop
                FS_auth_model.getFuture(),                // in for loop
                FS_account_level_data.getFuture()        ).via(cpuExecutor.get())
        .thenValue([txn](auto unit [[maybe_unused]]) -> folly::Future<folly::Unit> {
            txn->MI_auth_logging_module->init();
        // If there is a timeout set
            auto lFut = txn->MI_auth_logging_module->start();
            if( moduleInfo.auth_logging_module->moduleTimeout > std::chrono::microseconds(0)) {
              lFut = std::move(lFut).within(std::chrono::ceil<std::chrono::milliseconds>(moduleInfo.auth_logging_module->moduleTimeout))
                .thenError<folly::FutureTimeout>([txn](folly::FutureTimeout ){
                    LOG(INFO) << "Module auth_logging_module timedout for transaction " << txn->messageMetaDataStruct.getTransactionId().toHexString();
                    ++moduleInfo.auth_logging_module->metrics->timeoutCountExecution;
                    std::lock_guard lg(txn->MI_auth_logging_module->responseLock);
                    txn->MI_auth_logging_module->timedout = true;
                    return folly::Unit();
                });
            }
            return std::move(lFut).thenError([txn] (folly::exception_wrapper ew) -> folly::Future<folly::Unit> {
              std::lock_guard lg(txn->MI_auth_logging_module->responseLock);
              txn->MI_auth_logging_module->timedout = true;
              LOG(INFO) << "Module auth_logging_module error " <<  txn->messageMetaDataStruct.getTransactionId().toHexString() << " " << ew.what() << " false";
              return ew;
            });
        });


        FS_auth_logging_module_P = std::move(FS_auth_logging_module_P)
        .thenValue([txn](auto unit [[maybe_unused]]) {
        return folly::Unit();
            });
        auto FS_auth_logging_module = folly::splitFuture(std::move(FS_auth_logging_module_P));

        return folly::collect(
        //  after set delim
                // in for loop
                    // in if block
                     FS_account_level_data.getFuture()
                // in for loop
                    // in if block
                    , FS_auth_model.getFuture()
                // in for loop
                    // in if block
                    , FS_auth_response.getFuture()
                // in for loop
                    // in if block
                    , FS_auth_rules.getFuture()
                // in for loop
                    // in if block
                    , FS_card_level_data.getFuture()
                // in for loop
                    // in if block
                    , FS_customer_level_data.getFuture()
                // in for loop
                    // in if block
                    , FS_customer_transaction_history_rollup.getFuture()
                // in for loop
                    // in if block
                    , FS_fraud_model.getFuture()
                // in for loop
                    // in if block
                    , FS_fraud_rules.getFuture()
                // in for loop
                    // in if block
                    , FS_ip_retrieval.getFuture()
                // in for loop
                    // in if block
                    , FS_merchant_transaction_history_rollup.getFuture()
                // in for loop
        ).thenValue([txn](auto unit [[maybe_unused]]) {
            /* Add mapping to output of the transaction */
                txn->RO_txn_output = txn->MI_auth_response->OUT_txn_auth_output;
                        return folly::Unit();
        })
        .within(std::chrono::duration_cast<std::chrono::milliseconds>(timeout)) // Total transaction timeout
        .thenError([txn](folly::exception_wrapper &&e) -> folly::Future<folly::Unit> {
          LOG(ERROR)<<"Error executing transaction txn_auth V-1 - error = "<<e.what() << " " << txn->messageMetaDataStruct.getTransactionId().toHexString();
            txn->getDefaultResponse();
            return folly::Unit();
        });
    
    /* end generation of future for mapping of txn input */
}


void raven::generated::transaction_txn_auth_V1::getDefaultResponse(){
    raven::generated::RG_record_txn_auth_output_V1  txn_output;
    const auto txn_input = RI_txn_input;
  fb_write(txn_output,amount) = fb_read(txn_input,amount);
  fb_write(txn_output,card_number) = fb_read(txn_input,card_number);
  fb_write(txn_output,currency_code) = fb_read(txn_input,currency_code);
  fb_write(txn_output,device_id) = fb_read(txn_input,device_id);
  fb_write(txn_output,ip_address) = fb_read(txn_input,ip_address);
  fb_write(txn_output,merchant_id) = fb_read(txn_input,merchant_id);
  fb_write(txn_output,trace_number) = fb_read(txn_input,trace_number);
  fb_write_date(txn_output,transaction_date) = fb_read_date(txn_input,transaction_date);
  fb_write(txn_output,transaction_response) = u8"S"sv;
  fb_write_time(txn_output,transaction_time) = fb_read_time(txn_input,transaction_time);
  fb_write(txn_output,transaction_type) = fb_read(txn_input,transaction_type);


    flatbuffers::FlatBufferBuilder fbb_txn_output;
    auto offset_txn_output = raven::generated::Createrecord_txn_auth_output_V1(fbb_txn_output, &txn_output);
    fbb_txn_output.Finish(offset_txn_output);
    auto dBuff_txn_output = fbb_txn_output.Release();
    RO_txn_output = raven::generated::Getrecord_txn_auth_output_V1(dBuff_txn_output.data());
}


raven::generated::transaction_txn_auth_V1::transaction_txn_auth_V1() : raven::base::Transaction(getTransactionInfo_txn_auth_V1()) {

}


flatbuffers::DetachedBuffer raven::generated::transaction_txn_auth_V1::getRecord(const std::string_view recordName) const {
      if(recordName == "txn_input") {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_txn_auth_input_V1(fbb, RI_txn_input->UnPack());
        fbb.Finish(offset);
        auto detachedBuffer = fbb.Release();
        return detachedBuffer;
      }
      if(recordName == "txn_output") {
        flatbuffers::FlatBufferBuilder fbb;
        auto offset = raven::generated::Createrecord_txn_auth_output_V1(fbb, RO_txn_output->UnPack());
        fbb.Finish(offset);
        auto detachedBuffer = fbb.Release();
        return detachedBuffer;
      }
return {};
}

const flatbuffers::Table * raven::generated::transaction_txn_auth_V1::getRecordPtr(const std::string_view recordName) const {
      if(recordName == "txn_input") {
        return reinterpret_cast<const flatbuffers::Table *> (this->RI_txn_input);
      }
      if(recordName == "txn_output") {
        return reinterpret_cast<const flatbuffers::Table *> (this->RO_txn_output);
      }
return nullptr;
}

raven::generated::transaction_txn_auth_V1::~transaction_txn_auth_V1() = default;

std::shared_ptr<raven::base::Transaction> createNewTransaction_txn_auth_V1() {
    return std::make_shared<raven::generated::transaction_txn_auth_V1>();
}







#include "raven-engine/base/Artifact.h"




static bool transaction_txn_auth_V1_StatusChangeCallBack(raven::base::RavenArtifact & artifact [[maybe_unused]], raven::base::ArtifactStatus status [[maybe_unused]])
 {
    if(status==raven::base::ArtifactStatus::LOADED){
      raven::base::Transaction::AddTransactionInfo("txn_auth", 1,
      const_cast<raven::base::TransactionInfo *>(getTransactionInfo_txn_auth_V1()));
    } else if(status==raven::base::ArtifactStatus::ACTIVATED) {
      const_cast<raven::base::TransactionInfo *>(getTransactionInfo_txn_auth_V1())->active = true;
    } else if(status==raven::base::ArtifactStatus::DEACTIVATED) {
      const_cast<raven::base::TransactionInfo *>(getTransactionInfo_txn_auth_V1())->active = false;
    } else if(status==raven::base::ArtifactStatus::UNLOADED) {
        raven::base::Transaction::RemoveTransactionInfo("txn_auth", 1, getTransactionInfo_txn_auth_V1());
    }
    return true;
}

extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_transaction_txn_auth_V1();
extern "C" std::shared_ptr<raven::base::Artifact> get_artifact_transaction_txn_auth_V1() {
  std::shared_ptr<raven::base::Artifact> artifact = std::make_shared<raven::base::RavenArtifact>("Transaction",     std::vector({    std::string("txn_auth"),
})
, 1, transaction_txn_auth_V1_StatusChangeCallBack, "transaction_txn_auth_V1");
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("auth_response"),
})
, "Module", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("account_level_data"),
})
, "Module", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("fraud_model"),
})
, "Module", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_transaction_history_rollup"),
})
, "Module", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("customer_level_data"),
})
, "Module", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("card_level_data"),
})
, "Module", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("txn_auth_output"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("txn_auth_input"),
})
, "Record", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("auth_logging_module"),
})
, "Module", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("fraud_rules"),
})
, "Module", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("auth_model"),
})
, "Module", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("auth_rules"),
})
, "Module", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("ip_retrieval"),
})
, "Module", 1};
        artifact->addDependency(dep);
      }
      {
        auto dep=raven::base::ArtifactKey{    std::vector({    std::string("merchant_transaction_history_rollup"),
})
, "Module", 1};
        artifact->addDependency(dep);
      }
  return artifact;
}

#ifdef RAVEN_AUTO_REGISTER_ARTIFACTS
#include <cstdlib>
__attribute__((constructor))
static void transaction_txn_auth_V1_INIT() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
     auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_transaction_txn_auth_V1());
     if(!artifact) return;
     transaction_txn_auth_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::LOADED);
     transaction_txn_auth_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::ACTIVATED);
 } else {
    LOG(INFO) << "Skipping auto register even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
__attribute__((destructor))
static void transaction_txn_auth_V1_FINI() {
 if(std::getenv("RAVEN_AUTO_REGISTER_ARTIFACTS")) {
    auto artifact = std::dynamic_pointer_cast<raven::base::RavenArtifact> (get_artifact_transaction_txn_auth_V1());
    if(!artifact) return;
     transaction_txn_auth_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::DEACTIVATED);
     transaction_txn_auth_V1_StatusChangeCallBack(*artifact, raven::base::ArtifactStatus::UNLOADED);
 } else {
    LOG(INFO) << "Skipping auto unregister even though function is present due to environment variable RAVEN_AUTO_REGISTER_ARTIFACTS not defined";
 }
}
#endif
